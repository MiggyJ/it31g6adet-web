# BSIT 3-1 ADET GROUP 6 
Repository for: Barangay Management System Web Version
# Members
* Rica Bristol
* Lance Hubahib
* Stephen Laberinto
* Joseph Lamina
* Dominic Lomeda
* Jairus Montante
# How to create a page
1. Create a php file at ```./application/views/templates/[client|admin]``` for the content of the page you will create.
    - The pages are divided into sections which are represented by folders.
2. Create a php file at ```./application/views/scripts``` for the additional scripts that you will use for the page you will create.
    - The folder structure used in this folder is similar to the templates folder for ease of use.
3. Create a function in a controller that your created page will be under in.
    - Controller files can be found at ```./applications/controllers```.
    - You can create a new controller if you think it is necessary.
    - Each function has required and optional data parameters:
        * **title** (required) - the title of the page
        * **page** (required) - the current section of the page
        * **header** (required) - the header of the page. this Should be set to **default** if not using any custom header.
        * **content** (required) - the location of the template you created.
        * **script** (optional) - the location of the scripts file you created. This should be set to **blank** if none is included.
    - Return a view using a layout file found in the views folder.
        * Example: ```$this->load->view('guest_template');```
4. Add a route to the routes file at ```./application/config/routes.php```.
    - The format is ```$route[{route_name}] = {Controller}/{function}```.
5. Add the route endpoint that you created in the nav-links at the Navbar Section of the template you used for the view.