<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Guest extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
	}

	public function index(){
		$data['title'] = 'Barangay MS - Home';
		$data['page'] = '';
		$data['header'] = 'default';
		$data['content'] = 'templates/client/home/index';
		$data['scripts'] = 'scripts/client/home/index';
		$this->load->view('guest_template', $data);
	}

	public function officials(){
		$data['title'] = 'Barangay MS - Officials';
		$data['page'] = 'government';
		$data['header'] = 'Barangay Officials';
		$data['content'] = 'templates/client/government/officials';
		$data['scripts'] = 'scripts/client/government/officials';
		$this->load->view('guest_template', $data);
	}

	public function ordinance_list(){
		$data['title'] = 'Barangay MS - Ordinances';
		$data['page'] = 'government';
		$data['header'] = 'Barangay Ordinances';
		$data['content'] = 'templates/client/government/ordinance_list';
		$data['scripts'] = 'scripts/client/government/ordinance_list';
		$this->load->view('guest_template', $data);
	}

	public function ordinance_single($id){
		$data['id'] = $id;
		$data['title'] = 'Barangay MS - Ordinances';
		$data['page'] = 'government';
		$data['header'] = 'Barangay Ordinances';
		$data['content'] = 'templates/client/government/ordinance_single';
		$data['scripts'] = 'scripts/client/government/ordinance_single';
		$this->load->view('guest_template', $data);
	}

	public function onlineServices(){
		$data['title'] = 'Barangay MS - Officials';
		$data['page'] = 'services';
		$data['header'] = 'Online Services';
		$data['content'] = 'templates/client/onlineservices';
		$data['scripts'] = '';
		$this->load->view('guest_template', $data);
	}

	public function announcements_list(){
		$data['title'] = 'Barangay MS - Announcements';
		$data['page'] = 'announcements';
		$data['header'] = 'Barangay Announcements';
		$data['content'] = 'templates/client/announcements/announcements_list';
		$data['scripts'] = 'scripts/client/announcements/announcements_list';
		$this->load->view('guest_template', $data);
	}

	public function announcement_single($id){
		$data['id'] = $id;
		$data['title'] = 'Barangay MS - Announcements';
		$data['page'] = 'announcements';
		$data['header'] = 'Barangay Announcement';
		$data['content'] = 'templates/client/announcements/announcement_single';
		$data['scripts'] = 'scripts/client/announcements/announcement_single';
		$this->load->view('guest_template', $data);
	}

	public function business_list(){
		$data['title'] = 'Barangay MS - Businesses';
		$data['page'] = 'business';
		$data['header'] = 'Barangay Businesses';
		$data['content'] = 'templates/client/business/business_list';
		$data['scripts'] = 'scripts/client/business/business_list';
		$this->load->view('guest_template', $data);
	}

	public function not_found(){
		$this->load->view('error');
	}
}
