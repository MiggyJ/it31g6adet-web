<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Staff	 extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
		if(!get_cookie('token')){
			redirect('login');
		}else if(get_cookie('role') != 'Staff'){
			redirect('dashboard');
		}
	}

	//* DASHBOARD
	public function index(){
		$data['title'] = 'Barangay MS - Staff Dashboard';
		$data['page'] = 'staff_dashboard';
		$data['content'] = 'templates/admin/staff/index';
		$data['scripts'] = 'scripts/admin/staff/index';
		$this->load->view('user_template', $data);
	} 

	//* ANNOUNCEMENTS
	public function announcements_list(){
		$data['title'] = 'Barangay MS - Announcements List';
		$data['page'] = 'staff_announcements';
		$data['content'] = 'templates/admin/staff/announcement/announcements_list';
		$data['scripts'] = 'scripts/admin/staff/announcement/announcements_list';
		$this->load->view('user_template', $data);
	}

	public function announcement_single($id){
        $data['id'] = $id;
        $data['title'] = 'Barangay MS - Announcement';
		$data['page'] = 'staff_announcements';
		$data['content'] = 'templates/admin/staff/announcement/announcement_single';
		$data['scripts'] = 'scripts/admin/staff/announcement/announcement_single';
		$this->load->view('user_template', $data);
    }
	
	public function announcement_update($id){
		$data['id'] = $id;
        $data['title'] = 'Barangay MS - Update Announcement';
		$data['page'] = 'announcement';
		$data['content'] = 'templates/admin/staff/announcement/announcement_update';
		$data['scripts'] = 'scripts/admin/staff/announcement/announcement_update';
		$this->load->view('user_template', $data);
	}

	public function announcement_new(){
		$data['title'] = 'Barangay MS - New Announcement';
		$data['page'] = 'staff_announcements';
		$data['content'] = 'templates/admin/staff/announcement/announcement_new';
		$data['scripts'] = 'scripts/admin/staff/announcement/announcement_new';
		$this->load->view('user_template', $data);
	}

	//* BUSINESSES
	public function businesses_list(){
		$data['title'] = 'Barangay MS - Businesses';
		$data['page'] = 'staff_businesses';
		$data['content'] = 'templates/admin/staff/business/businesses_list';
		$data['scripts'] = 'scripts/admin/staff/business/businesses_list';
		$this->load->view('user_template', $data);
	}

	public function business_single($id){
		$data['id'] = $id;
		$data['title'] = 'Barangay MS - Certificates';
		$data['page'] = 'staff_businesses';
		$data['content'] = 'templates/admin/staff/business/business_single';
		$data['scripts'] = 'scripts/admin/staff/business/business_single';
		$this->load->view('user_template', $data);
	}

	public function business_new(){
		$data['title'] = 'Barangay MS - Businesses';
		$data['page'] = 'staff_businesses';
		$data['content'] = 'templates/admin/staff/business/business_new';
		$data['scripts'] = 'scripts/admin/staff/business/business_new';
		$this->load->view('user_template', $data);
	}

	//* CERTIFICATES
	public function certificates_list(){
		$data['title'] = 'Barangay MS - Certificates';
		$data['page'] = 'staff_certificates';
		$data['content'] = 'templates/admin/staff/certificate/certificates_list';
		$data['scripts'] = 'scripts/admin/staff/certificate/certificates_list';
		$this->load->view('user_template', $data);
	}

	public function certificate_single($id){
		$data['id'] = $id;
		$data['title'] = 'Barangay MS - Certificates';
		$data['page'] = 'staff_certificates';
		$data['content'] = 'templates/admin/staff/certificate/certificate_single';
		$data['scripts'] = 'scripts/admin/staff/certificate/certificate_single';
		$this->load->view('user_template', $data);
	}

	public function certificate_new(){
		$data['title'] = 'Barangay MS - Certificates';
		$data['page'] = 'staff_certificates';
		$data['content'] = 'templates/admin/staff/certificate/certificate_new';
		$data['scripts'] = 'scripts/admin/staff/certificate/certificate_new';
		$this->load->view('user_template', $data);
	}

	//* COMPLAINTS
	public function complaints_list(){
		$data['title'] = 'Barangay MS - Complaints';
		$data['page'] = 'staff_complaints';
		$data['content'] = 'templates/admin/staff/complaint/complaints_list';
		$data['scripts'] = 'scripts/admin/staff/complaint/complaints_list';
		$this->load->view('user_template', $data);
	}

	public function complaint_single($id){

        $data['id'] = $id;
        $data['title'] = 'Barangay MS - Complaints';
		$data['page'] = 'staff_complaints';
		$data['content'] = 'templates/admin/staff/complaint/complaint_single';
		$data['scripts'] = 'scripts/admin/staff/complaint/complaint_single';
		$this->load->view('user_template', $data);
    }

	public function complaint_new(){
        $data['title'] = 'Barangay MS - Complaints';
		$data['page'] = 'staff_complaints';
		$data['content'] = 'templates/admin/staff/complaint/complaint_new';
		$data['scripts'] = 'scripts/admin/staff/complaint/complaint_new';
		$this->load->view('user_template', $data);
	}

	public function complaint_update($id){
		$data['id'] = $id;
        $data['title'] = 'Barangay MS - Complaints';
		$data['page'] = 'staff_complaints';
		$data['content'] = 'templates/admin/staff/complaint/complaint_update';
		$data['scripts'] = 'scripts/admin/staff/complaint/complaint_update';
		$this->load->view('user_template', $data);
	}

	//* BLOTTERS
	public function blotters_list(){
		$data['title'] = 'Barangay MS - Blotters List';
		$data['page'] = 'staff_blotters';
		$data['content'] = 'templates/admin/staff/blotter/blotters_list';
		$data['scripts'] = 'scripts/admin/staff/blotter/blotters_list';
		$this->load->view('user_template', $data);
	}

	public function blotter_single($id){

        $data['id'] = $id;
        $data['title'] = 'Barangay MS - Blotters Single Data';
		$data['page'] = 'staff_blotters';
		$data['content'] = 'templates/admin/staff/blotter/blotter_single';
		$data['scripts'] = 'scripts/admin/staff/blotter/blotter_single';
		$this->load->view('user_template', $data);
    }

	public function blotter_new(){
        $data['title'] = 'Barangay MS - Blotters';
		$data['page'] = 'staff_blotters';
		$data['content'] = 'templates/admin/staff/blotter/blotter_new';
		$data['scripts'] = 'scripts/admin/staff/blotter/blotter_new';
		$this->load->view('user_template', $data);
	}

	public function blotter_update($id){
		$data['id'] = $id;
        $data['title'] = 'Barangay MS - Blotters';
		$data['page'] = 'staff_blotters';
		$data['content'] = 'templates/admin/staff/blotter/blotter_update';
		$data['scripts'] = 'scripts/admin/staff/blotter/blotter_update';
		$this->load->view('user_template', $data);
	}

	//*OFFICIALS
	public function officials_list(){
		$data['title'] = 'Barangay MS - Officials';
		$data['page'] = 'staff_officials';
		$data['content'] = 'templates/admin/staff/official/officials_list';
		$data['scripts'] = 'scripts/admin/staff/official/officials_list';
		$this->load->view('user_template', $data);
	}

	public function official_single($id){
		$data['id'] = $id;
        $data['title'] = 'Barangay MS - Officials';
		$data['page'] = 'staff_officials';
		$data['content'] = 'templates/admin/staff/official/official_single';
		$data['scripts'] = 'scripts/admin/staff/official/official_single';
		$this->load->view('user_template', $data);
	}

	public function official_new(){
		$data['title'] = 'Barangay MS - Officials';
		$data['page'] = 'staff_officials';
		$data['content'] = 'templates/admin/staff/official/official_new';
		$data['scripts'] = 'scripts/admin/staff/official/official_new';
		$this->load->view('user_template', $data);
	}

	//* ORDINANCES
	public function ordinances_list(){
		$data['title'] = 'Barangay MS - Ordinances';
		$data['page'] = 'staff_ordinances';
		$data['content'] = 'templates/admin/staff/ordinance/ordinances_list';
		$data['scripts'] = 'scripts/admin/staff/ordinance/ordinances_list';
		$this->load->view('user_template', $data);
	}

	public function ordinance_single($id){
		$data['id'] = $id;
        $data['title'] = 'Barangay MS - Ordinances';
		$data['page'] = 'staff_ordinances';
		$data['content'] = 'templates/admin/staff/ordinance/ordinance_single';
		$data['scripts'] = 'scripts/admin/staff/ordinance/ordinance_single';
		$this->load->view('user_template', $data);
	}

	public function ordinance_new(){
		$data['title'] = 'Barangay MS - Ordinances';
		$data['page'] = 'staff_ordinances';
		$data['content'] = 'templates/admin/staff/ordinance/ordinance_new';
		$data['scripts'] = 'scripts/admin/staff/ordinance/ordinance_new';
		$this->load->view('user_template', $data);
	}


	//* RELIEF OPS
	public function relief_operations_list(){
		$data['title'] = 'Barangay MS - Relief Operations';
		$data['page'] = 'relief Operations';
		$data['content'] = 'templates/admin/staff/relief_operation/relief_operations_list';
		$data['scripts'] = 'scripts/admin/staff/relief_operation/relief_operations_list';
		$this->load->view('user_template', $data);
	}

	public function relief_operation_single($id){

        $data['id'] = $id;
        $data['title'] = 'Barangay MS - Relief Operation';
		$data['page'] = 'relief Operation';
		$data['content'] = 'templates/admin/staff/relief_operation/relief_operation_single';
		$data['scripts'] = 'scripts/admin/staff/relief_operation/relief_operation_single';
		$this->load->view('user_template', $data);
    }

	public function relief_operation_new(){
        $data['title'] = 'Barangay MS - New Relief Operation';
		$data['page'] = 'staff_relief_operation';
		$data['content'] = 'templates/admin/staff/relief_operation/relief_operation_new';
		$data['scripts'] = 'scripts/admin/staff/relief_operation/relief_operation_new';
		$this->load->view('user_template', $data);
	}

	public function relief_operation_update($id){
		$data['id'] = $id;
        $data['title'] = 'Barangay MS - Update Relief Operation';
		$data['page'] = 'staff_relief_operation';
		$data['content'] = 'templates/admin/staff/relief_operation/relief_operation_update';
		$data['scripts'] = 'scripts/admin/staff/relief_operation/relief_operation_update';
		$this->load->view('user_template', $data);
	}


	//* RELIEF REC


	//* RESIDENTS
	public function residents_list(){
        $data['title'] = 'Barangay MS - Residents List';
		$data['page'] = 'staff_residents';
		$data['content'] = 'templates/admin/staff/resident/residents_list';
		$data['scripts'] = 'scripts/admin/staff/resident/residents_list';
		$this->load->view('user_template', $data);
	}

	public function resident_single($id){
		$data['id'] = $id;
        $data['title'] = 'Barangay MS - Resident';
		$data['page'] = 'staff_residents';
		$data['content'] = 'templates/admin/staff/resident/resident_single';
		$data['scripts'] = 'scripts/admin/staff/resident/resident_single';
		$this->load->view('user_template', $data);
	}

	public function resident_new(){
        $data['title'] = 'Barangay MS - New Resident';
		$data['page'] = 'staff_residents';
		$data['content'] = 'templates/admin/staff/resident/resident_new';
		$data['scripts'] = 'scripts/admin/staff/resident/resident_new';
		$this->load->view('user_template', $data);
	}

	public function resident_update($id){
		$data['id'] = $id;
        $data['title'] = 'Barangay MS - Update Resident';
		$data['page'] = 'staff_residents';
		$data['content'] = 'templates/admin/staff/resident/resident_update';
		$data['scripts'] = 'scripts/admin/staff/resident/resident_update';
		$this->load->view('user_template', $data);
	}

	//* SUMMONS
	public function summons_list(){
		$data['title'] = 'Barangay MS - Summons List';
		$data['page'] = 'staff_summons';
		$data['content'] = 'templates/admin/staff/summons/summons_list';
		$data['scripts'] = 'scripts/admin/staff/summons/summons_list';
		$this->load->view('user_template', $data);
	}

	public function summon_single($id){

        $data['id'] = $id;
        $data['title'] = 'Barangay MS - Summon';
		$data['page'] = 'staff_summons';
		$data['content'] = 'templates/admin/staff/summons/summon_single';
		$data['scripts'] = 'scripts/admin/staff/summons/summon_single';
		$this->load->view('user_template', $data);
    }

	public function summon_new(){
		$data['title'] = 'Barangay MS - New Summon';
		$data['page'] = 'staff_summons';
		$data['content'] = 'templates/admin/staff/summons/summon_new';
		$data['scripts'] = 'scripts/admin/staff/summons/summon_new';
		$this->load->view('user_template', $data);
	}
}
