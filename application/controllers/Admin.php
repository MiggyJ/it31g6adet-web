<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
		if(!get_cookie('token')){
			redirect('login');
		}else if(get_cookie('role') != 'Admin'){
			redirect('dashboard');
		}
	}

	public function index(){
		$data['title'] = 'Barangay MS - Admin Dashboard';
		$data['page'] = 'admin_dashboard';
		$data['content'] = 'templates/admin/admin/index';
		$data['scripts'] = 'scripts/admin/admin/index';
		$this->load->view('user_template', $data);
	}

	public function users_list(){
		$data['title'] = 'Barangay MS - Users';
		$data['page'] = 'users';
		$data['content'] = 'templates/admin/admin/users_list';
		$data['scripts'] = 'scripts/admin/admin/users_list';
		$this->load->view('user_template', $data);
	}

    public function user_single($id){
        $data['id'] = $id;
        $data['title'] = 'Barangay MS - User';
		$data['page'] = 'users';
		$data['content'] = 'templates/admin/admin/user_single';
		$data['scripts'] = 'scripts/admin/admin/user_single';
		$this->load->view('user_template', $data);
    }

	public function user_update($id){
		$data['id'] = $id;
        $data['title'] = 'Barangay MS - User';
		$data['page'] = 'users';
		$data['content'] = 'templates/admin/admin/user_update';
		$data['scripts'] = 'scripts/admin/admin/user_update';
		$this->load->view('user_template', $data);
	}

	public function user_new(){
		$data['title'] = 'Barangay MS - Users';
		$data['page'] = 'users';
		$data['content'] = 'templates/admin/admin/user_new';
		$data['scripts'] = 'scripts/admin/admin/user_new';
		$this->load->view('user_template', $data);
	}

}
