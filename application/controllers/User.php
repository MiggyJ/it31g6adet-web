<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->helper('cookie');
		if(!get_cookie('token')){
			redirect('login');
		}
	}

	public function index(){
		$data['title'] = 'Barangay MS - Dashboard';
		$data['page'] = 'dashboard';
		$data['content'] = 'templates/admin/user/index';
		$data['scripts'] = 'scripts/admin/user/index';
		$this->load->view('user_template', $data);
	}

	public function profile(){
		$data['title'] = 'Barangay MS - User Profile';
		$data['page'] = 'Profile';
		$data['content'] = 'templates/admin/user/profile';
		$data['scripts'] = 'scripts/admin/user/profile';
		$this->load->view('user_template', $data);
	}


	//*COMPLAINTS
	public function complaints_list(){
		$data['title'] = 'Barangay MS - Complaints';
		$data['page'] = 'complaints';
		$data['content'] = 'templates/admin/user/complaint/complaints_list';
		$data['scripts'] = 'scripts/admin/user/complaint/complaints_list';
		$this->load->view('user_template', $data);
	}

	public function complaint_single($id){

        $data['id'] = $id;
        $data['title'] = 'Barangay MS - Complaints';
		$data['page'] = 'complaints';
		$data['content'] = 'templates/admin/user/complaint/complaint_single';
		$data['scripts'] = 'scripts/admin/user/complaint/complaint_single';
		$this->load->view('user_template', $data);
    }

	//*BLOTTER
	public function blotters_list(){
		$data['title'] = 'Barangay MS - Blotters';
		$data['page'] = 'blotters';
		$data['content'] = 'templates/admin/user/blotter/blotters_list';
		$data['scripts'] = 'scripts/admin/user/blotter/blotters_list';
		$this->load->view('user_template', $data);
	}

	public function blotter_single($id){

        $data['id'] = $id;
        $data['title'] = 'Barangay MS - Blotters';
		$data['page'] = 'blotters';
		$data['content'] = 'templates/admin/user/blotter/blotter_single';
		$data['scripts'] = 'scripts/admin/user/blotter/blotter_single';
		$this->load->view('user_template', $data);
    }

	//*BUSINESS
	public function businesses_list(){
		$data['title'] = 'Barangay MS - Business';
		$data['page'] = 'businesses';
		$data['content'] = 'templates/admin/user/business/businesses_list';
		$data['scripts'] = 'scripts/admin/user/business/businesses_list';
		$this->load->view('user_template', $data);
	}

	public function business_single($id){

        $data['id'] = $id;
        $data['title'] = 'Barangay MS - Business';
		$data['page'] = 'businesses';
		$data['content'] = 'templates/admin/user/business/business_single';
		$data['scripts'] = 'scripts/admin/user/business/business_single';
		$this->load->view('user_template', $data);
    }

	public function business_new(){
		$data['title'] = 'Barangay MS - Businesses';
		$data['page'] = 'businesses';
		$data['content'] = 'templates/admin/user/business/business_new';
		$data['scripts'] = 'scripts/admin/user/business/business_new';
		$this->load->view('user_template', $data);
	}

	//*CERTIFICATES
	public function certificates_list(){
		$data['title'] = 'Barangay MS - Certificates';
		$data['page'] = 'certificates';
		$data['content'] = 'templates/admin/user/certificates/certificates_list';
		$data['scripts'] = 'scripts/admin/user/certificates/certificates_list';
		$this->load->view('user_template', $data);
	}

	//*SUMMONS
	public function summons_list(){
		$data['title'] = 'Barangay MS - Summons';
		$data['page'] = 'summons';
		$data['content'] = 'templates/admin/user/summon/summons_list';
		$data['scripts'] = 'scripts/admin/user/summon/summons_list';
		$this->load->view('user_template', $data);
	}

	public function summon_single($id){
		$data['id'] = $id;
		$data['title'] = 'Barangay MS - Summons';
		$data['page'] = 'summons';
		$data['content'] = 'templates/admin/user/summon/summon_single';
		$data['scripts'] = 'scripts/admin/user/summon/summon_single';
		$this->load->view('user_template', $data);
	}
	
}

