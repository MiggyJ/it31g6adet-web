<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function login(){
		$this->load->helper('cookie');
		if(get_cookie('token'))
			redirect('dashboard');
		$this->load->view('auth/login');
	}

	public function register(){
		$this->load->view('auth/register');
	}
}
