<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

// Admin Pages
$route['admin'] = 'Admin';
$route['admin/user/new'] = 'Admin/user_new';
$route['admin/user/edit/(:any)'] = 'Admin/user_update/$1';
$route['admin/user/(:any)'] = 'Admin/user_single/$1';
$route['admin/users'] = 'Admin/users_list';

//Staff pages
//*Staff Dashboard
$route['staff'] = 'Staff';

//*Announcements
$route['staff/announcement/new'] = 'Staff/announcement_new';
$route['staff/announcement/edit/(:any)'] = 'Staff/announcement_update/$1';
$route['staff/announcement/(:any)'] = 'Staff/announcement_single/$1';
$route['staff/announcements'] = 'Staff/announcements_list';

//*Businesses
$route['staff/business/(:any)'] = 'Staff/business_single/$1';
$route['staff/businesses'] = 'Staff/businesses_list';

//*Certificates
$route['staff/certificate/new'] = 'Staff/certificate_new';
$route['staff/certificate/(:any)'] = 'Staff/certificate_single/$1';
$route['staff/certificates'] = 'Staff/certificates_list';

//*Complaints 
$route['staff/complaint/new'] = 'Staff/complaint_new';
$route['staff/complaint/edit/(:any)'] = 'Staff/complaint_update/$1';
$route['staff/complaint/(:any)'] = 'Staff/complaint_single/$1';
$route['staff/complaints'] = 'Staff/complaints_list';

//*Blotters
$route['staff/blotter/new'] = 'Staff/blotter_new';
$route['staff/blotter/edit/(:any)'] = 'Staff/blotter_update/$1';
$route['staff/blotter/(:any)'] = 'Staff/blotter_single/$1';
$route['staff/blotters'] = 'Staff/blotters_list';

//*Officials
$route['staff/official/new'] = 'Staff/official_new';
$route['staff/official/(:any)'] = 'Staff/official_single/$1';
$route['staff/officials'] = 'Staff/officials_list';

//*Ordinances
$route['staff/ordinance/new'] = 'Staff/ordinance_new';
$route['staff/ordinance/(:any)'] = 'Staff/ordinance_single/$1';
$route['staff/ordinances'] = 'Staff/ordinances_list';

//*Relief Ops
$route['staff/relief_operation/new'] = 'Staff/relief_operation_new';
$route['staff/relief_operationedit/(:any)'] = 'Staff/relief_operation_update/$1';
$route['staff/relief_operation/(:any)'] = 'Staff/relief_operation_single/$1';
$route['staff/relief_operations'] = 'Staff/relief_operations_list';

//*Relief Rec
$route['staff/relief_recipient/(:any)'] = 'Staff/relief_recipient_single/$1';
$route['staff/relief_recipient'] = 'Staff/relief_recipients_list';

//*Residents
$route['staff/resident/new'] = 'Staff/resident_new';
$route['staff/resident/edit/(:any)'] = 'Staff/resident_update/$1';
$route['staff/resident/(:any)'] = 'Staff/resident_single/$1';
$route['staff/residents'] = 'Staff/residents_list';

//*Summons
$route['staff/summon/new'] = 'Staff/summon_new';
$route['staff/summon/(:any)'] = 'Staff/summon_single/$1';
$route['staff/summons'] = 'Staff/summons_list';

// User pages
//*User Dashboard
$route['dashboard'] = 'User';
$route['user/profile'] = 'User/profile';
//*Complaint
$route['user/complaint/(:any)'] = 'User/complaint_single/$1';
$route['user/complaints'] = 'User/complaints_list';

//*Blotter
$route['user/blotter/(:any)'] = 'User/blotter_single/$1';
$route['user/blotters'] = 'User/blotters_list';

//*Blotter
$route['user/business/(:any)'] = 'User/business_single/$1';
$route['user/businesses'] = 'User/businesses_list';

//*Certificates
$route['user/certificates'] = 'User/certificates_list';

//* Summons
$route['user/summon/(:any)'] = 'User/summon_single/$1';
$route['user/summons'] = 'User/summons_list';

// Auth pages
$route['register'] = 'Auth/register';
$route['login'] = 'Auth/login';

// Guest pages
$route['announcement/(:any)'] = 'Guest/announcement_single/$1';
$route['announcements'] = 'Guest/announcements_list';
$route['businesses'] = 'Guest/business_list';
$route['officials'] = 'Guest/officials';
$route['ordinances'] = 'Guest/ordinance_list';
$route['ordinances/(:any)'] = 'Guest/ordinance_single/$1';
$route['services'] = 'Guest/onlineServices';
$route['default_controller'] = 'Guest';


$route['404_override'] = 'Guest/not_found';
$route['translate_uri_dashes'] = FALSE;

