<script>

    <?php if(isset($_GET['mode'])):?>
        setTimeout(() => {
            $('#edit').click()
        }, 1000);
    <?php endif;?>

    CKEDITOR.replace('details')
    let blotter, status, reason, complainant, defendant, details, form_date, incidence_date, incidence_place

    function getResidents(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=defendant_id], select[name=complainant_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
            }
        });
    }
    getResidents()

    function fetchBlotter(reset = false){
        $.ajax({
            type: "get",
            url: proxy + "/api/blotter/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    $('#blotterNumber').val(data.blotter_number)
                    $('#complainant_id').val(data.blotter_complainant.id)
                    $('#reason').val(data.reason)
                    $('#incidence_date').val(data.incidence_date)
                    $('#defendant_id').val(data.blotter_defendant.id)
                    $('#incidence_place').val(data.incidence_place)
                    $('#details').val(data.details)

                    if(!reset){
                        $('select').selectpicker({
                            style: 'select-with-transition',
                            liveSearch: true
                        })
                    }else
                        CKEDITOR.instances.details.setReadOnly(true)
                    $('select').attr('disabled', true).selectpicker('refresh')
                    CKEDITOR.instances.details.setData(details)
                    
                    blotter = data.id
                    status = data.status
                    complainant = data.blotter_complainant.id
                    defendant = data.blotter_defendant.id
                    details = data.details
                    incidence_date = data.incidence_date
                    form_date = `${new Date(data.incidence_date).getMonth() + 1}/${new Date(data.incidence_date).getDate()}/${new Date(data.incidence_date).getFullYear()}`
                    incidence_place = data.incidence_place
                    reason = data.reason

                    if(data.status == 'Active'){
                        $('#delete').text('DELETE')
                    }else{
                        $('#delete').text('RESTORE')
                    }
                }
            }
        });
    }
    fetchBlotter()

    $('#edit').click(()=>{
        $('[disabled]:not(#blotterNumber)').attr('disabled', false)
        $('select').attr('disabled', false).selectpicker('refresh')
        CKEDITOR.instances.details.setReadOnly(false)
        $('[name=incidence_date]').val(form_date)
        $('[name=incidence_date]').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        })
        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted=false){
        if(!submitted){
            fetchBlotter(true)
        }
        $('input').attr('disabled', true)

        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        
        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }

    $('#cancel').click(resetForm)

    let form = $('#blotter_update').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.details.updateElement()
    }).validate({
        ignore: [],
        rules:{
            defendant_id:{
                required: true,
            },
            complainant_id:{
                required: true,
            },
            incidence_place:{
                required: true
            },
            incidence_date: {
                required: true,
            },
            reason:{
                required: true
            },
            details:{
                required: true
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $.ajax({
                type: "PUT",
                url: proxy + "/api/blotter/" + blotter,
                data: $('#blotter_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(null, submitted=true)
                        fetchBlotter()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(status == 'Active'){
            Swal.fire({
                title: 'Delete Blotter',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/blotter/" + blotter, //delete
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchBlotter()
                                $.notify(
                                    {
                                        message: response.message
                                    }
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }else{
            Swal.fire({
                title: 'Restore Blotter',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                preConfirm: ()=>{
                    $.ajax({
                        type: "put",
                        url: proxy + "/api/blotter/restore/" + blotter,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchBlotter()
                                $.notify(
                                    {message: response.message},
                                    {type: 'warning'}
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }
    })
    
</script>