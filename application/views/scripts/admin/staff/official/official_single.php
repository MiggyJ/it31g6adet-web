<script>

let official, status, start_date, end_date, start_form, end_form
function getResidents(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=resident_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
            }
        });
}
    getResidents()

function fetchOfficial(reset = false){
        $.ajax({
            type: "get",
            url: proxy + "/api/official/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    $('[name=resident_id]').val(data.profile.id)
                    $('[name=position]').val(data.position)
                    $('[name=term_start]').val(data.term_start)
                    $('[name=term_end]').val(data.term_end)
                    $('#resident_profile').attr('href', '<?=base_url()?>staff/resident/'+data.profile.id)

                    official = data.id
                    status = data.status
                    start_date = data.term_start
                    end_date = data.term_end

                    start_form = `${new Date(data.term_start).getMonth() + 1}/${new Date(data.term_start).getDate()}/${new Date(data.term_start).getFullYear()}`

                    end_form = `${new Date(data.term_end).getMonth() + 1}/${new Date(data.term_end).getDate()}/${new Date(data.term_end).getFullYear()}`

                    if(!reset){
                        $('[name=position]').selectpicker({
                            style: 'select-with-transition',
                        })
                        $('[name=resident_id]').selectpicker({
                            style: 'select-with-transition',
                            liveSearch: true
                        })
                    }
                    $('select').attr('disabled', true).selectpicker('refresh')

                    $('#edit').attr('href', '<?=base_url()?>staff/resident/edit/'+data.id);

                    if(data.status == 'Active'){
                        $('#delete').text('DELETE')
                    }else{
                        $('#delete').text('RESTORE')
                    }
                }
            }
        });
    }

    fetchOfficial()

    $('#edit').click(()=>{
        $('[disabled]').attr('disabled', false)
        $('select').attr('disabled', false).selectpicker('refresh')

        $('[name=term_start]').val(start_form).datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        })

        $('[name=term_end]').val(end_form).datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        })

        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted = false){
        if(!submitted){
            fetchOfficial(true)
        }
        $('input').attr('disabled', true)

        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }

    $('#cancel').click(resetForm)

    let form = $('#official_update').validate({
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $.ajax({
                type: "PUT",
                url: proxy + "/api/official/" + official,
                data: $('#official_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function(response){
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(null, submitted=true)
                        fetchOfficial()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                }
            })
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(status == 'Active'){
            Swal.fire({
                title: 'Deactivate Official',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/official/" + official,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchOfficial()
                                $.notify(
                                    { message: response.message },
                                    { type: 'success' }
                                )
                            }else{
                                $.notify(
                                    { message: response.message },
                                    { type: 'danger' }
                                )
                            }
                        }
                    });
                    return
                }
            })
        }else{
            Swal.fire({
                title: 'Restore Official',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "put",
                        url: proxy + "/api/official/restore/" + official,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchOfficial()
                                $.notify(
                                    { message: response.message },
                                    { type: 'success' }
                                )
                            }else{
                                $.notify(
                                    { message: response.message },
                                    { type: 'danger' }
                                )
                            }
                        }
                    });
                    return
                }
            })
        }
    })
</script>