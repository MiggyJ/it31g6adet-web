<script>
    function getResidents(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=resident_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
                $('[name=position]').selectpicker({
                    style: 'select-with-transition',
                })
                $('[name=resident_id]').selectpicker({
                    style: 'select-with-transition',
                    liveSearch: true
                })
            }
        });
    }
    getResidents()

    $('[name=term_start]').datetimepicker({
        format: 'MM/DD/YYYY',
        icons: {
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    })

    $('[name=term_end]').datetimepicker({
        format: 'MM/DD/YYYY',
        icons: {
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    })

    function resetForm(){
        form.resetForm()
        $('select').val(null).selectpicker('refresh')
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        $('#official_new').get(0).reset()
        $('[type=submit]').attr('disabled', false)
    }

    let form = $('#official_new').validate({
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $('[type=submit]').attr('disabled', true)
            $.ajax({
                type: "POST",
                url: proxy + "/api/official/create",
                data: $('#official_new').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {
                                message: `${response.message} Click to view.`,
                                url: '<?=base_url()?>/staff/official/'+response.data.id,
                                target: '_blank'
                            },
                            {type: 'success'}
                        )
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        ) 
                    }
                    resetForm()
                }
            });
        }
    })
</script>