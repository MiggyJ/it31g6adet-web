<script>
    let allTable = $('#allTable').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/official/datatables',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs:[
            {
                targets: [4,5],
                className: 'dt-body-center'
            }
        ],
        columns: [
            {data: 'profile.first_name', visible: false, searchable: true},
            {data: 'profile.last_name', visible: false, searchable: true},
            {data: 'profile.full_name', searchable: false, orderable: false},
            {data: 'position'},
            {data: 'term_start'},
            {data: 'term_end'},
            {
                data: 'id', render: (data)=>(
                    `
                    <div class="text-center">
                        <a href="http://localhost/front/staff/official/${data}" class="btn btn-link btn-info btn-just-icon like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                        <a href="http://localhost/front/staff/official/${data}?mode=edit" class="btn btn-link btn-warning btn-just-icon edit" data-toggle="tooltip" data-placement="left" title="Edit"><i class="material-icons">edit</i></a>
                        <div data-item="${data}" class="btn btn-link btn-danger btn-just-icon remove" data-toggle="tooltip" data-placement="left" title="Delete"><i class="material-icons">delete</i></div>
                    </div>
                    `
                )
            },
        ]
    })

    let currentTable = $('#currentTable').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/official/datatablesCurrent',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns: [
            {data: 'profile.first_name', visible: false, searchable: true},
            {data: 'profile.last_name', visible: false, searchable: true},
            {data: 'profile.full_name', searchable: false, orderable: false},
            {data: 'position'},
            {data: 'term_start'},
            {data: 'term_end'},
            {
                data: 'id', render: (data)=>(
                    `
                    <div class="text-center">
                        <a href="http://localhost/front/staff/official/${data}" class="btn btn-link btn-info btn-just-icon like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                        <a href="http://localhost/front/staff/official/${data}?mode=edit" class="btn btn-link btn-warning btn-just-icon edit" data-toggle="tooltip" data-placement="left" title="Edit"><i class="material-icons">edit</i></a>
                        <div data-item="${data}" class="btn btn-link btn-danger btn-just-icon remove" data-toggle="tooltip" data-placement="left" title="Delete"><i class="material-icons">delete</i></div>
                    </div>
                    `
                )
            },
        ]
    })

    let inactiveTable = $('#inactiveTable').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/official/datatablesInactive',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns: [
            {data: 'profile.first_name', visible: false, searchable: true},
            {data: 'profile.last_name', visible: false, searchable: true},
            {data: 'profile.full_name', searchable: false, orderable: false},
            {data: 'position'},
            {data: 'term_start'},
            {data: 'term_end'},
            {
                data: 'id', render: (data)=>(
                    `
                    <div class="text-center">
                        <a href="http://localhost/front/staff/resident/${data}" class="btn btn-link btn-info btn-just-icon like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                        <div data-item="${data}" class="btn btn-link btn-warning btn-just-icon remove" data-toggle="tooltip" data-placement="left" title="Restore"><i class="material-icons">restore</i></div>
                    </div>
                    `
                )
            },
        ]
    })

    $('#allTable, #currentTable').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-danger')){
            const id = $(e.target.parentElement).data('item')
            if(allTable.row(this).data().status == 'Active' || currentTable.row(this).data().status == 'Active'){
                Swal.fire({
                    title: 'Deactivate Official',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    confirmButtonColor: '#f44336',
                    preConfirm: ()=>{
                        $.ajax({
                            type: "delete",
                            url: proxy + "/api/official/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    allTable.draw()
                                    currentTable.draw()
                                    inactiveTable.draw()
                                    $.notify(
                                        {message: response.message},
                                        {type: 'warning'}
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })

    $('#inactiveTable').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-warning')){
            const id = $(e.target.parentElement).data('item')
            if(inactiveTable.row(this).data().status == 'Inactive'){
                Swal.fire({
                    title: 'Restore Official',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    preConfirm: ()=>{
                        $.ajax({
                            type: "put",
                            url: proxy + "/api/official/restore/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    currentTable.draw()
                                    allTable.draw()
                                    inactiveTable.draw()
                                    $.notify(
                                        {message: response.message},
                                        {type: 'success'}
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })
</script>