<script>
    <?php if(isset($_GET['mode'])):?>
        setTimeout(() => {
            $('#edit').click()
        }, 1000);
    <?php endif;?>
    CKEDITOR.replace('details')
    let summon, status, summon_date, form_date, details
    function fetchSummon(reset = false){
        $.ajax({
            type: "get",
            url: proxy + "/api/summon/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    //*FILL UP FORM
                    $('#complaint>a').attr('href', `<?=base_url()?>staff/complaint/${data.complaint_id}`)
                    $('#complaint>a').text(data.summon_complaint.complaint_number)
                    $('#date').val()
                    $('#summon_date').val(data.summon_date)
                    //*Dates for form and view
                    summon_date = data.summon_date
                    form_date = `${new Date(data.summon_date).getMonth() + 1}/${new Date(data.summon_date).getDate()}/${new Date(data.summon_date).getFullYear()}`
                    //*Format time to 12-hour
                    $('#summon_time').val(moment(data.summon_time, 'HH:mm').format('hh:mm A'))
                    $('#summon_complainant').val(data.summon_complaint.complaint_by.full_name)
                    $('#summon_defendant').val(data.summon_complaint.complaint_defendant.full_name)
                    $('#mediator').val(data.mediator && data.mediator.profile.full_name)
                    $('#duration').val(data.duration)
                    $('#details').val(data.details)
                    $('#status').val(data.status)
                    if(!reset){
                        $('[name=status').selectpicker({
                            style: 'select-with-transition',
                            width: 'fit'
                        })
                    }else
                    CKEDITOR.instances.details.setReadOnly(true)

                    $('[name=status').attr('disabled', true).selectpicker('refresh')
                    CKEDITOR.instances.details.setData(details)
                    
                    summon = data.id
                    status = data.status
                    details = data.details

                }
            }
        });
    }
    fetchSummon()

    $('#edit').click((e)=>{
        //*Enable disabled inputs except :not
        $('[disabled]:not(#summon_complainant, #summon_defendant, #mediator)').attr('disabled', false)
        //*Enable CKEDITOR (CKEDITOR.instances.[textarea id])
        CKEDITOR.instances.details.setReadOnly(false)
        //*Engable SelectPicker
        $('#status').prop('disabled', false).selectpicker('refresh')
        //*Enable datetimepicker
        $('[name=summon_date]').val(form_date)
        $('[name=summon_date]').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        })

        $('[name=summon_time]').datetimepicker({
            format: 'h:mm A',
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        })

        //*Show cancel and update button
        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted = false){
        //*Disabled inputs, CKEDITOR, SelectPicker, and DateTimePicker
        $('input').attr('disabled', true)
        if(!submitted){
            fetchSummon(true)
            // $('#status').val(status)
            // CKEDITOR.instances.details.setData(details)
        }
        $('[name=summon_date], [name=summon_time]').attr('disabled', true)
        $('[name=summon_date]').val(summon_date)

        // * Reset Form
        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')

        //* Show edit and delete button
        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }

    $('#cancel').on('click', resetForm)

    let form = $('#summon_update').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.details.updateElement()
    }).validate({
            ignore: [],
            rules:{
                summon_date: {
                    required: true,
                    date: true
                },
                summon_time:{
                    required: true
                },
                duration: {
                    required: true,
                },
                details: {
                    required: true,
                    minlength: 10
                }
            },
            errorElement: 'span',
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
            },
            unhighlight: function(element, errorClass, successClass){
                $(element).closest('.form-group').removeClass('has-danger')
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
            },
            errorPlacement : function(error, element) {
                element.closest('.form-group').append(error)
            },
            submitHandler: function(){
                $.ajax({
                    type: "PUT",
                    url: proxy +'/api/summon/' + summon,
                    data: $('#summon_update').serialize(),
                    dataType: "json",
                    xhrFields:{
                        withCredentials: true
                    },
                    success: function (response) {
                        if(!response.error){
                            $.notify(
                                {message: response.message},
                                {type: 'success'}
                            )
                        }
                        resetForm(null, true)
                        fetchSummon()
                    }
                });
            }
        })

    // ? Witnesses

    $('.card-header').click(function (e) { 
        e.preventDefault();
        setTimeout(() => {
            if($('#witness').hasClass('active')){
                $('#form_footer').addClass('d-none')
                $('#witness_footer').removeClass('d-none')
            }else{
                $('#form_footer').removeClass('d-none')
                $('#witness_footer').addClass('d-none')
            }   
        }, 10);
    });

    function getSelectOptions(){

        //* GET RESIDENTS
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=resident_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
                $('select[name=resident_id]').selectpicker({
                    style: 'select-with-transition',
                    liveSearch: true
                })
            }
        });
    }

    getSelectOptions()

    let witnessTable
    function fetchWitnesses(){
        witnessTable = $('#witness_table').DataTable({
            serverSide: true,
            processing: true,
            language: {
                processing: 'Fetching Data...'
            },
            ajax: {
                url: proxy + '/api/witness/datatables?summon_id='+summon,
                xhrFields:{
                    withCredentials: true
                }
            },
            drawCallback: function(){
                $('[rel=tooltip]').tooltip()
            },
            columns: [
                {data: 'resident_profile.full_name', searchable: false, orderable: false},
                {data: 'resident_profile.personal_contact_number'},
                {data: 'resident_profile.current_address'},
                {data: 'id', render: (data)=>(`
                    <div class="text-center">
                        <div data-item="${data}" class="btn btn-link btn-danger btn-just-icon remove" rel="tooltip" data-toggle="tooltip" data-placement="left" title="Delete"><i class="material-icons">delete</i></div>
                    </div>
                `)},
            ]
        })
    }
    setTimeout(() => {
        fetchWitnesses()
    }, 350);


    $('#witness_new').submit(e=>{
        e.preventDefault()
        $.ajax({
            type: "POST",
            url: proxy + "/api/witness/create",
            data: {
                resident_id: $('[name=resident_id]').val(),
                summon_id: summon
            },
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(!response.error){
                    $.notify(
                        {message: response.message},
                        {type: 'success'}
                    )
                    $('#witness_new').get(0).reset()
                    $("#witnessModal").modal('hide')
                    witnessTable.draw()
                    $('[name=resident_id]').selectpicker('refresh')
                }else{
                    $.notify(
                        {message: response.message},
                        {type: 'danger'}
                    )
                }
            }
        });
    })

    $('#witness_table').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-danger')){
            const id = $(e.target.parentElement).data('item')
            if(witnessTable.row(this).data().status != 'Inactive'){
                Swal.fire({
                    title: 'Deactivate Witness?',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    confirmButtonColor: '#f44336',
                    preConfirm: ()=>{
                        $.ajax({
                            type: "delete",
                            url: proxy + "/api/witness/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    witnessTable.draw()
                                    $.notify(
                                        {
                                            message: response.message
                                        }
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })
    
</script>