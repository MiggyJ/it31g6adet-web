<script>
    let table = $('.table').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        order: [
            [0, 'desc']
        ],
        language: {
            processing: 'Fetching Data...',
            zeroRecords: 'No summons found'
        },
        ajax: {
            url: proxy + '/api/summon/datatables',
            xhrFields:{
                withCredentials: true
            }
        },
        drawCallback: function(){
            $('[rel=tooltip]').tooltip()
        },
        columnDefs:[
            {
                targets: [0, 1],
                className: 'dt-body-center',
            }
        ],
        columns: [
            {data: 'summon_complaint.complaint_number'},
            {data: 'summon_date'},
            {data: 'summon_time', render: data=>(
                moment(data, 'HH:mm').format('hh:mm A')
            )},
            {data: 'summon_complaint.complaint_by.first_name', visible: false, searchable: true},
            {data: 'summon_complaint.complaint_by.last_name', visible: false, searchable: true},
            {data: 'summon_complaint.complaint_by.full_name', orderable: false},
            {data: 'mediator.profile.first_name', visible: false, searchable: true},
            {data: 'mediator.profile.last_name', visible: false, searchable: true},
            {data: 'mediator.profile.full_name', orderable: false},
            {data: 'status'},
            {data: 'id', render: (data)=>(`
                <div class="text-center">
                    <a href="http://localhost/front/staff/summon/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                    <a href="http://localhost/front/staff/summon/${data}?mode=edit" class="btn btn-link btn-warning btn-just-icon edit" rel="tooltip" data-toggle="tooltip" data-placement="left" title="Edit"><i class="material-icons">edit</i></a>
                </div>
            `)},
        ]
    })

    // *Filter buttons
    $('#filter').click((e)=>{
        if($(e.target).hasClass('nav-link')){
            if($(e.target).text() != 'All'){
                table.search($(e.target).text()).draw()
            } else {
                table.search('').draw()
            }

            $('#filter>li>.nav-link').removeClass('active')
            $(e.target).addClass('active')
        }
    })
    
</script>