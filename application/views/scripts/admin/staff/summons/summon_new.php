<script>

    $('[name=summon_date]').datetimepicker({
        format: 'MM/DD/YYYY',
        icons: {
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    })

    $('[name=summon_time]').datetimepicker({
        format: 'h:mm A',
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    })

    function getComplaints(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/complaint/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                response.forEach(el=>(
                    $('select[name=complaint_id]').append(`<option value="${el.id}">${el.complaint_number}</option>`)
                ))
                $('select[name=complaint_id]').selectpicker({
                    style: 'select-with-transition',
                    liveSearch: true,
                })
            }
        });
    }
    getComplaints()

    $('#summon_new').submit(e=>e.preventDefault()).validate({
        rules:{
            complaint_id:{
                required: true
            },
            summon_date:{
                required: true,
            },
            summon_time:{
                required: true
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $('[type=submit]').attr('disabled', true)
            $.ajax({
                type: "POST",
                url: proxy + "/api/summon/create",
                data: {
                    complaint_id: $('[name=complaint_id]').val(),
                    summon_date: $('[name=summon_date]').val(),
                    summon_time: moment($('[name=summon_time]').val(), 'h:mm A').format('HH:mm'),
                },
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        setTimeout(() => {
                            window.location.href = '<?=base_url()?>staff/summon/'+response.data.id
                        }, 1000);
                    }else{
                        $('[type=submit]').attr('disabled', true)

                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        ) 
                    }
                }
            });
        }
    })
</script>