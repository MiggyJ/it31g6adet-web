<script>
    CKEDITOR.replace('body')

    $('#announcement_new').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.body.updateElement()
    }).validate({
        ignore: [],
        rules:{
            title:{
                required: true,
            },
            body:{
                required: true,
            },
            file:{
                required: true
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
            let id = $(element).closest('.tab-pane').attr('id')
            if(!$(`[href="#${id}"]>span`).length){
                $(`[href="#${id}"]`).append(`
                    <span class="badge badge-pill badge-danger px-2">
                        <i class="material-icons m-0" style="font-size: .7rem">priority_high</i>
                    </span>
                `)
            }
        },
        success: function(element) {
            let id = $(element).closest('.tab-pane').attr('id')
            $(`[href="#${id}"]>span`).remove()
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            var form = $('#announcement_new')[0]; 
            var formData = new FormData(form);
            formData.set('body', CKEDITOR.instances.body.getData())
            $('button[type=submit').attr('disabled', true)
            $('input[disabled]').attr('disabled', false)
            $.ajax({
                type: "POST",
                url: proxy + "/api/announcement/create",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        $('input[disabled=false]').attr('disabled', true)
                        $('button[type=submit]').attr('disabled', false)
                        $('#announcement_new').get(0).reset()
                        CKEDITOR.instances.body.setData('')
                        CKEDITOR.instances.body.updateElement()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                }
            });
        }
    })
</script>
