<script>
    <?php if(isset($_GET['mode'])):?>
        setTimeout(() => {
            $('#edit').click()
        }, 1000);
    <?php endif;?>
    CKEDITOR.replace('body')
    let announcement, status
    function fetchAnnouncement(reset= false){
        $.ajax({
            type: "get",
            url: proxy + "/api/announcement/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    $('#title').val(data.title)
                    $('#body').val(data.body)                   
                    $('#created_at').val(data.created_at)
                    $('.img-thumbnail>img').attr('src', `http://localhost:3000/uploads/announcements/${data.file}`)
    
                    if(!reset){
                        $('select').selectpicker({
                            style: 'select-with-transition',
                            width: 'fit'
                        })
                    }else{
                        $('select').attr('disabled', true).selectpicker('refresh')
                        CKEDITOR.instances.body.setData(body)
                        CKEDITOR.instances.body.setReadOnly(true)
                        $(':input[type="file"]').prop('disabled', true);
                    }

                    announcement = data.id
                    status = data.status


                    if(data.status == 'Active'){
                        $('#delete').text('DELETE')
                    }else{
                        $('#delete').text('RESTORE')
                    }
                }
            }
        });
     }
    fetchAnnouncement()

    $('#edit').click(()=>{
        $('[disabled]').attr('disabled', false)
        CKEDITOR.instances.body.setReadOnly(false)
        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted=false){
        $('input').attr('disabled', true)
        if(!submitted){
            fetchAnnouncement(true)
        }

        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        $(`[data-toggle=tab]>span`).remove()

        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }

    $('#cancel').click(resetForm)

    
    let form = $('#announcement_update').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.body.updateElement()
    }).validate({
        ignore: [],
        rules:{
            title:{
                required: true,
            },
            body:{
                required: true,
            }
        },

        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
            let id = $(element).closest('.tab-pane').attr('id')
            if(!$(`[href="#${id}"]>span`).length){
                $(`[href="#${id}"]`).append(`
                    <span class="badge badge-pill badge-danger px-2">
                        <i class="material-icons m-0" style="font-size: .7rem">priority_high</i>
                    </span>
                `)
            }
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            let id = $(element).closest('.tab-pane').attr('id')
            $(`[href="#${id}"]>span`).remove()
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            var form = $('#announcement_update')[0]; 
            var formData = new FormData(form);
            $.ajax({
                type: "PUT",
                url: proxy + "/api/announcement/" + announcement,
                data: formData,
                dataType: "json",
                contentType: false,
                processData: false,
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(null, submitted=true)
                        fetchAnnouncement()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(status == 'Active'){
            Swal.fire({
                title: 'Deactivate Announcement',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/announcement/" + announcement,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchAnnouncement()
                                $.notify(
                                    {
                                        message: response.message
                                    }
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }else{
            Swal.fire({
                title: 'Restore Announcement',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "put",
                        url: proxy + "/api/announcement/restore/" + announcement,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchAnnouncement()
                                $.notify(
                                    { message: response.message },
                                    { type: 'success' }
                                )
                            }else{
                                $.notify(
                                    { message: response.message },
                                    { type: 'danger' }
                                )
                            }
                        }
                    });
                    return
                }
            })
        }
    })
</script>