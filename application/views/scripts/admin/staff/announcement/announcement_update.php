<script>
    CKEDITOR.replace('body')
    let announcement, status
    function fetchAnnouncement(reset= false){
        $.ajax({
            type: "get",
            url: proxy + "/api/announcement/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    $('#title').val(data.title)
                    $('#body').val(data.body)                   
                    $('#created_at').val(data.created_at)
                    $('#img').val(data.file)

                    if(!reset){
                        $('select').selectpicker({
                            style: 'select-with-transition',
                            width: 'fit'
                        })
                    }else{
                        $('select').attr('disabled', true).selectpicker('refresh')
                        CKEDITOR.instances.body.setData(body)
                        CKEDITOR.instances.body.setReadOnly(true)
                    }

                    announcement = data.id
                    status = data.status

                    $('#edit').attr('href', '<?=base_url()?>staff/announcement/edit/'+data.id);

                    if(data.status == 'Active'){
                        $('#delete').text('DELETE')
                    }else{
                        $('#delete').text('RESTORE')
                    }
                }
            }
        });
     }
    fetchAnnouncement()

    

    $('#edit').click(()=>{
        $('[disabled]:not(#created_at)').attr('disabled', false)
        $('select').attr('disabled', false).selectpicker('refresh')
        CKEDITOR.instances.body.setReadOnly(false)
        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted=false){
        if(!submitted){
            fetchAnnouncement(true)
        }
        $('input, textarea').attr('disabled', true)
        
        $('input:submit').addClass('d-none')
        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }
    $('#cancel').click(resetForm)

    
    $('#announcement_update').validate({
        rules:{
            title:{
                required: true,
            },
            body:{
                required: true,
            }
        },
    // let form = $('#announcement_update').validate({
        // ignore: [],
        // errorElement: 'span',highlight: function(element) {
        //     $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        //     let id = $(element).closest('.tab-pane').attr('id')
        //     if(!$(`[href="#${id}"]>span`).length){
        //         $(`[href="#${id}"]`).append(`
        //             <span class="badge badge-pill badge-danger px-2">
        //                 <i class="material-icons m-0" style="font-size: .7rem">priority_high</i>
        //             </span>
        //         `)
        //     }
        // },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            // console.log($('#announcement_update').serialize())
            $.ajax({
                type: "put",
                url: proxy + "/api/announcement/" + announcement,
                data: {
                    title: $('#title').val(),
                    body: CKEDITOR.instances.body.getData()
                },
                // data: $('#announcement_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(null, submitted=true)
                        fetchComplaint()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
                
            });
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(status == 'Active'){
            Swal.fire({
                title: 'Deactivate Announcement',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/announcement/" + announcement,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchAnnouncement()
                                $.notify(
                                    {
                                        message: response.message
                                    }
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }
    })
    
</script>