<script>
    function getResidents(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=owner_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
                $('select[name=owner_id]').selectpicker({
                            style: 'select-with-transition',
                            liveSearch: true,
                            width: 'fit'
                })
            }
        });
    }
    getResidents()

    let form = $('#business_new').validate({
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $.ajax({
                type: "POST",
                url: proxy + "/api/business/create",
                data: $('#business_new').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        setTimeout(() => {
                            window.location.href = '<?=base_url()?>staff/business/'+response.data.id
                        }, 1000);
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })
</script>