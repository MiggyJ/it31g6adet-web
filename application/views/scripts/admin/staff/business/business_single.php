<script>
    let business, status
    function getResidents(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=owner_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
            }
        });
    }
    getResidents()

    function fetchBusiness(reset = false){
        $.ajax({
            type: "get",
            url: proxy + "/api/business/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    //fill up form
                    $('[name=name]').val(data.name)
                    $('[name=contact_number]').val(data.contact_number)
                    $('[name=house_number]').val(data.house_number)
                    $('[name=street]').val(data.street)
                    $('[name=barangay]').val(data.barangay)
                    $('[name=municipality]').val(data.municipality)
                    $('[name=province]').val(data.province)
                    $('[name=owner_id]').val(data.owner_id)
                    $('#status').val(data.status)
                    if(!reset){
                        $('select[name=owner_id]').selectpicker({
                            style: 'select-with-transition',
                            liveSearch: true,
                            width: 'fit'
                        })
                        $('select[name=status]').selectpicker({
                            style: 'select-with-transition',
                            width: 'fit',
                        })
                    }
                    $('select').attr('disabled', true).selectpicker('refresh')

                    business = data.id
                    status = data.status

                    if(data.status == 'Active'){
                        $('#delete').text('DELETE')
                    }else{
                        $('#delete').text('RESTORE')
                    }
                }
            }
        });
    }
    fetchBusiness()

    $('#edit').click(()=>{
        $('[disabled]').attr('disabled', false)
        $('select').attr('disabled', false).selectpicker('refresh')
        
        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted = false){
        if(!submitted){
            fetchBusiness(true)
        }

        $('input').attr('disabled', true)
        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }

    $('#cancel').click(resetForm)

    let form = $('#business_update').validate({
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $.ajax({
                type: "PUT",
                url: proxy + "/api/business/" + business,
                data: $('#business_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(null, submitted=true)
                        fetchBusiness()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(status == 'Active'){
            Swal.fire({
                title: 'Delete Business',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/business/" + business,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchBusiness()
                                $.notify(
                                    {
                                        message: response.message
                                    }
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }else{
            Swal.fire({
                title: 'Restore Business',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "put",
                        url: proxy + "/api/business/restore/" + business,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchBusiness()
                                $.notify(
                                    { message: response.message },
                                    { type: 'success' }
                                )
                            }else{
                                $.notify(
                                    { message: response.message },
                                    { type: 'danger' }
                                )
                            }
                        }
                    });
                    return
                }
            })
        }
    })

</script>