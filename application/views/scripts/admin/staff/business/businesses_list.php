<script>
    let activeTable = $('#active>.table').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/business/datatables',
            xhrFields:{
                withCredentials: true
            }
        },
        drawCallback: function(){
            $('[rel=tooltip]').tooltip()
        },
        columnDefs: [
            {
                targets: [10],
                className: 'dt-body-center'
            }
        ],
        columns:[
            {data: 'name'},
            {data: 'business_owner.first_name', visible: false, searchable: true},
            {data: 'business_owner.last_name', visible: false, searchable: true},
            {data: 'business_owner.full_name', searchable: false, orderable: false},
            {data: 'house_number', visible: false, searchable: true},
            {data: 'street', visible: false, searchable: true},
            {data: 'barangay', visible: false, searchable: true},
            {data: 'municipality', visible: false, searchable: true},
            {data: 'province', visible: false, searchable: true},
            {data: 'full_address', searchable: false, orderable: false},
            {data: null, searchable: false, orderable: false, render: function(data, type, row){
                return (`
                    <div>${row.contact_number != null ? row.contact_number : ''}</div>
                    <div>${row.email != null ? row.email: ''}</div>
                `)
            }},
            {data: 'id', render: (data)=>(`
                <div class="text-center">
                    <a href="http://localhost/front/staff/business/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                    <a href="http://localhost/front/staff/business/${data}?mode=edit" class="btn btn-link btn-warning btn-just-icon edit" rel="tooltip" data-toggle="tooltip" data-placement="left" title="Edit"><i class="material-icons">edit</i></a>
                    <div data-item="${data}" class="btn btn-link btn-danger btn-just-icon remove" rel="tooltip" data-toggle="tooltip" data-placement="left" title="Delete"><i class="material-icons">delete</i></div>
                </div>
            `)}
        ]
    })

    // *DELETE FUNCTION
    $('#activeTable').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-danger')){
            const id = $(e.target.parentElement).data('item')
            if(activeTable.row(this).data().status == 'Active'){
                Swal.fire({
                    title: 'Delete Business',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    confirmButtonColor: '#f44336',
                    preConfirm: ()=>{
                        $.ajax({
                            type: "delete",
                            url: proxy + "/api/business/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    activeTable.draw()
                                    inactiveTable.draw()
                                    $.notify(
                                        {message: response.message},
                                        {type: 'warning'}
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })

    let inactiveTable = $('#trash>.table').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/business/datatablesInactive',
            xhrFields:{
                withCredentials: true
            }
        },
        drawCallback: function(){
            $('[rel=tooltip]').tooltip()
        },
        columnDefs: [
            {
                targets: [10],
                className: 'dt-body-center'
            }
        ],
        columns:[
            {data: 'name'},
            {data: 'business_owner.first_name', visible: false, searchable: true},
            {data: 'business_owner.last_name', visible: false, searchable: true},
            {data: 'business_owner.full_name', searchable: false, orderable: false},
            {data: 'house_number', visible: false, searchable: true},
            {data: 'street', visible: false, searchable: true},
            {data: 'barangay', visible: false, searchable: true},
            {data: 'municipality', visible: false, searchable: true},
            {data: 'province', visible: false, searchable: true},
            {data: 'full_address', searchable: false, orderable: false},
            {data: null, searchable: false, orderable: false, render: function(data, type, row){
                return (`
                    <div>${row.contact_number != null ? row.contact_number : ''}</div>
                    <div>${row.email != null ? row.email: ''}</div>
                `)
            }},
            {data: 'id', render: (data)=>(`
                <div class="text-center">
                        <a href="http://localhost/front/staff/resident/${data}" class="btn btn-link btn-info btn-just-icon like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                        <div data-item="${data}" class="btn btn-link btn-warning btn-just-icon remove" data-toggle="tooltip" data-placement="left" title="Restore"><i class="material-icons">restore</i></div>
                    </div>
            `)}
        ]
    })

    $('#inactiveTable').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-warning')){
            const id = $(e.target.parentElement).data('item')
            if(inactiveTable.row(this).data().status == 'Inactive'){
                Swal.fire({
                    title: 'Restore Business',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    preConfirm: ()=>{
                        $.ajax({
                            type: "put",
                            url: proxy + "/api/business/restore/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    activeTable.draw()
                                    inactiveTable.draw()
                                    $.notify(
                                        {message: response.message},
                                        {type: 'success'}
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })
</script>