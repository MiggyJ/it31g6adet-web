<script>

    let table = $('#active>table').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/ordinance/datatables',
            xhrFields:{
                withCredentials: true
            }
        },
        drawCallback: function(){
            $('[rel=tooltip]').tooltip()
        },
        columnDefs:[
            {
                targets: [3],
                className: 'dt-body-center'
            }
        ],
        columns:[
            {data: 'ordinance_number'},
            {data: 'title', width: '300px'},
            {data: 'ordinance_author.full_name', orderable: false, searchable: false},
            {data: 'date_effective'},
            {data: 'id', render: (data)=>(`
                <div class="text-center">
                    <a href="http://localhost/front/staff/ordinance/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                    <a href="http://localhost/front/staff/ordinance/${data}?mode=edit" class="btn btn-link btn-warning btn-just-icon edit" rel="tooltip" data-toggle="tooltip" data-placement="left" title="Edit"><i class="material-icons">edit</i></a>
                </div>
            `)},
        ]
    })

    $('.table').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-danger')){
            const id = $(e.target.parentElement).data('item')
            if(table.row(this).data().status != 'Inactive'){
                Swal.fire({
                    title: 'Deactivate Ordinance',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    confirmButtonColor: '#f44336',
                    preConfirm: ()=>{
                        $.ajax({
                            type: "delete",
                            url: proxy + "/api/ordinance/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    table.draw()
                                    $.notify(
                                        {
                                            message: response.message
                                        }
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })
</script>