<script>
     CKEDITOR.replace('details')
    function getOfficials(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/official/current",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.data.forEach(el =>{
                    $('select[name=author]').append(`<option value="${el.profile.id}">${el.profile.full_name}</option>`)
                })
                $('select').selectpicker({
                    style: 'select-with-transition',
                    liveSearch: true,
                })
            }
        });
    }
    getOfficials()

    $('[name=date_effective]').datetimepicker({
        format: 'MM/DD/YYYY',
        icons: {
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    })

    let form = $('#ordinance_new').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.details.updateElement()
    }).validate({
        ignore: [],
        rules:{
            details: {
                required: true,
                minlength: 35,
            },
            file:{
                required: true
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
            if(!$(element).closest('.form-group').length){
                $.notify(
                    {message: 'File is required.'},
                    {type: 'danger'}
                )
            }
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            var form = $('#ordinance_new')[0] 
            var formData = new FormData(form)
            $.ajax({
                type: "POST",
                url: proxy + "/api/ordinance/create",
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        window.location.href = '<?=base_url()?>/staff/ordinance/' + response.data.id

                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })
</script>