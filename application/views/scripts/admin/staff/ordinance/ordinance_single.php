<script>
    let ordinance, status, form_date, actual_date
    CKEDITOR.replace('details')
    function getOfficials(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/official/current",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.data.forEach(el =>{
                    $('select[name=author]').append(`<option value="${el.profile.id}">${el.profile.full_name}</option>`)
                })
            }
        });
    }
    getOfficials()

    function fetchOrdinance(reset = false){
        $.ajax({
            type: "GET",
            url: proxy + "/api/ordinance/" + "<?=$id?>",
            dataType: "json",
            success: function (response) {
                if(response.error){
                    $.notify(
                        {message: response.message},
                        {type: 'danger'}
                    )
                }else{
                    let data = response.data
                    $('#fileview').attr('href', `http://localhost:3000/uploads/ordinances/${data.file}`)
                    $('[name=ordinance_number]').val(data.ordinance_number)
                    $('[name=author]').val(data.author)
                    $('[name=title]').val(data.title)
                    $('[name=date_effective]').val(data.date_effective)
                    if(!reset){
                        $('#details').val(data.details)
                        $('select').selectpicker({
                            style: 'select-with-transition',
                            liveSearch: true,
                            width: 'fit'
                        })
                    }
                    $('select').attr('disabled', true).selectpicker('refresh')
                    CKEDITOR.instances.details.setData(data.details)
                    CKEDITOR.instances.details.setReadOnly(true)

                    ordinance = data.id
                    actual_date = data.date_effective
                    status = data.status
                    form_date = `${new Date(data.date_effective).getMonth() + 1}/${new Date(data.date_effective).getDate()}/${new Date(data.date_effective).getFullYear()}`

                }
            }
        });
    }
    fetchOrdinance()

    $('#edit').click(()=>{
        $('[disabled]:not([name=ordinance_number])').attr('disabled', false)
        $('select').attr('disabled', false).selectpicker('refresh')
        CKEDITOR.instances.details.setReadOnly(false)
        $('[name=date_effective]').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        })

        $('[name=date_effective]').val(form_date)
        $('input:submit, #cancel, #change_file').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted = false){
        if(!submitted){
            fetchOrdinance(true)
        }
        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        $('[name=date_effective]').val(actual_date)
        $('#change_file').val(null)
        $('input:submit, #cancel, #change_file').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }

    $('#cancel').click(resetForm)

    let form = $('#ordinance_update').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.details.updateElement()
    }).validate({
        ignore: [],
        rules:{
            details: {
                required: true,
                minlength: 35,
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            var form = $('#ordinance_update')[0] 
            var formData = new FormData(form)
            $.ajax({
                type: "PUT",
                url: proxy + "/api/ordinance/" + ordinance,
                data: formData,
                contentType: false,
                processData: false,
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(null, submitted=true)
                        fetchOrdinance()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(status == 'Active'){
            Swal.fire({
                title: 'Delete Ordinance',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/ordinance/" + ordinance, //delete
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchComplaint()
                                $.notify(
                                    {
                                        message: response.message
                                    }
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }
    })
</script>