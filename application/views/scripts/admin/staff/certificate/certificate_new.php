<script>
    let temp, certificate

    function getResidents(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=owner_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
                $('[name=type]').selectpicker({
                    style: 'select-with-transition',
                })
                $('[name=owner_id]').selectpicker({
                    style: 'select-with-transition',
                    liveSearch: true
                })
            }
        });
    }
    getResidents()

    function resetForm(){
        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        $('select').val(null).selectpicker('refresh')
    }

    $('[name=type]').on('changed.bs.select', (e)=>{
        if($(e.currentTarget).val() == 'Business'){
            $('[name=business_name]').closest('.row').removeClass('d-none')
            $('[name=business_address]').closest('.row').removeClass('d-none')
            $('[name=business_name], [name=business_address]').attr('disabled', false)
        }else{
            $('[name=business_name]').closest('.row').addClass('d-none')
            $('[name=business_address]').closest('.row').addClass('d-none')
            $('[name=business_name], [name=business_address]').attr('disabled', true)
        }
    })

    let form = $('#certificate_new').submit(e=>e.preventDefault()).validate({
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            submit()
            setTimeout(() => {
                if($('[name=approved]')[0].checked && temp){
                    approve(temp)
                    $('[name=approved]')[0].checked = false
                }
                $('#certificate_new').get(0).reset()
                $('[name=business_name]').closest('.row').addClass('d-none')
                $('[name=business_address]').closest('.row').addClass('d-none')
                $('[name=business_name], [name=business_address]').attr('disabled', true)
            }, 100);
            temp = undefined
        }
    })


    function submit(){
        $.ajax({
            type: "POST",
            url: proxy + "/api/certificate/request",
            data: $('#certificate_new').serialize(),
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                if(!response.error){
                    resetForm()
                    if(!$('[name=approved]')[0].checked){
                        $.notify(
                            {
                                message: `${response.message} Click to view.`,
                                url: '<?=base_url()?>/staff/certificate/'+response.data.id,
                                target: '_blank'
                            },
                            {type: 'success'}
                        )
                    }
                    temp = certificate = response.data.id
                }else{
                    $.notify(
                        {message: response.message},
                        {type: 'danger'}
                    )
                    temp = false
                }
            }
        });
    }

    function approve(id){
        $.ajax({
            type: "put",
            url: proxy + "/api/certificate/approve/" + id,
            data: "json",
            xhrFields:{
                withCredentials: true,
            },
            xhr: function(){
                $('.modal').modal('show')
                var xhr = new window.XMLHttpRequest()
                xhr.upload.addEventListener('progress', (evt) => {
                    if (evt.lengthComputable) {
                        var percentComplete = evt.loaded / evt.total * 100
                        $(".progress-bar").css("width", percentComplete + "%")
                    }
                })
                return xhr
            },
            success: function (response) {
                $('.modal').modal('hide')
                if(!response.error){
                    $.notify(
                        {
                            message: `${response.message} Click to view.`,
                            url: '<?=base_url()?>/staff/certificate/'+certificate,
                            target: '_blank'
                        },
                        {type: 'success'}
                    )
                }else{
                    response.message.forEach(el => {
                        $.notify(
                            {
                                message: el
                            },
                            {
                                type: 'danger'
                            }
                        )
                    })
                }
            }
        });
    }
</script>