<script>
    let requestTable = $('#request>table').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/certificate/datatablesRequest',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns:[
            {data: 'certificate_number'},
            {data: 'certificate_owner.first_name', visible: false, searchable: true},
            {data: 'certificate_owner.last_name', visible: false, searchable: true},
            {data: 'certificate_owner.full_name', orderable: false},
            {data: 'type'},
            {data: 'id', render: (data)=>(`
                <div class="text-center">
                    <a href="http://localhost/front/staff/certificate/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                    <div data-item="${data}" class="btn btn-link btn-success btn-just-icon remove" data-toggle="tooltip" data-placement="left" title="Approve"><i class="material-icons">done</i></div>
                    <div data-item="${data}" class="btn btn-link btn-danger btn-just-icon remove" rel="tooltip" data-toggle="tooltip" data-placement="left" title="Reject"><i class="material-icons">remove_circle</i></div>
                </div>
            `)},
        ]
    })

    let approvedTable = $('#approved>table').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/certificate/datatablesAccept',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns:[
            {data: 'certificate_number'},
            {data: 'certificate_owner.first_name', visible: false, searchable: true},
            {data: 'certificate_owner.last_name', visible: false, searchable: true},
            {data: 'certificate_owner.full_name', orderable: false, searchable: true},
            {data: 'type'},
            {data: 'id', render: (data)=>(`
                <div class="text-center">
                    <a href="http://localhost/front/staff/certificate/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                </div>
            `)},
        ]
    })


    let trashTable = $('#trash>table').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/certificate/datatablesReject',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns:[
            {data: 'certificate_number'},
            {data: 'certificate_owner.first_name', visible: false, searchable: true},
            {data: 'certificate_owner.last_name', visible: false, searchable: true},
            {data: 'certificate_owner.full_name', orderable: false},
            {data: 'type'},
            {data: 'id', render: (data)=>(`
                <div class="text-center">
                    <a href="http://localhost/front/staff/certificate/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                </div>
            `)},
        ]
    })

    $('#request').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-success')){
            const id = $(e.target.parentElement).data('item')
            Swal.fire({
                title: 'Approve Certificate',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                preConfirm: ()=>{
                    $.ajax({
                        type: "put",
                        url: proxy + "/api/certificate/approve/" + id,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        xhr: function(){
                            $('.modal').modal('show')
                            var xhr = new window.XMLHttpRequest()
                            xhr.upload.addEventListener('progress', (evt) => {
                                if (evt.lengthComputable) {
                                    var percentComplete = evt.loaded / evt.total * 100
                                    $(".progress-bar").css("width", percentComplete + "%")
                                }
                            })
                            return xhr
                        },
                        success: function (response) {
                            $('.modal').modal('hide')
                            if(!response.error){
                                requestTable.draw()
                                approvedTable.draw()
                                trashTable.draw()
                                $.notify(
                                    {message: response.message},
                                    {type: 'success'}
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }else if($(e.target.parentElement).hasClass('btn-danger')){
            const id = $(e.target.parentElement).data('item')
            Swal.fire({
                title: 'Delete Request?',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/certificate/reject/" + id,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                requestTable.draw()
                                approvedTable.draw()
                                trashTable.draw()
                                $.notify(
                                    {message: response.message},
                                    {type: 'warning'}
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }

    })

    // *Filter buttons
    $('#filter').click((e)=>{
        if($(e.target).hasClass('nav-link')){
            if($(e.target).text() != 'All'){
                requestTable.search($(e.target).text()).draw()
                approvedTable.search($(e.target).text()).draw()
                trashTable.search($(e.target).text()).draw()
            } else {
                requestTable.search('').draw()
                approvedTable.search('').draw()
                trashTable.search('').draw()
            }

            $('#filter>li>.nav-link').removeClass('active')
            $(e.target).addClass('active')
        }
    })
</script>