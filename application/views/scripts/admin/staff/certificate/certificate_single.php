<script>
    let certificate, status
    function fetchCertificate(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/certificate/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                        $.notify(
                            { message: response.message },
                            {type: 'danger'}
                        )
                }else{
                    let data = response.data
                    $('#certificate_number').text(data.certificate_number)
                    $('#certificate_type').text(data.type)
                    $('#status').text(data.status)
                    $('#resident').attr('href', `http://localhost/front/staff/resident/${data.owner_id}`).text(data.certificate_owner.full_name)
                    $('#request').text(data.created_at)

                    if(data.status == 'Rejected'){
                        $('.card-footer').addClass('d-none')
                    }else if(data.status == 'Approved'){
                        $('#file').removeClass('d-none')
                        $('#approve, #delete').addClass('d-none')

                        
                        $('#approved_by').text(data.user_approved.profile.full_name)
                        $('#date_approved').text(data.date_approved)
                        $('#expiry').text(data.valid_until)
                        $('#file').attr('href', `http://localhost:3000/uploads/certificates/${data.type.toLowerCase()}/${data.file}`)
                    }

                    certificate = data.id
                    status = data.status
                }
            }
        });
    }

    fetchCertificate()


    $('#approve').click(()=>{
        Swal.fire({
            title: 'Approve Certificate',
            text: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            preConfirm: ()=>{
                $.ajax({
                    type: "put",
                    url: proxy + "/api/certificate/approve/" + certificate,
                    data: "json",
                    xhrFields:{
                        withCredentials: true,
                    },
                    xhr: function(){
                        $('.modal').modal('show')
                        var xhr = new window.XMLHttpRequest()
                        xhr.upload.addEventListener('progress', (evt) => {
                            if (evt.lengthComputable) {
                                var percentComplete = evt.loaded / evt.total * 100
                                $(".progress-bar").css("width", percentComplete + "%")
                            }
                        })
                        return xhr
                    },
                    success: function (response) {
                        $('.modal').modal('hide')
                        if(!response.error){
                            fetchCertificate()
                            $.notify(
                                {message: response.message},
                                {type: 'success'}
                            )
                        }else{
                            response.message.forEach(el => {
                                $.notify(
                                    {
                                        message: el
                                    },
                                    {
                                        type: 'danger'
                                    }
                                )
                            })
                        }
                    }
                });
                return
            }
        })
    })

    $('#reject').click(()=>{
        Swal.fire({
            title: 'Delete Request?',
            text: 'Are you sure?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes',
            showLoaderOnConfirm: true,
            confirmButtonColor: '#f44336',
            preConfirm: ()=>{
                $.ajax({
                    type: "delete",
                    url: proxy + "/api/certificate/reject/" + certificate,
                    data: "json",
                    xhrFields:{
                        withCredentials: true,
                    },
                    success: function (response) {
                        if(!response.error){
                            fetchCertificate()
                            $.notify(
                                {message: response.message},
                                {type: 'warning'}
                            )
                        }else{
                            response.message.forEach(el => {
                                $.notify(
                                    {
                                        message: el
                                    },
                                    {
                                        type: 'danger'
                                    }
                                )
                            })
                        }
                    }
                });
                return
            }
        })
    })
</script>