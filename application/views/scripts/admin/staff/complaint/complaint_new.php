<script>
    CKEDITOR.replace('details')
    
    $('input[name="complaint_date"]').datetimepicker({
        format: 'MM/DD/YYYY',
        icons: {
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    })

    //FUNCTIONS FOR OPTIONS
    function getSelectOptions(){

        //* GET RESIDENTS
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=complainant_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                    $('select[name=defendant_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
                $('select[name=complainant_id], select[name=defendant_id]').selectpicker({
                    style: 'select-with-transition',
                    liveSearch: true
                })
            }
        });

    }


    getSelectOptions()

    $('#complaint_new').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.details.updateElement()
    }).validate({
        ignore: [],
        rules:{
            complainant_id:{
                required: true,
            },
            defendant_id:{
                required: true,
            },
            complaint_date:{
                required: true,
            },
            details:{
                required: true,
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(e){
            $('button[type=submit').attr('disabled', true)
            $('input[disabled]').attr('disabled', false)
            $.ajax({
                type: "POST",
                url: proxy + "/api/complaint/create",
                data: {
                    complainant_id: $('#complainant_id').val(),
                    defendant_id: $('#defendant_id').val(),
                    complaint_date: $('[name=complaint_date]').val(),
                    details: CKEDITOR.instances.details.getData()
                },
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        $('input[disabled=false]').attr('disabled', true)
                        $('button[type=submit]').attr('disabled', false)
                        $('#complaint_new').get(0).reset()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                }
            });
        }
    })
</script>