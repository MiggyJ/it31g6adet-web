<script>

    <?php if(isset($_GET['mode'])):?>
        setTimeout(() => {
            $('#edit').click()
        }, 1000);
    <?php endif;?>
    CKEDITOR.replace('details')
    let complaint, complainant, defendant, isActive, complaint_date, form_date, details

    function getResidents(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=defendant_id], select[name=complainant_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
            }
        });
    }
    getResidents()

    function fetchComplaint(reset = false){
        $.ajax({
            type: "get",
            url: proxy + "/api/complaint/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    //fill up form
                    $('#complaint_number').val(data.complaint_number)
                    $('#complainant_id').val(data.complaint_by.id)
                    $('#defendant_id').val(data.complaint_defendant.id)
                    $('#complaint_date').val(data.complaint_date)
                    //*Dates for form and view
                    complaint_date = data.complaint_date
                    form_date = `${new Date(data.complaint_date).getMonth() + 1}/${new Date(data.complaint_date).getDate()}/${new Date(data.complaint_date).getFullYear()}`
                    $('#status').val(data.status)
                    if(!reset){
                        $('#details').val(data.details)
                        $('select[name=defendant_id], select[name=complainant_id]').selectpicker({
                            style: 'select-with-transition',
                            liveSearch: true,
                            width: 'auto'
                        })
                    }else
                        CKEDITOR.instances.details.setReadOnly(true)
                    $('select').attr('disabled', true).selectpicker('refresh')
                    CKEDITOR.instances.details.setData(data.details)
                    
                    if(data.meetings.length){
                        $('#empty').hide()
                    }

                    data.meetings.forEach((el)=>{
                        let row = 
                        `
                            <tr>
                                <td align="center">${el.summon_date}</td>
                                <td align="center">${moment(el.summon_time, 'H:mm A').format('H:mm A')}</td>
                                <td>${el.mediator.profile.full_name}</td>
                                <td>${el.status}</td>
                                <td class="text-center">
                                <a href="http://localhost/front/staff/summon/${el.id}" class="btn btn-info like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i> View</a>
                                </td>
                            </tr>
                        `
                        $('tbody').append(row)
                        return
                    })

                    complaint = data.id
                    isActive = data.isActive
                    complainant = data.complaint_by.id
                    defendant = data.complaint_defendant.id
                    details = data.details

                    if(data.isActive){
                        $('#delete').text('DELETE')
                    }else{
                        $('#delete').text('RESTORE')
                    }
                }
            }
        });
    }
    fetchComplaint()

    $('#edit').click(()=>{
        $('[disabled]:not(#complaint_number, #status)').attr('disabled', false)
        CKEDITOR.instances.details.setReadOnly(false)
        $('[name=complaint_date]').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        })
        $('[name=complaint_date]').val(form_date)
        $('#defendant_id, #complainant_id').attr('disabled', false).selectpicker('refresh')
        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted = false){
        $('[name=complaint_date]').val(complaint_date)
        if(!submitted){
            fetchComplaint(true)
        }
        $('input').attr('disabled', true)

        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }

    $('#cancel').click(resetForm)

    let form = $('#complaint_update').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.details.updateElement()
    }).validate({
        ignore: [],
        rules:{
            complainant_id:{
                required: true,
            },
            defendant_id:{
                required: true
            },
            complaint_date:{
                required: true,
            },
            details: {
                required: true,
                minlength: 10
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $.ajax({
                type: "PUT",
                url: proxy + "/api/complaint/" + complaint,
                data: $('#complaint_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(null, submitted=true)
                        fetchComplaint()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(isActive == true){
            Swal.fire({
                title: 'Delete Complaint',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/complaint/" + complaint, //delete
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchComplaint()
                                $.notify(
                                    {
                                        message: response.message
                                    }
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }else{
            Swal.fire({
                title: 'Restore Complaint',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                preConfirm: ()=>{
                    $.ajax({
                        type: "put",
                        url: proxy + "/api/complaint/restore/" + complaint,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchComplaint()
                                $.notify(
                                    {message: response.message},
                                    {type: 'warning'}
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }
    })
    
</script>