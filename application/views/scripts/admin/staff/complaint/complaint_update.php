<script>
CKEDITOR.replace('details')
$('input[name="birth_date"]').datetimepicker({
    format: 'DD/MM/YYYY',
    icons: {
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    }
})
let complaint, status

    //TODO - jquery validate

    function getComplaint(){
        $.ajax({
            type: "get",
            url: proxy + "/api/complaint/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    //complainant
                    $('input[name="first_name"]').val(data.complaint_by.first_name)
                    $('input[name="middle_name"]').val(data.complaint_by.middle_name)
                    $('input[name="last_name"]').val(data.complaint_by.last_name)
                    //defendant
                    $('input[name="complaint_defendant.first_name"]').val(data.complaint_defendant.first_name)
                    $('input[name="complaint_defendant.middle_name"]').val(data.complaint_defendant.middle_name)
                    $('input[name="complaint_defendant.last_name"]').val(data.complaint_defendant.last_name)

                    $('input[name="complaint_date"]').val(new Date(data.complaint_date).toLocaleDateString())
                    $('input[name="details"]').val(data.details)

                    $('select').selectpicker({
                        style: 'select-with-transition'
                    })


                    complaint = data.id
                    status = data.status
                }
            }
        });
    }
    getComplaint()

    $('#complaint_update').validate({
        rules:{
            first_name:{
                required: true,
            },
            last_name:{
                required: true,
            },
            first_name:{
                required: true,
            },
            last_name:{
                required: true,
            },
            complaint_date:{
                required: true,
            },
            details:{
                requred: true,
            }
        },

        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(e){
            $.ajax({
                type: "put",
                url: proxy + "/api/complaint/" + complaint,
                data: $('#complaint_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        getResident()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })

</script>