<script>
    <?php if(isset($_GET['mode'])):?>
        setTimeout(() => {
            $('#edit').click()
        }, 1000);
    <?php endif;?>

    CKEDITOR.replace('details')
    let relief_operation, status, form_date, details
    
    function fetchReliefOperation(reset = false){
        $.ajax({
            type: "get",
            url: proxy + "/api/relief_operation/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },
                        {
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    $('#sponsor').val(data.sponsor)
                    $('#operation_date').val(data.operation_date)
                    $('#details').val(data.details)
                    $('#status').val(data.status)
                    $('#recipient_id').val(data.recipients.id)
                    operation_date = data.operation_date  
                    form_date = `${new Date(data.operation_date).getMonth() + 1}/${new Date(data.operation_date).getDate()}/${new Date(data.operation_date).getFullYear()}`
                    if(!reset){
                        $('[name=status').selectpicker({
                            style: 'select-with-transition',
                            width: 'fit'
                        })
                    }else
                        CKEDITOR.instances.details.setReadOnly(true)
                    $('select:not([name=recipient_id])').attr('disabled', true).selectpicker('refresh')
                    CKEDITOR.instances.details.setData(details)
                    
                    relief_operation = data.id
                    status = data.status
                    details = data.details

                    if(data.status != 'Inactive'){
                        $('#delete').text('DELETE')
                    }else{
                        $('#delete').text('RESTORE')
                    }
                }
            }
        });
     }
    fetchReliefOperation()

    $('#edit').click(()=>{
        $('[disabled]:not(#relief_recipient)').attr('disabled', false)
        $('select').attr('disabled', false).selectpicker('refresh')
        CKEDITOR.instances.details.setReadOnly(false)
        $('[name=operation_date]').val(form_date)
        $('[name=operation_date]').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        })

        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted=false){
        if(!submitted){
            fetchReliefOperation(true)
        }
        $('[name=operation_date]').attr('disabled', true)
        $('[name=summon_date]').val(operation_date)

        $('input').attr('disabled', true)

        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }

    $('#cancel').click(resetForm)

    
    let form = $('#relief_operation_update').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.details.updateElement()
    }).validate({
        ignore: [],
        rules:{
            sponsor:{
                required: true,
            },
            operation_date:{
                required: true,
            },
            details:{
                required: true,
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $.ajax({
                type: "PUT",
                url: proxy + "/api/relief_operation/" + relief_operation,
                data: $('#relief_operation_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(null, submitted=true)
                        fetchReliefOperation()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(status == 'Active'){
            Swal.fire({
                title: 'Deactivate Relief Operation',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/relief_operation/" + relief_operation,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchReliefOperation()
                                $.notify(
                                    { message: response.message },
                                    { type: 'success' }
                                )
                            }else{
                                $.notify(
                                    { message: response.message },
                                    { type: 'danger' }
                                )
                            }
                        }
                    });
                    return
                }
            })
        }else{
            Swal.fire({
                title: 'Restore Relief Operation',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "put",
                        url: proxy + "/api/relief_operation/restore/" + relief_operation,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchReliefOperation()
                                $.notify(
                                    { message: response.message },
                                    { type: 'success' }
                                )
                            }else{
                                $.notify(
                                    { message: response.message },
                                    { type: 'danger' }
                                )
                            }
                        }
                    });
                    return
                }
            })
        }
    })

    // RELIEF RECIPIENTS

    $('.card-header').click(function (e) { 
        e.preventDefault();
        setTimeout(() => {
            if($('#relief_recipient').hasClass('active')){
                $('#form_footer').addClass('d-none')
                $('#recipient_footer').removeClass('d-none')
            }else{
                $('#form_footer').removeClass('d-none')
                $('#recipient_footer').addClass('d-none')
            }   
        }, 10);
    });

    function getSelectOptions(){
    //* GET RESIDENTS
    $.ajax({
        type: "GET",
        url: proxy + "/api/resident/findAll",
        dataType: "json",
        xhrFields:{
            withCredentials: true,
        },
        success: function (response) {
            response.forEach(el =>{
                $('select[name=recipient_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
            })
            $('select[name=recipient_id]').selectpicker({
                style: 'select-with-transition',
                liveSearch: true
            })
        }
    });
    }
    getSelectOptions()

    let recipient_table
    function fetchReliefRecipients(){
       recipient_table = $('#recipient_table').DataTable({
            serverSide: true,
            processing: true,
            language: {
                processing: 'Fetching Data...'
            },
            ajax: {
                url: proxy + '/api/relief_recipient/datatables?operation_id='+relief_operation,
                xhrFields:{
                    withCredentials: true
                }
            },
            drawCallback: function(){
                $('[rel=tooltip]').tooltip()
            },
            columns: [
                {data: 'recipient_profile.first_name', visible: false},
                {data: 'recipient_profile.last_name', visible: false},
                {data: 'recipient_profile.full_name', searchable: false, orderable: false},
                {data: 'recipient_profile.personal_contact_number'},
                {data: 'item_received'},
                {data: 'id', render: (data)=>(`
                    <div class="text-center">
                        <div data-item="${data}" class="btn btn-link btn-danger btn-just-icon remove" rel="tooltip" data-toggle="tooltip" data-placement="left" title="Delete"><i class="material-icons">delete</i></div>
                    </div>
                `)},
            ]
        })
    }
    setTimeout(() => {
        fetchReliefRecipients()
    }, 350);


    $('#recipient_new').submit(e=>{
        e.preventDefault()
            $.ajax({
                type: "POST",
                url: proxy + "/api/relief_recipient/create",
                data: {
                    recipient_id: $('[name=recipient_id]').val(),
                    operation_id: relief_operation,
                    item_received: $('#item_received').val()
                },
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )      
                        $('#recipient_new').get(0).reset()
                        $("#recipientModal").modal('hide')
                        recipient_table.draw()
                        $('[name=recipient_id]').selectpicker('refresh')                  
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                }
            });
    })

    $('#recipient_table').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-danger')){
            const id = $(e.target.parentElement).data('item')
            if(recipient_table.row(this).data().status != 'Inactive'){
                Swal.fire({
                    title: 'Remove Relief Recipient?',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    confirmButtonColor: '#f44336',
                    preConfirm: ()=>{
                        $.ajax({
                            type: "delete",
                            url: proxy + "/api/relief_recipient/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    recipient_table.draw()
                                    $.notify(
                                        {
                                            message: response.message
                                        }
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })
    
</script>