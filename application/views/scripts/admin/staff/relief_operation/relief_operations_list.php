<script>
   let activeTable = $('#activeTable').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/relief_operation/datatablesActive',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs:[
            {
                targets: 1,
                className: 'dt-body-center'
            }
        ],
        columns: [
            {data: 'sponsor'},
            {data: 'operation_date'},
            {
                data: 'id', render: (data)=>(`
                    <div class="text-center">
                    <a href="http://localhost/front/staff/relief_operation/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="top" title="View"><i class="material-icons">visibility</i></a>
                    <a href="http://localhost/front/staff/relief_operation/${data}" class="btn btn-link btn-warning btn-just-icon edit" rel="tooltip" data-toggle="tooltip" data-placement="top" title="Edit"><i class="material-icons">edit</i></a>
                    <div data-item="${data}" class="btn btn-link btn-danger btn-just-icon remove" data-toggle="tooltip" data-placement="left" title="Delete"><i class="material-icons">delete</i></div>
                    </div>    
                `)
            },
        ]
    })

   // DELETE FUNCTION
   $('#activeTable').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-danger')){
            const id = $(e.target.parentElement).data('item')
            if(activeTable.row(this).data().status == 'Active'){
                Swal.fire({
                    title: 'Deactivate Relief Operation',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    confirmButtonColor: '#f44336',
                    preConfirm: ()=>{
                        $.ajax({
                            type: "delete",
                            url: proxy + "/api/relief_operation/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    activeTable.draw()
                                    inactiveTable.draw()
                                    $.notify(
                                        {message: response.message},
                                        {type: 'warning'}                                    
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })

    let inactiveTable = $('#inactiveTable').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/relief_operation/datatablesInactive',
            xhrFields:{
                withCredentials: true
            }
        },
        columns: [
            {data: 'sponsor'},
            {data: 'operation_date'},
            {
                data: 'id', render: (data)=>(`
                    <div class="text-center">
                    <a href="http://localhost/front/staff/relief_operation/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="top" title="View"><i class="material-icons">visibility</i></a>
                    <div data-item="${data}" class="btn btn-link btn-warning btn-just-icon remove" data-toggle="tooltip" data-placement="left" title="Restore"><i class="material-icons">restore</i></div>
                    </div>    
                `)
            },
        ]
    })

    // RESTORE FUNCTION
    $('#inactiveTable').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-warning')){
            const id = $(e.target.parentElement).data('item')
            if(inactiveTable.row(this).data().status == 'Inactive'){
                Swal.fire({
                    title: 'Restore Relief Operation',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    preConfirm: ()=>{
                        $.ajax({
                            type: "put",
                            url: proxy + "/api/relief_operation/restore/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    activeTable.draw()
                                    inactiveTable.draw()
                                    $.notify(
                                        {message: response.message},
                                        {type: 'warning'}
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })
</script>