<script>
    CKEDITOR.replace('details')

    $('[name=operation_date]').datetimepicker({
        format: 'MM/DD/YYYY',
        icons: {
            date: "fa fa-calendar",
            up: "fa fa-chevron-up",
            down: "fa fa-chevron-down",
            previous: 'fa fa-chevron-left',
            next: 'fa fa-chevron-right',
            today: 'fa fa-screenshot',
            clear: 'fa fa-trash',
            close: 'fa fa-remove'
        }
    })


    $('#relief_operation_new').submit(e=>{
        e.preventDefault()
        CKEDITOR.instances.details.updateElement()
    }).validate({
        ignore: [],
        rules:{
            sponsor:{
                required: true
            },
            operation_date:{
                required: true,
            },
            details:{
                required: true
            }
        },
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $('[type=submit]').attr('disabled', true)
            $.ajax({
                type: "POST",
                url: proxy + "/api/relief_operation/create",
                data: $('#relief_operation_new').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        setTimeout(() => {
                            window.location.href = '<?=base_url()?>/staff/relief_operation/'+response.data.id
                        }, 1000);
                    }else{
                        $('[type=submit]').attr('disabled', true)

                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        ) 
                    }
                }
            });
        }
    })
</script>