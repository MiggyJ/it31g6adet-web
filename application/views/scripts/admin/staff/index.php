<script>

    function fillDashboard(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/dashboard/staff",
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(!response.error){
                    let data = response.data
                    $('#certificates').text(data.certificates)
                    $('#blotters').text(data.blotters)
                    $('#complaints').text(data.complaints)
                    $('#summons').text(data.summons)
                    $('#residents').text(data.residents)
                    $('#relief_operations').text(data.relief_operations)
                    $('#businesses').text(data.businesses)
                    $('#ordinances').text(data.ordinances)
                    $('#male').text(data.male)
                    $('#female').text(data.female)
                    $('#nonbinary').text(data.nonbinary)

                    let complaints_chart = new Chart($('#complaints_chart'),{
                        type: 'pie',
                        data: {
                            labels: ['Unresolved', 'Resolved'],
                            datasets: [
                                {
                                    data: [
                                        data.complaints_unresolved,
                                        data.complaints_resolved
                                    ],
                                    backgroundColor:[
                                        '#ff9800',
                                        '#00bcd4'
                                    ]
                                }
                            ]
                        }
                    })
                }
            }
        });
    }
    fillDashboard()
</script>