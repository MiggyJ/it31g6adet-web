<script>

$('input[name="birth_date"]').datetimepicker({
    format: 'DD/MM/YYYY',
    icons: {
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    }
})
let resident, status

    function getResident(){
        $.ajax({
            type: "get",
            url: proxy + "/api/resident/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    $('input[name="first_name"]').val(data.first_name)
                    $('input[name="middle_name"]').val(data.middle_name)
                    $('input[name="last_name"]').val(data.last_name)
                    $('input[name="birth_date"]').val(new Date(data.birth_date).toLocaleDateString())
                    $('input[name="personal_contact_number"]').val(data.personal_contact_number)
                    $('input[name="current_house_number"]').val(data.current_house_number)
                    $('input[name="current_street"]').val(data.current_street)
                    $(`select[name="civil_status"]>option[value="${data.civil_status}"]`).attr('selected', true)
                    $(`select[name="gender"]>option[value="${data.gender}"]`).attr('selected', true)

                    $('select').selectpicker({
                        style: 'select-with-transition'
                    })

                    data.benefits.forEach(el=>(
                        $(`input[name="benefits"][value="${el}"]`).attr('checked', true)
                    ))

                    resident = data.id
                    status = data.status
                }
            }
        });
    }
    getResident()

    $('#resident_update').validate({
        rules:{
            first_name:{
                required: true,
            },
            last_name:{
                required: true,
            },
            birth_date:{
                required: true,
            },
            personal_contact_number:{
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10,
            },
            current_house_number:{
                required: true,
            },
            current_street:{
                required: true,
            },
            civil_status:{
                required: true,
            },
            gender:{
                required: true
            }
        },
        messages:{
            personal_contact_number:{
                minlength: 'Enter 10-digit number after +63',
                maxlength: 'Enter 10-digit number after +63'
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(e){
            $.ajax({
                type: "put",
                url: proxy + "/api/resident/" + resident,
                data: $('#resident_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        getResident()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })

</script>