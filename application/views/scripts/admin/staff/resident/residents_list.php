<script>
    let activeTable = $('#activeTable').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/resident/datatablesActive',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs:[
            {
                targets: 3,
                className: 'dt-body-center'
            },{
                targets: 10,
                className: 'dt-body-right'
            }
        ],
        columns: [
            {data: 'first_name', visible: false, searchable: true},
            {data: 'last_name', visible: false, searchable: true},
            {data: 'full_name', orderable: false},
            {data: 'age', orderable: false},
            {data: 'current_house_number', visible: false, searchable: true},
            {data: 'current_street', visible: false, searchable: true},
            {data: 'current_barangay', visible: false, searchable: true},
            {data: 'current_municipality', visible: false, searchable: true},
            {data: 'current_province', visible: false, searchable: true},
            {data: 'current_address', orderable: false},
            {data: 'personal_contact_number'},
            {
                data: 'id', render: (data)=>(
                    `
                    <div class="text-center">
                        <a href="http://localhost/front/staff/resident/${data}" class="btn btn-link btn-info btn-just-icon like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                        <a href="http://localhost/front/staff/resident/${data}?mode=edit" class="btn btn-link btn-warning btn-just-icon edit" data-toggle="tooltip" data-placement="left" title="Edit"><i class="material-icons">edit</i></a>
                        <div data-item="${data}" class="btn btn-link btn-danger btn-just-icon remove" data-toggle="tooltip" data-placement="left" title="Delete"><i class="material-icons">delete</i></div>
                    </div>
                    `
                )
            },
        ]
    })

    // DELETE FUNCTION
    $('#activeTable').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-danger')){
            const id = $(e.target.parentElement).data('item')
            if(activeTable.row(this).data().status == 'Active'){
                Swal.fire({
                    title: 'Deactivate Resident',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    confirmButtonColor: '#f44336',
                    preConfirm: ()=>{
                        $.ajax({
                            type: "delete",
                            url: proxy + "/api/resident/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    activeTable.draw()
                                    inactiveTable.draw()
                                    $.notify(
                                        {message: response.message},
                                        {type: 'warning'}
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })

    let inactiveTable = $('#inactiveTable').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/resident/datatablesInactive',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columnDefs:[
            {
                targets: 3,
                className: 'dt-body-center'
            },{
                targets: 10,
                className: 'dt-body-right'
            }
        ],
        columns: [
            {data: 'first_name', visible: false, searchable: true},
            {data: 'last_name', visible: false, searchable: true},
            {data: 'full_name', orderable: false},
            {data: 'age', orderable: false},
            {data: 'current_house_number', visible: false, searchable: true},
            {data: 'current_street', visible: false, searchable: true},
            {data: 'current_barangay', visible: false, searchable: true},
            {data: 'current_municipality', visible: false, searchable: true},
            {data: 'current_province', visible: false, searchable: true},
            {data: 'current_address', orderable: false},
            {data: 'personal_contact_number'},
            {
                data: 'id', render: (data)=>(
                    `
                    <div class="text-center">
                        <a href="http://localhost/front/staff/resident/${data}" class="btn btn-link btn-info btn-just-icon like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                        <div data-item="${data}" class="btn btn-link btn-warning btn-just-icon remove" data-toggle="tooltip" data-placement="left" title="Restore"><i class="material-icons">restore</i></div>
                    </div>
                    `
                )
            },
        ]
    })

    // Restore Function
    $('#inactiveTable').on('click', 'tr', function (e){
        if($(e.target.parentElement).hasClass('btn-warning')){
            const id = $(e.target.parentElement).data('item')
            if(inactiveTable.row(this).data().status == 'Inactive'){
                Swal.fire({
                    title: 'Restore Resident',
                    text: 'Are you sure?',
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    showLoaderOnConfirm: true,
                    preConfirm: ()=>{
                        $.ajax({
                            type: "put",
                            url: proxy + "/api/resident/restore/" + id,
                            data: "json",
                            xhrFields:{
                                withCredentials: true,
                            },
                            success: function (response) {
                                if(!response.error){
                                    activeTable.draw()
                                    inactiveTable.draw()
                                    $.notify(
                                        {message: response.message},
                                        {type: 'success'}
                                    )
                                }else{
                                    response.message.forEach(el => {
                                        $.notify(
                                            {
                                                message: el
                                            },
                                            {
                                                type: 'danger'
                                            }
                                        )
                                    })
                                }
                            }
                        });
                        return
                    }
                })
            }
        }
    })
</script>