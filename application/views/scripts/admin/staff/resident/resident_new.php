<script>
    $('select').selectpicker({
        style: 'select-with-transition',
        width: 'fit'
    })
    $('input[name="birth_date"]').datetimepicker({
    format: 'MM/DD/YYYY',
    icons: {
        date: "fa fa-calendar",
        up: "fa fa-chevron-up",
        down: "fa fa-chevron-down",
        previous: 'fa fa-chevron-left',
        next: 'fa fa-chevron-right',
        today: 'fa fa-screenshot',
        clear: 'fa fa-trash',
        close: 'fa fa-remove'
    }
})
    //TODO - jquery validate

    $('#resident_new').validate({
        messages:{
            personal_contact_number:{
                minlength: 'Enter 10-digit number after +63',
                maxlength: 'Enter 10-digit number after +63'
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(e){
            $('button[type=submit').attr('disabled', true)
            $('input[disabled]').attr('disabled', false)
            $.ajax({
                type: "POST",
                url: proxy + "/api/resident/create",
                data: $('#resident_new').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        $('input[disabled=false]').attr('disabled', true)
                        $('button[type=submit]').attr('disabled', false)
                        $('#resident_new').get(0).reset()
                        window.location.href = "<?=base_url()?>staff/resident/" + response.data.id
                    }else{
                        response.message.forEach(el => (
                            $.notify(
                                {message: el},
                                {type: 'danger'}
                            )
                        ))
                    }
                }
            });
        }
    })
</script>