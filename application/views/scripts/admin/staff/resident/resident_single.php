<script>
    <?php if(isset($_GET['mode'])):?>
        setTimeout(() => {
            $('#edit').click()
        }, 1000);
    <?php endif;?>
    let resident, status
    function fetchResident(reset = false){
        $.ajax({
            type: "get",
            url: proxy + "/api/resident/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    $('[name=first_name]').val(data.first_name)
                    $('[name=middle_name]').val(data.middle_name)
                    $('[name=last_name]').val(data.last_name)
                    $('#age').val(data.age)
                    $('[name=birth_date]').val(data.birth_date)
                    $('#gender').val(data.gender)
                    $('[name=personal_contact_number]').val(data.personal_contact_number)
                    $('[name=current_house_number]').val(data.current_house_number)
                    $('[name=current_street]').val(data.current_street)
                    $('[name=current_barangay]').val(data.current_barangay)
                    $('[name=current_municipality]').val(data.current_municipality)
                    $('[name=current_province]').val(data.current_province)
                    $('[name=permanent_house_number]').val(data.permanent_house_number)
                    $('[name=permanent_street]').val(data.permanent_street)
                    $('[name=permanent_barangay]').val(data.permanent_barangay)
                    $('[name=permanent_municipality]').val(data.permanent_municipality)
                    $('[name=permanent_province]').val(data.permanent_province)
                    $('[name=occupation]').val(data.occupation)
                    $('[name=height]').val(data.height)
                    $('[name=weight]').val(data.weight)
                    $('#hair_color').text(data.hair_color)
                    $('#blood_type').text(data.blood_type)
                    $('[name=emergency_contact_person_1]').val(data.emergency_contact_person_1)
                    $('[name=emergency_contact_number_1]').val(data.emergency_contact_number_1)
                    $('[name=emergency_contact_person_2]').val(data.emergency_contact_person_2)
                    $('[name=emergency_contact_number_2]').val(data.emergency_contact_number_2)
                    $('[name=nationality]').val(data.nationality)
                    $('[name=citizenship]').val(data.citizenship)
                    
                    
                    $('[name=civil_status]').val(data.civil_status)
                    $('#status').val(data.status)

                    if(data.benefits.length){
                        $('#benefits').empty()
                        data.benefits.forEach(el => {
                            $(`input[value="${el}"]`).attr('checked', true)
                        })
                    }
                    resident = data.id
                    status = data.status

                    if(!reset){
                        $('select').selectpicker({
                            style: 'select-with-transition',
                            width: 'fit'
                        })
                    }
                    $('select').attr('disabled', true).selectpicker('refresh')
                    

                    $('#edit').attr('href', '<?=base_url()?>staff/resident/edit/'+data.id);

                    if(data.status == 'Active'){
                        $('#delete').text('DELETE')
                    }else{
                        $('#delete').text('RESTORE')
                    }
                }
            }
        });
    }
    fetchResident()

    $('#edit').click(()=>{
        $('textarea, [disabled]:not(#age)').attr('disabled', false)

        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted = false){
        if(!submitted){
            fetchResident(true)
        }
        $('input, textarea').attr('disabled', true)
        
        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')

        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        $(`[data-toggle=tab]>span`).remove()
    }

    $('#cancel').click(resetForm)

    let form = $('#resident_update').validate({
        ignore: [],
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
            let id = $(element).closest('.tab-pane').attr('id')
            if(!$(`[href="#${id}"]>span`).length){
                $(`[href="#${id}"]`).append(`
                    <span class="badge badge-pill badge-danger px-2">
                        <i class="material-icons m-0" style="font-size: .7rem">priority_high</i>
                    </span>
                `)
            }
        },
        unhighlight: function(element, errClass, sucClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            let id = $(element).closest('.tab-pane').attr('id')
            $(`[href="#${id}"]>span`).remove()
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            $(element).closest('.form-group').append(error)
        },
        submitHandler: function(){
            $.ajax({
                type: "put",
                url: proxy + "/api/resident/" + resident,
                data: $('#resident_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(null, true)
                        fetchResident()
                    }else{
                        response.message.forEach(el => (
                            $.notify(
                                {message: el},
                                {type: 'danger'}
                            )
                        ))
                    }
                },
                error: function(err){
                    console.log(err)
                }
            });
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(status == 'Active'){
            Swal.fire({
                title: 'Deactivate Resident',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/resident/" + resident,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchResident()
                                $.notify(
                                    { message: response.message },
                                    { type: 'success' }
                                )
                            }else{
                                $.notify(
                                    { message: response.message },
                                    { type: 'danger' }
                                )
                            }
                        }
                    });
                    return
                }
            })
        }else{
            Swal.fire({
                title: 'Restore Resident',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "put",
                        url: proxy + "/api/resident/restore/" + resident,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchResident()
                                $.notify(
                                    { message: response.message },
                                    { type: 'success' }
                                )
                            }else{
                                $.notify(
                                    { message: response.message },
                                    { type: 'danger' }
                                )
                            }
                        }
                    });
                    return
                }
            })
        }
    })
    
</script>