<script>
    let user, status, role

    <?php if(isset($_GET['mode'])):?>
        setTimeout(() => {
            $('#edit').click()
        }, 1000);
    <?php endif;?>
    function getRoles(){
        $.ajax({
            type: "get",
            url: proxy + "/api/roles/findAll",
            dataType: "json",
            success: function (response) {
                if(!response.error){
                    response.data.forEach(el => (
                        $('select[name=role_id]').append(`<option value="${el.id}">${el.name}</option>`)
                    ))
                }else{
                    $.notify(
                        {
                            message: 'Something went wrong.'
                        },
                        {
                            type: 'danger'
                        }
                    )
                }
            }
        });
    }

    getRoles()

    function fetchUser(submitted = false){
        $.ajax({
            type: "get",
            url: proxy + "/api/user/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    $('#resident').val(data.profile.full_name)
                    $('[name=email]').val(data.email)
                    $('#created').val(data.created_at)
                    $(`[name=role_id]`).val(data.role_id)
                    $(`[name=status]`).val(data.status)
                    if(!submitted){
                        $('select[name=role_id], select[name=status]').selectpicker({
                            style: 'select-with-transition',
                            width: 'fit'
                        })
                    }else{
                        $('select').selectpicker('refresh')
                    }
                    user = data.id
                    status = data.status
                    role = data.role_id

                    if(data.status == 'Active'){
                        $('#delete').text('DELETE')
                    }else{
                        $('#delete').text('RESTORE')
                    }
                }
            }
        });
    }
    fetchUser()

    $('#edit').click((e)=>{
        $('[disabled]:not(#resident, #created)').attr('disabled', false)
        $('[name=status], [name=role_id]').prop('disabled', false).selectpicker('refresh')

        $('input:submit, #cancel').removeClass('d-none')
        $('#edit, #delete').addClass('d-none')
    })

    function resetForm(e, submitted = false){
        $('input').attr('disabled', true)
        if(!submitted){
            fetchUser(true)
        }

        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')

        $('input:submit, #cancel').addClass('d-none')
        $('#edit, #delete').removeClass('d-none')
    }

    $('#cancel').click(resetForm)

    let form = $('#user_update').validate({
        rules:{
            email:{
                required: true,
                email: true
            },
            status: {
                required: true
            },
            role_id:{
                required: true
            }
        },
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $.ajax({
                type: "put",
                url: proxy + "/api/user/" + user,
                data: $('#user_update').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true
                },
                success: function (response) {
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        resetForm(true)
                        fetchUser()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                },
                error: function(err){
                    console.alert(err)
                }
            });
        }
    })

    $('#delete').click((e)=>{
        e.preventDefault()
        if(status == 'Active'){
            Swal.fire({
                title: 'Deactivate User',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                confirmButtonColor: '#f44336',
                preConfirm: ()=>{
                    $.ajax({
                        type: "delete",
                        url: proxy + "/api/user/" + user,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                fetchUser()
                                $.notify(
                                    {
                                        message: response.message
                                    }
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }else{
            Swal.fire({
                title: 'Restore User',
                text: 'Are you sure?',
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes',
                showLoaderOnConfirm: true,
                preConfirm: ()=>{
                    $.ajax({
                        type: "put",
                        url: proxy + "/api/user/restore/" + user,
                        data: "json",
                        xhrFields:{
                            withCredentials: true,
                        },
                        success: function (response) {
                            if(!response.error){
                                resetForm()
                                $.notify(
                                    {message: response.message},
                                    {type: 'success'}
                                )
                            }else{
                                response.message.forEach(el => {
                                    $.notify(
                                        {
                                            message: el
                                        },
                                        {
                                            type: 'danger'
                                        }
                                    )
                                })
                            }
                        }
                    });
                    return
                }
            })
        }
    })
    
</script>