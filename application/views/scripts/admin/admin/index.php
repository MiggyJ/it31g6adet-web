<script>

    let months = [
        'January',
        'February',
        'March',
        'April',
        'May',
        'June',
        'July',
        'August',
        'September',
        'October',
        'November',
        'December'
    ]

    function fillDashboard(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/dashboard/admin",
            data: "data",
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(!response.error){
                    let data = response.data
                    $('#users').text(data.users)
                    $('#residents').text(data.residents)
                    $('#staff').text(data.staff)
                    $('#admin').text(data.admin)
                    let register_chart = new Chart($('#register_chart'),{
                        type: 'line',
                        data: {
                            labels: data.chartLabel.map(el=>months[el]),
                            datasets:[
                                {
                                    label: 'Residents',
                                    data: data.chartData,
                                    borderColor: '#E91E63',
                                    backgroundColor: '#E91E63'
                                }
                            ]
                        },
                        options: {
                            responsive: true,
                            plugins: {
                                legend: {
                                    display: false,
                                },
                            },
                            scales : {
                                y : {
                                    min: 0,
                                    max: 50,
                                    precision: 0
                                }
                            }
                        },
                    })
                }
            }
        });
    }
    fillDashboard()

</script>