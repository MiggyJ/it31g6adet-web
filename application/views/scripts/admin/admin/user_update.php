<script>
let user, status
    function getRoles(role){
        $.ajax({
            type: "get",
            url: proxy + "/api/roles/findAll",
            dataType: "json",
            success: function (response) {
                if(!response.error){
                    response.data.forEach(el => (
                        $('select[name=role_id]').append(`<option ${role == el.id && 'selected'} value="${el.id}">${el.name}</option>`)
                    ))
                    $('select').selectpicker({
                        style: 'select-with-transition',
                    })

                }else{
                    $.notify(
                        {
                            message: 'Something went wrong.'
                        },
                        {
                            type: 'danger'
                        }
                    )
                }
            }
        });
    }

    function getUser(){
        $.ajax({
            type: "get",
            url: proxy + "/api/user/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    getRoles(data.role_id)
                    $('#name').text(data.profile.full_name)
                    $('input[name=email]').val(data.email)
                    $(`select[name=role]>option[value=${data.role.id}]`).attr('selected', true)
                    $('select[name=status]').val(data.status)
                    user = data.id

                    status = data.status
                }
            }
        });
    }
    getUser()

    $('form').submit((e)=>{
        e.preventDefault()
        $.ajax({
            type: "put",
            url: proxy + "/api/user/" + user,
            data: $('form').serialize(),
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(!response.error){
                    $.notify(
                        {
                            message: response.message
                        },
                        {
                            type: 'success'
                        }
                    )
                    getUser()
                }else{
                    $.notify(
                        {
                            message: response.message
                        },
                        {
                            type: 'danger'
                        }
                    )
                }
            },
            error: function(err){
                console.log(err)
            }
        });
    })
</script>