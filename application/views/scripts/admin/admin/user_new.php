<script>
    function getSelectOptions(){
        //* GET RESIDENTS
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=resident_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
                $('select[name=resident_id]').selectpicker({
                    style: 'select-with-transition',
                    liveSearch: true
                })
            }
        });
        //* GET ROLES
        $.ajax({
            type: "get",
            url: proxy + "/api/roles/findAll",
            dataType: "json",
            success: function (response) {
                if(!response.error){
                    response.data.forEach(el => (
                        $('select[name=role_id]').append(`<option value="${el.id}">${el.name}</option>`)
                    ))
                    $('select[name=role_id]').selectpicker({
                        style: 'select-with-transition',
                    })

                }else{
                    $.notify(
                        {
                            message: 'Something went wrong.'
                        },
                        {
                            type: 'danger'
                        }
                    )
                }
            }
        });
    }

    getSelectOptions()

    $("#user_new")
        .validate({
            rules:{
                resident_id: {
                    required: true,
                },
                role_id: {
                    required: true
                },
                email: {
                    required: true,
                    email: true,
                },
                password: {
                    required: true,
                    minlength: 8
                },
                confirm_password:{
                    required: true,
                    equalTo: 'input[name=password]'
                }
            },
            errorElement: 'span',
            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
            },
            success: function(element) {
                $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
            },
            errorPlacement : function(error, element) {
                element.closest('.form-group').append(error)
            },
            submitHandler: function() {
                $.ajax({
                    type: "POST",
                    url: proxy + "/api/user/create",
                    data: $("#user_new").serialize(),
                    dataType: "json",
                    xhrFields: {
                        withCredentials: true
                    },
                    success: function (response) {
                    if(response.error == false){
                        $.notify({
                            message: response.message
                        },{
                            type: 'success'
                        })

                        $('#user_new').get(0).reset()
                        setTimeout(() => {
                            window.location.href = '<?=base_url()?>/admin/user/'+response.user.id
                        }, 2000);
                    }else{
                        $.notify({
                            message: response.message
                        },{
                            type: 'danger'
                        })
                    }
                    }
                });
            }
        })
</script>