<script>
    function fillDashboard(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/dashboard/user",
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(!response.error){
                    let data = response.data
                    $('#certificates').text(data.certificates)
                    $('#blotters').text(data.blotters)
                    $('#complaints').text(data.complaints)
                    $('#summons').text(data.summons)
                    
                    if(data.upcoming_summons.length)
                        $('#empty_summon').hide()
                    data.upcoming_summons.forEach(el=>{
                        let builder = 
                        `
                            <tr>
                                <td align="center">${el.summon_date}</td>
                                <td align="center">${moment(el.summon_time, 'HH:mm A').format('HH:mm A')}</td>
                                <td align="center">${el.mediator.profile.full_name}</td>
                            </tr>
                        `
                        $('#upcoming_summon>tbody').append(builder)
                        return
                    })

                    if(data.certificate_list.length)
                        $('#empty_cert').hide()
                    data.certificate_list.forEach(el=>{
                        let builder =
                        `
                            <tr>
                                <td>${el.type}</td>
                                <td>
                                    <a href="http://localhost:3000/uploads/certificates/${el.type}/${el.file}">
                                        Download
                                    </a>
                                </td>
                            </tr>
                        `
                        $('#certificate_list>tbody').append(builder)
                        return
                    })
                }
            }
        });
    }

    fillDashboard()
</script>