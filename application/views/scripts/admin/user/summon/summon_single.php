<script>
    let summon
    function fetchSummon(){
        $.ajax({
            type: "get",
            url: proxy + "/api/summon/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    Swal.fire({
                        title: 'Unauthorized',
                        text: response.message,
                        type: 'error',
                        confirmButtonColor: '#f44336',
                        allowOutsideClick: false,
                        preConfirm: ()=>{
                            window.location.href = '/front/dashboard'
                            return
                        }
                    })
                }else{
                    let data = response.data
                    //*FILL UP FORM
                    $('#complaint>a').attr('href', `<?=base_url()?>user/complaint/${data.complaint_id}`)
                    $('#complaint>a').text(data.summon_complaint.complaint_number)
                    $('#date').val()
                    $('#summon_date').val(data.summon_date)
                    //*Format time to 12-hour
                    $('#summon_time').val(moment(data.summon_time, 'HH:mm').format('hh:mm A'))
                    $('#summon_complainant').val(data.summon_complaint.complaint_by.full_name)
                    $('#summon_defendant').val(data.summon_complaint.complaint_defendant.full_name)
                    $('#mediator').val(data.mediator.profile.full_name)
                    $('#duration').val(`${data.duration} hours`)
                    $('#details').val(data.details)
                    $('#status').val(data.status)
                    $('#details').append(data.details)

                    summon = data.id
                }
            }
        });
    }
    fetchSummon()

    let witnessTable
    function fetchWitnesses(){
        witnessTable = $('#witness_table').DataTable({
            serverSide: true,
            processing: true,
            language: {
                processing: 'Fetching Data...',
                zeroRecords: 'No witnesses found'
            },
            ajax: {
                url: proxy + '/api/witness/datatables?summon_id='+summon,
                xhrFields:{
                    withCredentials: true
                }
            },
            drawCallback: function(){
                $('[rel=tooltip]').tooltip()
            },
            columns: [
                {data: 'resident_profile.full_name', searchable: false, orderable: false},
                {data: 'resident_profile.personal_contact_number'},
                {data: 'resident_profile.current_address'},
            ]
        })
    }
    fetchWitnesses()
</script>