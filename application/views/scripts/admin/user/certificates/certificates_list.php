<script>
    let table = $('.table').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/certificate/datatablesUser',
            xhrFields:{
                withCredentials: true
            },
        },
        columns:[
            {data: 'certificate_number'},
            {data: 'type'},
            {data: 'created_at'},
            {data: 'status'},
            {data: 'valid_until', render: (data)=>(data === null ? 'N/A' : data)},
            {data: 'file', render: (data, type, row)=>{
                if(data == null){
                    return (
                        `<div class="text-center">
                            <button class="btn btn-black disabled" disabled="true">
                                Unavailable
                            </button>
                        </div>`
                    )
                }else{
                    return (
                        `<div class="text-center">
                            <a href="http://localhost:3000/uploads/certificates/${row.type.toLowerCase()}/${data}" target="_blank" class="btn btn-info">
                                <i class="material-icons">print</i>&nbsp;Print
                            </a>
                        </div>`
                    )
                }
            }}
        ]
    })

    // *Selectpicker
    $('[name=type]').selectpicker({
        style: 'select-with-transition'
    })

    $('[name=type]').on('changed.bs.select', (e)=>{
        if($(e.currentTarget).val() == 'Business'){
            $('[name=business_name]').closest('.row').removeClass('d-none')
            $('[name=business_address]').closest('.row').removeClass('d-none')
            $('[name=business_name], [name=business_address]').attr('disabled', false)
        }else{
            $('[name=business_name]').closest('.row').addClass('d-none')
            $('[name=business_address]').closest('.row').addClass('d-none')
            $('[name=business_name], [name=business_address]').attr('disabled', true)
        }
    })

    // *Filter buttons
    $('#type_filter').click((e)=>{
        if($(e.target).hasClass('nav-link')){
            if($(e.target).text() != 'All Types'){
                table.search($(e.target).text()).draw()
            } else {
                table.search('').draw()
            }

            $('#type_filter>li>.nav-link').removeClass('active')
            $(e.target).addClass('active')
        }
    })

    $('#status_filter').click((e)=>{
        if($(e.target).hasClass('nav-link')  && $(e.target).data('toggle') != 'modal'){
            if($(e.target).text() != 'All Status'){
                table.search($(e.target).text()).draw()
            } else {
                table.search('').draw()
            }

            $('#status_filter>li>.nav-link').removeClass('active')
            $(e.target).addClass('active')
        }
    })

    function resetForm(){
        form.resetForm()
        $('.form-group').removeClass('has-danger').removeClass('has-success')
        $('[name=type]').val(null).selectpicker('refresh')
        $('#certificate_new').get(0).reset()
        $('[name=business_name]').closest('.row').addClass('d-none')
        $('[name=business_address]').closest('.row').addClass('d-none')
        $('[name=business_name], [name=business_address]').attr('disabled', true)
    }

    // *Form Submit
    let form = $('#request_new').validate({
        errorElement: 'span',
        highlight: function(element) {
            $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
        },
        unhighlight: function(element, errorClass, successClass){
            $(element).closest('.form-group').removeClass('has-danger')
        },
        success: function(element) {
            $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
        },
        errorPlacement : function(error, element) {
            element.closest('.form-group').append(error)
        },
        submitHandler: function(){
            $.ajax({
                type: "POST",
                url: proxy + "/api/certificate/request",
                data: $('#request_new').serialize(),
                dataType: "json",
                xhrFields:{
                    withCredentials: true,
                },
                success: function(response){
                    if(!response.error){
                        $.notify(
                            {message: response.message},
                            {type: 'success'}
                        )
                        $('.modal').modal('hide')
                        table.draw()
                    }else{
                        $.notify(
                            {message: response.message},
                            {type: 'danger'}
                        )
                    }
                    resetForm()
                }
            });
        }
    })

    $('.modal').on('hidden.bs.modal', resetForm)
    
</script>