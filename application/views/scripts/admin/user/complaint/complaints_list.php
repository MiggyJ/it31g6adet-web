<script>
    let complainant = $('#complainant').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        dom: 'ftp',
        language: {
            processing: 'Fetching Data...',
            zeroRecords: 'No complaints found'
        },
        ajax: {
            url: proxy + '/api/complaint/datatablesComplainant',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns: [
            {data: 'complaint_number'},
            {data: 'complaint_defendant.first_name', visible: false, searchable: true},
            {data: 'complaint_defendant.last_name', visible: false, searchable: true},
            {data: 'complaint_defendant.full_name', orderable: false},
            {data: 'status'},
            {
                data: 'id', render: (data)=>(
                    `
                    <div class="text-center">
                        <a href="http://localhost/front/user/complaint/${data}" class="btn btn-link btn-info btn-just-icon like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                    </div>
                    `
                )
            },
        ]
    })

    let defendant = $('#defendant').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        dom: 'ftp',
        language: {
            processing: 'Fetching Data...',
            zeroRecords: 'No complaints found'
        },
        ajax: {
            url: proxy + '/api/complaint/datatablesDefendant',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns: [
            {data: 'complaint_number'},
            {data: 'complaint_by.first_name', visible: false, searchable: true},
            {data: 'complaint_by.last_name', visible: false, searchable: true},
            {data: 'complaint_by.full_name', orderable: false},
            {data: 'status'},
            {
                data: 'id', render: (data)=>(
                    `
                    <div class="text-center">
                        <a href="http://localhost/front/user/complaint/${data}" class="btn btn-link btn-info btn-just-icon like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                    </div>
                    `
                )
            },
        ]
    })
</script>