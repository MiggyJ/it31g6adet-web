<script>

    function getResidents(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=defendant_id], select[name=complainant_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
            }
        });
    }
    getResidents()

    function fetchComplaint(){
        $.ajax({
            type: "get",
            url: proxy + "/api/complaint/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    Swal.fire({
                        title: 'Unauthorized',
                        text: response.message,
                        type: 'error',
                        confirmButtonColor: '#f44336',
                        allowOutsideClick: false,
                        preConfirm: ()=>{
                            window.location.href = '/front/dashboard'
                            return
                        }
                    })
                }else{
                    let data = response.data
                    //fill up form
                    $('#complaint_number').val(data.complaint_number)
                    $('#complainant_id').val(data.complaint_by.id)
                    $('#defendant_id').val(data.complaint_defendant.id)
                    $('#complaint_date').val(data.complaint_date)
                    //*Dates for form and view
                    complaint_date = data.complaint_date
                    form_date = `${new Date(data.complaint_date).getMonth() + 1}/${new Date(data.complaint_date).getDate()}/${new Date(data.complaint_date).getFullYear()}`
                    $('#details').val(data.details)
                    $('#status').val(data.status)
                    $('select[name=defendant_id], select[name=complainant_id]').selectpicker({
                        style: 'select-with-transition',
                        liveSearch: true,
                        width: 'auto'
                    })
                    $('#details').append(data.details)

                    if(data.meetings.length){
                        $('#empty').hide()
                    }

                    data.meetings.forEach((el)=>{
                        let row = 
                        `
                            <tr>
                                <td align="center">${el.summon_date}</td>
                                <td align="center">${moment(el.summon_time, 'H:mm A').format('H:mm A')}</td>
                                <td>${el.mediator.profile.full_name}</td>
                                <td>${el.status}</td>
                                <td class="text-center">
                                <a href="http://localhost/front/user/summon/${el.id}" class="btn btn-info like" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i> View</a>
                                </td>
                            </tr>
                        `
                        $('tbody').append(row)
                        return
                    })
                }
            }
        });
    }
    fetchComplaint()

</script>