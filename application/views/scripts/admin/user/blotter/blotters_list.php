<script>
    let complainant = $('#complainant').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        dom: 'ftp',
        language: {
            processing: 'Fetching Data...',
            zeroRecords: 'No blotters found'
        },
        ajax: {
            url: proxy + '/api/blotter/datatablesComplainant',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns: [
            {data: 'blotter_number'},
            {data: 'blotter_defendant.first_name', visible: false, searchable: true},
            {data: 'blotter_defendant.last_name', visible: false, searchable: true},
            {data: 'blotter_defendant.full_name', orderable: false},
            {data: 'reason'},
            {data: 'id', render: (data)=>(`
                <div class="text-center">
                    <a href="http://localhost/front/user/blotter/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                </div>
            `)},
        ]
    })

    let defendant = $('#defendant').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        dom: 'ftp',
        language: {
            processing: 'Fetching Data...',
            zeroRecords: 'No blotters found'
        },
        ajax: {
            url: proxy + '/api/blotter/datatablesDefendant',
            xhrFields:{
                withCredentials: true
            },
        },
        drawCallback: function(){
            $('[data-toggle="tooltip"]').tooltip()
        },
        columns: [
            {data: 'blotter_number'},
            {data: 'blotter_complainant.first_name', visible: false, searchable: true},
            {data: 'blotter_complainant.last_name', visible: false, searchable: true},
            {data: 'blotter_complainant.full_name', orderable: false},
            {data: 'reason'},
            {data: 'id', render: (data)=>(`
                <div class="text-center">
                    <a href="http://localhost/front/user/blotter/${data}" class="btn btn-link btn-info btn-just-icon like" rel="tooltip" data-toggle="tooltip" data-placement="left" title="View"><i class="material-icons">visibility</i></a>
                </div>
            `)},
        ]
    })

</script>