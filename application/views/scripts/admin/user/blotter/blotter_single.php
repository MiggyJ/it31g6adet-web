<script>

    function getResidents(){
        $.ajax({
            type: "GET",
            url: proxy + "/api/resident/findAll",
            dataType: "json",
            xhrFields:{
                withCredentials: true,
            },
            success: function (response) {
                response.forEach(el =>{
                    $('select[name=defendant_id], select[name=complainant_id]').append(`<option value="${el.id}">${el.full_name} - ${el.age} y/o</option>`)
                })
            }
        });
    }
    getResidents()

    function fetchBlotter(){
        $.ajax({
            type: "get",
            url: proxy + "/api/blotter/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    Swal.fire({
                        title: 'Unauthorized',
                        text: response.message,
                        type: 'error',
                        confirmButtonColor: '#f44336',
                        allowOutsideClick: false,
                        preConfirm: ()=>{
                            window.location.href = '/front/dashboard'
                            return
                        }
                    })
                }else{
                    let data = response.data
                    $('#blotterNumber').val(data.blotter_number)
                    $('#complainant_id').val(data.blotter_complainant.id)
                    $('#reason').val(data.reason)
                    $('#incidence_date').val(data.incidence_date)
                    $('#defendant_id').val(data.blotter_defendant.id)
                    $('#incidence_place').val(data.incidence_place)
                    $('#details').append(data.details)

                    $('select').selectpicker({
                        style: 'select-with-transition',
                        liveSearch: true
                    })

                }
            }
        });
    }
    fetchBlotter()
    
</script>