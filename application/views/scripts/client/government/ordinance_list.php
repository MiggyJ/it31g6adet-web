<script>
    const proxy = 'http://localhost:3000'
    const limit = 2
    let page = 1


    function fillOrdinance() {
        $.ajax({
            type: "GET",
            url: `${proxy}/api/ordinance/paginate?_page=${page++}&_limit=${limit}`,
            dataType: "json",
            success: function (response) {
                if (!response.error){
                        $('.loader').hide()
                        if(response.data.length){
                            response.data.forEach(el=>{
                                let card = `
                                    <div class="card">
                                        <div class="card-body">
                                            <h3 class="card-title">${el.title}</h3>
                                            <h4 class="card-subtitle mb-2 text-muted">Ordinance Number: ${el.ordinance_number}, Authored by: ${el.ordinance_author.full_name}</h4>
                                            <div style="font-size:1.3rem;">${el.details}</div>
                                        </div>
                                        <div class="card-footer justify-content-end">
                                        <a href="http://localhost:3000/uploads/ordinances/${el.file}" class="btn btn-default">View Ordinance</a>
                                        </div>
                                    </div>
                                `
                                $('.container>.ordinances').append(card)
                                return
                            })
                        }else{
                            window.removeEventListener('scroll', loading)
                        }
                }else{
                    console.error(response.message)
                }
            }
        });
    }

    fillOrdinance()

    const loading = () => {
        const { scrollTop, scrollHeight, clientHeight } = document.documentElement;

        if (scrollTop + clientHeight >= scrollHeight) {
            $('.loader').show()
            setTimeout(() => {
                fillOrdinance()
            }, 1000);
        }
    }

    window.addEventListener('scroll', loading);
    
</script>