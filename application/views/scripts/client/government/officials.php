<script>
    const proxy = 'http://localhost:3000'
    $.ajax({
        type: "get",
        url: proxy + "/api/official/current",
        dataType: "json",
        success: function (response) {
            if(!response.error){
                let index = 0
                response.data.forEach(el => {
                    if(el.position == 'Barangay Chairman'){
                        $('#captain').text(el.profile.full_name)
                    }else if(el.position == 'Barangay Councilor'){
                        $('.council').eq(index++).text(el.profile.full_name)
                    }else{
                        $('.sk').text(el.profile.full_name)
                    }
                });
            }else{
                $.notify(
                    {message: response.message},
                    {type: 'danger'}
                )
            }
        },
        error: function(){
            $.notify(
            {message: 'Internal Server Error'},
            {type: 'danger'}
            )
        }
    });
</script>