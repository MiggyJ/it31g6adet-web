<script>
    let proxy = 'http://localhost:3000'
    let table = $('.table').DataTable({
        responsive: true,
        serverSide: true,
        processing: true,
        language: {
            processing: 'Fetching Data...'
        },
        ajax: {
            url: proxy + '/api/business/datatables',
        },
        columns:[
            {data: 'name'},
            {data: 'business_owner.first_name', searchable: true, visible: false},
            {data: 'business_owner.last_name', searchable: true, visible: false},
            {data: 'business_owner.full_name', searchable: false, orderable: false},
            {data: 'house_number', visible: false, searchable: true},
            {data: 'street', visible: false, searchable: true},
            {data: 'barangay', visible: false, searchable: true},
            {data: 'municipality', visible: false, searchable: true},
            {data: 'province', visible: false, searchable: true},
            {data: 'full_address', searchable: false, orderable: false},
            {data: 'contact_number', render: function(data){return data === null ? 'N/A' : data}},
            {data: 'email', render: function(data){return data === null ? 'N/A' : data}}
        ]
    })
</script>