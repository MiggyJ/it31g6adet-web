<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

  <!-- Custom Scripts -->
  <script>
    const proxy = 'http://localhost:3000'
    $.ajax({
      type: "get",
      url: proxy + "/api/announcement/latest",
      dataType: "json",
      success: function (response) {
        if (response.error) {
          response.message.forEach(el => {
            $.notify(
              {
                message: el
              },
              {
                type: 'warning'
              }
            )
          })
        } else {
          let cards = $('.card')
          response.data.forEach((el, index) => {
            $(cards[index]).find('.img.img-raised').attr('src', `http://localhost:3000/uploads/announcements/${el.file}`)
            $(cards[index]).find('.card-title').text(el.title)
            $(cards[index]).find('span').text(el.created_at)
            $(cards[index]).find('p').text(el.body)
            $(cards[index]).find('.card-body').append(`<a href="<?=base_url()?>announcement/${el.id}" class="btn btn-info">See More</a>`)
          })
        }
      },
      error: function(error){
        $.notify(
          {
            message: 'Internal Server Error'
          },
          {
            type: 'danger'
          }
        )
      }
    });
  </script>