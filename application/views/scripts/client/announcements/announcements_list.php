<script>
    const proxy = 'http://localhost:3000'
    const limit = 2
    let page = 1

    function fillAnnouncement(){
        $.ajax({
            type: "GET",
            url: `${proxy}/api/announcement/paginate?_limit=${limit}&_page=${page++}`,
            data: "json",
            success: function (response) {
                if (!response.error){
                    $('.loader').hide()
                    if(response.data.length){
                        response.data.forEach(el=>{
                            let card = `
                                <div class="card">
                                    <div class="row no-gutters">
                                        <div class="col-md-2 col-sm-3">
                                            <img class="card-img" src="http://localhost:3000/uploads/announcements/${el.file}"/>
                                        </div>
                                        <div class="col">
                                            <div class="card-body">
                                                <div class="card-title h2">${el.title}</div>
                                                <div>Published: ${el.created_at}</div>
                                                <p style="font-size:1.3rem; display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;overflow: hidden;">${el.body}</p>
                                                <a href="<?=base_url()?>announcement/${el.id}">See More</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            `
                            $('.container>.announcements').append(card)
                            return
                        })
                    }else{
                        window.removeEventListener('scroll', loading)
                    }
                }
            }
        });
    }

    fillAnnouncement()

    const loading = () => {
        const { scrollTop, scrollHeight, clientHeight } = document.documentElement;

        if (scrollTop + clientHeight >= scrollHeight) {
            $('.loader').show()
            setTimeout(() => {
                fillAnnouncement()
            }, 1000);
        }
    }

    window.addEventListener('scroll', loading);
</script>