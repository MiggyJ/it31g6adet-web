<script>
    <?php if(isset($_GET['mode'])):?>
        setTimeout(() => {
            $('#edit').click()
        }, 1000);
    <?php endif;?>
        const proxy = 'http://localhost:3000'
        function fetchAnnouncement(){
        $.ajax({
            type: "get",
            url: proxy + "/api/announcement/"+'<?=$id?>',
            dataType: "json",
            xhrFields:{
                withCredentials: true
            },
            success: function (response) {
                if(response.error){
                    $.notify(
                        {
                            message: response.message
                        },{
                            type: 'danger'
                        }
                    )
                }else{
                    let data = response.data
                    $('#title').text(data.title)
                    $('#view').attr('href', `http://localhost:3000/uploads/announcements/${data.file}`)
                    $('#image').attr('src', `http://localhost:3000/uploads/announcements/${data.file}`)
                    $('#body').html(data.body)
                    $('#created').text(data.created_at)
                }
            }
        });
    }
    fetchAnnouncement()
</script>
