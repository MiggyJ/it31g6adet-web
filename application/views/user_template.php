<!doctype html>
<html lang="en">

<head>
  <title><?=$title?></title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.14/dist/css/bootstrap-select.min.css">
  <!-- Material Kit CSS -->
  <link href="<?=base_url()?>assets/pro/css/material-dashboard.min.css" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
  <style>
    .sidebar::before{
      content:"";
      background: white !important;
    }
  </style>
</head>

<body>
  <div class="wrapper ">
    <div class="sidebar" data-color="purple" data-background-color="azure" data-image="">
      <div class="logo">
        <a href="<?=base_url()?>" class="simple-text logo-mini">
          
        </a>
        <a href="<?=base_url()?>" class="simple-text logo-normal">
          Barangay MS
        </a>
      </div>
      <div class="sidebar-wrapper">
      <?php if(get_cookie('role') == 'Admin'):?>
        <div class="user">
          <div class="photo">
            <i class="material-icons" style="font-size: 35px;">manage_accounts</i>
          </div>
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample2" class="username collapsed" aria-expanded="false">
              <span>
                Admin
                <b class="caret"></b>
              </span>
            </a>
            <div class="collapse" id="collapseExample2" style="">
              <ul class="nav">
                <li class="nav-item <?=$page == 'admin_dashboard' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>admin">
                    <span class="sidebar-mini">D</span>
                    <span class="sidebar-normal">
                      Dashboard
                    </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'users' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>admin/users">
                    <span class="sidebar-mini">UM</span>
                    <span class="sidebar-normal">
                      Users Management
                    </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      <?php endif;?>
      <?php if(get_cookie('role') == 'Staff'):?>
        <div class="user">
          <div class="photo">
            <i class="material-icons" style="font-size: 35px;">manage_accounts</i>
          </div>
          <div class="user-info">
            <a data-toggle="collapse" href="#collapseExample" class="username collapsed" aria-expanded="false">
              <span>
                Staff
                <b class="caret"></b>
              </span>
            </a>
            <div class="collapse" id="collapseExample" style="">
              <ul class="nav">
                <li class="nav-item <?=$page == 'staff_dashboard' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff">
                    <span class="sidebar-mini">D</span>
                    <span class="sidebar-normal">
                      Dashboard
                    </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_announcements' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/announcements">
                    <span class="sidebar-mini">A</span>
                    <span class="sidebar-normal">
                      Announcements
                    </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_blotters' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/blotters">
                    <span class="sidebar-mini">Bl</span>
                      <span class="sidebar-normal">
                        Blotters
                      </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_businesses' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/businesses">
                    <span class="sidebar-mini">Bu</span>
                      <span class="sidebar-normal">
                        Businesses
                      </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_certificates' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/certificates">
                    <span class="sidebar-mini">Ce</span>
                      <span class="sidebar-normal">
                        Certificates
                      </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_staff_complaints' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/complaints">
                    <span class="sidebar-mini">Co</span>
                      <span class="sidebar-normal">
                        Complaints
                      </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_officials' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/officials">
                    <span class="sidebar-mini">Of</span>
                    <span class="sidebar-normal">
                      Officials
                    </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_ordinances' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/ordinances">
                    <span class="sidebar-mini">Or</span>
                    <span class="sidebar-normal">
                      Ordinances
                    </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_residents' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/residents">
                    <span class="sidebar-mini">R</span>
                    <span class="sidebar-normal">
                      Residents
                    </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_relief' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/relief_operations">
                    <span class="sidebar-mini">RO</span>
                    <span class="sidebar-normal">
                      Relief Operations
                    </span>
                  </a>
                </li>
                <li class="nav-item <?=$page == 'staff_summons' ? 'active' : ''?>">
                  <a class="nav-link" href="<?=base_url()?>staff/summons">
                    <span class="sidebar-mini">S</span>
                      <span class="sidebar-normal">
                        Summons
                      </span>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      <?php endif;?>
        <ul class="nav">
          <li class="nav-item <?=$page == 'dashboard' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url()?>dashboard">
              <i class="material-icons">dashboard</i>
              <p>Dashboard</p>
            </a>
          </li>
          <!-- your sidebar here -->
          <li class="nav-item <?=$page == 'blotters' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url()?>user/blotters"">
              <i class="material-icons">local_police</i>
              <p>Blotter</p>
            </a>
          </li>
          <li class="nav-item <?=$page == 'businesses' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url()?>user/businesses">
              <i class="material-icons">store</i>
              <p>Businesses</p>
            </a>
          </li>
          <li class="nav-item <?=$page == 'certificates' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url()?>user/certificates">
              <i class="material-icons">verified</i>
              <p>Certificates</p>
            </a>
          </li>
          <li class="nav-item <?=$page == 'complaints' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url()?>user/complaints">
              <i class="material-icons">flag</i>
              <p>Complaints</p>
            </a>
          </li>
          <li class="nav-item <?=$page == 'summons' ? 'active' : ''?>">
            <a class="nav-link" href="<?=base_url()?>user/summons"">
              <i class="material-icons">gavel</i>
              <p>Summons</p>
            </a>
          </li>
        </ul>
      </div>
    </div>
    <div class="main-panel">
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute">
        <div class="container-fluid">
          <div class="navbar-wrapper">
            <div class="navbar-minimize">
                <button id="minimizeSidebar" class="btn btn-just-icon btn-white btn-fab btn-round">
                  <i class="material-icons text_align-center visible-on-sidebar-regular">more_vert</i>
                  <i class="material-icons design_bullet-list-67 visible-on-sidebar-mini">view_list</i>
              </button>
            </div>
            <div class="navbar-brand" href="#">
              <?php
                if(count(explode('_', $page)) < 2){
                  echo ucfirst($page);
                }else{
                  if($page == 'staff_dashboard')
                    echo 'Staff Dashboard';
                  else if ($page == 'admin_dashboard')
                    echo 'Admin Dashboard';
                  else
                    echo ucfirst(explode('_', $page)[1]);
                }
              ?>
            </div>
          </div>
          <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
            <span class="sr-only">Toggle navigation</span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
            <span class="navbar-toggler-icon icon-bar"></span>
          </button>
          <div class="collapse navbar-collapse justify-content-end">
          <form class="navbar-form navbar-right d-none" role="search"> <div class="form-group is-empty"> <input type="text" class="form-control" placeholder="Search"> <span class="material-input"></span> <span class="material-input"></span></div> <button type="submit" class="btn btn-white btn-round btn-just-icon"> <i class="material-icons">search</i> <div class="ripple-container"></div> </button> </form>
            <ul class="navbar-nav">
              <!-- your navbar here -->
              <li class="nav-item dropdown">
                <a class="nav-link" href="javascript:;" id="navbarDropdownProfile" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <i class="material-icons">person</i> Account
                </a>
                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownProfile">
                  <a class="dropdown-item" href="<?=base_url()?>user/profile">Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item logoutNav" href="#" data-role="logout">Log out</a>
                </div>
              </li>
              <hr class="d-none d-sm-block">
            </ul>
          </div>
        </div>
      </nav>
      <!-- End Navbar -->
      <div class="content">
        <div class="container-fluid">
          <!-- your content here -->
          <?php $this->load->view($content)?>
        </div>
      </div>
      <footer class="footer">
        <div class="container-fluid">
          <nav class="float-left">
            <ul>
              <li>
                <a href="https://www.creative-tim.com">
                  ADET 3-1 Group 6
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="https://www.creative-tim.com" target="_blank">Creative Tim</a> for a better web.
          </div>
          <!-- your footer here -->
        </div>
      </footer>
    </div>
  </div>
  
	<!--   Core JS Files   -->
  <script src="<?=base_url()?>assets/pro/js/core/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/pro/js/core/popper.min.js"></script>
  <script src="<?=base_url()?>assets/pro/js/core/bootstrap-material-design.min.js"></script>
  <script src="<?=base_url()?>assets/pro/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="<?=base_url()?>assets/pro/js/plugins/moment.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="<?=base_url()?>assets/pro/js/plugins/sweetalert2.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="<?=base_url()?>assets/pro/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the Wizard, full documentation here: https://github.com/VinceG/twitter-bootstrap-wizard -->
  <script src="<?=base_url()?>assets/pro/js/plugins/jquery.bootstrap-wizard.js"></script>
  <!--	Plugin for Select, full documentation here: http://silviomoreto.github.io/bootstrap-select -->
  <script src="<?=base_url()?>assets/pro/js/plugins/bootstrap-selectpicker.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
  <script src="<?=base_url()?>assets/pro/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!--  DataTables.net Plugin, full documentation here: https://datatables.net/  -->
  <script src="<?=base_url()?>assets/pro/js/plugins/jquery.dataTables.min.js"></script>
  <!--	Plugin for Tags, full documentation here: https://github.com/bootstrap-tagsinput/bootstrap-tagsinputs  -->
  <script src="<?=base_url()?>assets/pro/js/plugins/bootstrap-tagsinput.js"></script>
  <!-- Plugin for Fileupload, full documentation here: http://www.jasny.net/bootstrap/javascript/#fileinput -->
  <script src="<?=base_url()?>assets/pro/js/plugins/jasny-bootstrap.min.js"></script>
  <!--  Full Calendar Plugin, full documentation here: https://github.com/fullcalendar/fullcalendar    -->
  <script src="<?=base_url()?>assets/pro/js/plugins/fullcalendar.min.js"></script>
  <!-- Vector Map plugin, full documentation here: http://jvectormap.com/documentation/ -->
  <script src="<?=base_url()?>assets/pro/js/plugins/jquery-jvectormap.js"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?=base_url()?>assets/pro/js/plugins/nouislider.min.js"></script>
  <!-- Include a polyfill for ES6 Promises (optional) for IE11, UC Browser and Android browser support SweetAlert -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <!-- Library for adding dinamically elements -->
  <script src="<?=base_url()?>assets/pro/js/plugins/arrive.min.js"></script>
  <!--  Google Maps Plugin    -->
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB2Yno10-YTnLjjn_Vtk0V8cdcY5lC4plU"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Chart JS -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js@3.4.1/dist/chart.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?=base_url()?>assets/pro/js/plugins/bootstrap-notify.js"></script>
  <!-- CKEditor 4 -->
  <script src="https://cdn.ckeditor.com/4.16.1/standard/ckeditor.js"></script>
  <!-- Control Center for Material Dashboard: parallax effects, scripts for the example pages etc -->
  <script src="<?=base_url()?>assets/pro/js/material-dashboard.min.js" type="text/javascript"></script>
  <!-- Custom Scripts -->
  <script>
    const proxy = 'http://localhost:3000'
    setTimeout(() => {
      $('a[data-role=logout]').click((e)=>{
        e.preventDefault()
        console.log('nice')
        $.ajax({
          type: "post",
          url: proxy + "/api/auth/logout",
          dataType: "json",
          xhrFields:{
            withCredentials: true
          },
          success: function (response) {
            window.location.reload()
          }
        });
      })
    }, 2000);

    $(function() {
        $.ajaxSetup({
            error: function(jqXHR, exception) {
                if (jqXHR.status === 401) {
                  Swal.fire({
                      title: 'Unauthorized',
                      text: 'You are not allowed to do this action',
                      type: 'danger',
                      confirmButtonColor: '#f44336',
                      preConfirm: ()=>{
                          window.location.href = '/front/dashboard'
                          return
                      }
                  })
                } else {
                    alert('Uncaught Error.\n' + jqXHR.responseText);
                }
            }
        });
    });
  </script>
  <?php if($scripts != ''):?>
    <?php $this->load->view($scripts)?>
  <?php endif?>
  <script>
    $(".main-panel").perfectScrollbar('destroy')
  </script>
</body>

</html>