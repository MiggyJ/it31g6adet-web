<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/client/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?=base_url()?>assets/client/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    <?=$title?>
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?=base_url()?>assets/client/css/material-kit.css?v=2.0.7" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
  <style>
  @media (max-width: 991px){
    .dropdown .dropdown-menu{
      height: auto !important;
      overflow-y: auto !important;
    }
  }
  </style>
</head>

<body class="landing-page sidebar-collapse">
<!-- Navbar -->
  <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand" href="<?=base_url()?>">
          Hehi 2  
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item <?=$page == 'announcements' ? 'active' : ''?>">
            <a href="<?=base_url()?>announcements" class="nav-link">
              <i class="material-icons">article</i> Announcements
            </a>
          </li>
          <li class="dropdown nav-item <?=$page == 'government' ? 'active' : ''?>">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
              <i class="material-icons">groups</i> Government
            </a>
            <div class="dropdown-menu">
              <a href="<?=base_url()?>officials" class="dropdown-item">Officials</a>
              <a href="<?=base_url()?>ordinances" class="dropdown-item">Ordinances</a>
            </div>
          </li>
          <li class="nav-item <?=$page == 'businesses' ? 'active' : ''?>">
            <a href="<?=base_url()?>businesses" class="nav-link">
              <i class="material-icons">store</i> Businesses
            </a>
          </li>
          <li class="nav-item <?=$page == 'services' ? 'active' : ''?>">
            <a href="<?=base_url()?>services" class="nav-link">
              <i class="material-icons">miscellaneous_services</i> Online Services
            </a>
          </li>
          <?php if(empty(get_cookie('token'))):?>
          <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              <i class="material-icons">account_circle</i> Account
            </a>
            <div class="dropdown-menu dropdown-with-icons">
              <a href="<?=base_url()?>login" class="dropdown-item">
                <i class="material-icons">login</i> Login
              </a>
              <a href="<?=base_url()?>register" class="dropdown-item">
                <i class="material-icons">person_add</i> Register
              </a>
            </div>
          </li>
          <?php else:?>
            <li class="nav-item">
            <a href="<?=base_url()?>dashboard" class="nav-link">
              <i class="material-icons">account_circle</i> Account
            </a>
          </li>
          <?php endif?>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Header -->
  <div class="page-header header-filter <?=$page != ''? 'header-small' : ''?>" data-parallax="true" style="background-image: url('<?=base_url()?>assets/client/img/bg3.jpg')">
    <div class="container">
      <div class="row">
        <?php if($header == 'default'):?>
        <div class="col-md-6">
          <h1 class="title">Barangay Hehi 2</h1>
          <blockquote class="blockquote">
          A healthy social life is found only when, in the mirror of each soul, the whole community finds its reflection, and when, in the whole community, the virtue of each one is living.
          <footer class="blockquote-footer">
            Rudolf Steiner
          </footer>
          </blockquote>
        </div>
        <?php else:?>
        <div class="col-md-8 mr-auto ml-auto">
          <h1 class="text-center title">
            <?=$header?>
          </h1>
        </div>
        <?php endif?>
      </div>
    </div>
  </div>

  <!-- Main -->
  <div class="main main-raised">
    <?php $this->load->view($content)?>
  </div>

  <!-- Footer -->
  <footer class="footer footer-default">
    <div class="container">
      <nav class="float-left">
        <ul>
          <li>
            <a href="##">
              Privacy Policy
            </a>
          </li>
          <li>
            <a href="##">
              Contact Us
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-facebook"></i> Facebook
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-twitter"></i> Twitter
            </a>
          </li>
        </ul>
      </nav>
      <div class="copyright float-right">
        &copy;
        <script>
          document.write(new Date().getFullYear())
        </script>, made with <i class="material-icons">favorite</i> by
        <a href="#" target="_blank">BSIT 3-1 Group 6</a>.
      </div>
    </div>
  </footer>
  
  <!--   Core JS Files   -->
  <script src="<?=base_url()?>assets/client/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="<?=base_url()?>assets/client/js/core/popper.min.js" type="text/javascript"></script>
  <script src="<?=base_url()?>assets/client/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="<?=base_url()?>assets/client/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="<?=base_url()?>assets/client/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?=base_url()?>assets/client/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!-- Datatables -->
  <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?=base_url()?>assets/client/js/material-kit.js" type="text/javascript"></script>

  <!-- Custom Scripts -->
  <?php if($scripts != ''):?>
  <?php $this->load->view($scripts)?>
  <?php endif?>
</body>

</html>