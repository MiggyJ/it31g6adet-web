<style>
    td.dt-body-center{
        text-align: center;
    }
</style>
<div class="card">
    <div class="card-body">
        <ul id="filter" class="nav nav-pills nav-pills-rose justify-content-end">
            <li class="nav-item">
                <a class="nav-link bg-primary text-white" href="<?=base_url()?>staff/summon_new" >
                New Summon
                </a>
            </li>
            <li class="nav-item" style="cursor:pointer;">
                <div class="nav-link active" data-target="#">All</div>
            </li>
            <li class="nav-item" style="cursor:pointer;">
                <div class="nav-link" data-target="#">New</div>
            </li>
            <li class="nav-item" style="cursor:pointer;">
                <div class="nav-link" data-target="#">Adjourned</div>
            </li>
            <li class="nav-item" style="cursor:pointer;">
                <div class="nav-link" data-target="#">Concluded</div>
            </li>
        </ul>
        <hr>
        <table class="table table-striped table-bordered" width="100%">
            <thead>
                <tr>
                    <th>Complaint Number</th>
                    <th>Summon Date</th>
                    <th>Summon Time</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Summon Complainant</th>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Mediator</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
        </table>
    </div>
</div>