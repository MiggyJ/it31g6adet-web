<div class="container">
    <div class="row">
        <div class="col-lg-4 col-sm-6 mx-auto">
            <form id="summon_new">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title h3">New Summon</div>
                        <div class="container-fluid mt-4">
                            <div class="form-group">
                                <label class="label-control">Complaint Number</label>
                                <select name="complaint_id" class="ps-child" title="Select Complaint">
                                    <option selected disabled>Select Complaint</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="label-control">Summon Date</label>
                                <input type="text" class="form-control" name="summon_date">
                            </div>
                            <div class="form-group">
                                <label class="label-control">Summon Time</label>
                                <input type="text" class="form-control" name="summon_time">
                            </div>
                            <div class="form-group text-right">
                                <input type="submit" value="Submit" class="btn btn-primary">
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>