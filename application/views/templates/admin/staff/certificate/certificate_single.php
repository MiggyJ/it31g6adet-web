<div class="row">
    <div class="col-lg-8 col-md-10 mr-auto ml-auto">
        <div class="card">
            <div class="card-body">
                <div class="card-title h3 mb-2">Certificate Information</div>
                <div class="container">
                    <div class="row mb-4">
                        <div class="col">
                            <label class="mb-0">Certificate Number</label>
                            <div id="certificate_number"></div>
                        </div>
                        <div class="col">
                            <label class="mb-0">Certificate Type</label>
                            <div id="certificate_type"></div>
                        </div>
                        <div class="col">
                            <label class="mb-0">Status</label>
                            <div id="status"></div>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col-4">
                            <label class="mb-0">Resident</label>
                            <div>
                                <a href="" id="resident"></a>
                            </div>
                        </div>
                        <div class="col">
                            <label for="" class="mb-0">Date of Request</label>
                            <div id="request"></div>
                        </div>
                    </div>
                    <div class="row mb-4">
                        <div class="col">
                            <label class="mb-0">Approved By</label>
                            <div id="approved_by"></div>
                        </div>
                        <div class="col">
                            <label class="mb-0">Date Approved</label>
                            <div id="date_approved"></div>
                        </div>
                        <div class="col">
                            <label class="mb-0">Expires</label>
                            <div id="expiry"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer justify-content-end">
                <div id="approve" class="btn btn-success">Approve</div>
                <div id="delete" class="btn btn-danger">Reject</div>
                <a id="file" target="_blank" class="text-white btn btn-info d-none">View File</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
  <div class="modal-dialog modal-dialog-centered mt-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Processing Certificate</h5>
      </div>
      <div class="modal-body">
            <div class="progress" style="height: 15px">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
      </div>
    </div>
  </div>
</div>