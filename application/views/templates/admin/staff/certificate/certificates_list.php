<div class="row">
    <div class="col-3">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills nav-pills-icons nav-pills-rose flex-column" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link bg-primary text-white" href="<?=base_url()?>staff/certificate/new" >
                        New Certificate
                        </a>
                    </li>
                    <hr>
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#request" role="tablist">
                        Requests
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#approved" role="tablist">
                        Approved
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#trash" role="tablist">
                        Trash
                        </a>
                    </li>
                </ul>
                <hr>
                <ul class="nav nav-pills nav-pills-icons nav-pills-black flex-column" id="filter" style="cursor:pointer;">
                    <li class="nav-item ">
                        <div data-target="#" id="all" class="nav-link active">All</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" id="barangay" class="nav-link">Barangay</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" id="business" class="nav-link">Business</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" id="good_moral" class="nav-link">Good Moral</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" id="indigency" class="nav-link">Indigency</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-9">
        <div class="card">
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="request">
                        <table class="table table-striped table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th>Certificate Number</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Resident</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tab-pane" id="approved">
                        <table class="table table-striped table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th>Certificate Number</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Resident</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tab-pane" id="trash">
                        <table class="table table-striped table-bordered" width="100%">
                            <thead>
                                <tr>
                                    <th>Certificate Number</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Resident</th>
                                    <th>Type</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered mt-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Processing Certificate</h5>
      </div>
      <div class="modal-body">
            <div class="progress" style="height: 15px">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
      </div>
    </div>
  </div>
</div>