<div class="row">
    <div class="col-lg-4 col-md-5 mr-auto ml-auto">
        <form id="certificate_new">        
            <div class="card">
                <div class="card-body">
                    <div class="card-title h3">Certificate Request</div>
                    <div class="container">
                        <div class="row mb-2">
                            <div class="col">
                                <div class="form-group bmd-form-group">
                                    <label class="label-static">Resident</label>
                                    <select required name="owner_id" title="Select Resident"></select>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col">
                                <div class="form-group bmd-form-group">
                                    <label class="bmd-label-static">Certificate Type</label>
                                    <select required name="type" title="Select Type">
                                        <option value="Barangay">Barangay</option>
                                        <option value="Good Moral">Good Moral</option>
                                        <option value="Indigency">Indigency</option>
                                        <option value="Business">Business</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row d-none">
                            <div class="col">
                                <div class="form-group">
                                    <label class="label-control">Business Name</label>
                                    <input type="text" name="business_name" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row d-none">
                            <div class="col">
                                <div class="form-group">
                                    <label class="label-control">Business Address</label>
                                    <input type="text" name="business_address" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-2">
                            <div class="col">
                                <div class="form-check">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" name="approved" value="true">
                                        Approved
                                        <span class="form-check-sign">
                                            <span class="check"></span>
                                        </span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer justify-content-end">
                    <input type="submit" value="Submit" class="btn btn-primary">
                </div>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" tabindex="-1" data-backdrop="static" role="dialog">
  <div class="modal-dialog modal-dialog-centered mt-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Processing Certificate</h5>
      </div>
      <div class="modal-body">
            <div class="progress" style="height: 15px">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
      </div>
    </div>
  </div>
</div>