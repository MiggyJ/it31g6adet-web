<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-sm-12 mr-auto ml-auto">
            <form id="resident_update">
                <div class="card">
                    <div class="card-header card-header-tabs card-header-rose">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                            <span class="nav-tabs-title h4 my-0">Resident Info</span>
                            <ul class="nav nav-tabs justify-content-end" data-tabs="tabs">
                                <li class="nav-item">
                                <a class="nav-link active" href="#basic" data-toggle="tab">
                                    <i class="material-icons">fingerprint</i> Basic Info
                                    <div class="ripple-container"></div>
                                </a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link" href="#contact" data-toggle="tab">
                                    <i class="material-icons">contact_phone</i> Contact Info
                                    <div class="ripple-container"></div>
                                </a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link" href="#extra" data-toggle="tab">
                                    <i class="material-icons">description</i> Other Info
                                    <div class="ripple-container"></div>
                                </a>
                                </li>
                            </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="basic">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="" class="label-control">First Name</label>
                                                <input required type="text"  disabled name="first_name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="" class="label-control">Middle Name</label>
                                                <input type="text" disabled name="middle_name" class="form-control">
                                            </div>
                                        </div><div class="col">
                                            <div class="form-group">
                                                <label for="" class="label-control">Last Name</label>
                                                <input required type="text" disabled name="last_name" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group bmd-form-group">
                                                <select required id="status" name="status" disabled>
                                                    <option value="Active">Active</option>
                                                    <option value="Inactive">Inactive</option>
                                                </select>
                                                <label class="bmd-label-static">Status</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label class="label-control">Birth Date</label>
                                                <input required type="text" disabled name="birth_date" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group">
                                                <label class="label-control">Age</label>
                                                <input type="number" disabled id="age" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-static">Gender</label>
                                                <select required name="gender" disabled>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                    <option value="Non-binary">Non-binary</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-2">
                                            <div class="form-group bmd-form-group">
                                                <label class="bmd-label-static">Civil Status</label>
                                                <select requried name="civil_status" disabled>
                                                    <option value="Single">Single</option>
                                                    <option value="Married">Married</option>
                                                    <option value="Widowed">Widowed</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label class="label-control">Contact Number</label>
                                                <input required type="text" name="personal_contact_number" disabled class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Height(cm)</label>
                                                <input type="number" disabled name="height" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Weight(kg)</label>
                                                <input type="number" disabled name="weight" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label for="" class="label-control">Citizenship</label>
                                                <input type="text" name="citizenship" disabled class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label for="" class="label-control">Nationality</label>
                                                <input type="text" disabled name="nationality" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Occupation</label>
                                                <input type="text" disabled name="occupation" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">House Number</label>
                                                <input required type="text" disabled name="current_house_number" class="form-control text-right">
                                                <small class="bmd-helper">Current Address</small>
                                            </div>
                                        </div>
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label class="label-control">Street</label>
                                                <input required type="text" disabled name="current_street" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Barangay</label>
                                                <input required type="text" disabled name="current_barangay" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Municipality</label>
                                                <input required type="text" disabled name="current_municipality" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Province</label>
                                                <input required type="text" disabled name="current_province" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="contact">
                                <div class="container-fluid">
                                    <div class="row my-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Emergency Contact Person 1</label>
                                                <input type="text" disabled name="emergency_contact_person_1" class="form-control" id="emergency_person_1">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label class="label-control">Emergency Contact Number 1</label>
                                                <input type="text" disabled name="emergency_contact_number_1" class="form-control" id="emergency_contact_number_1">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row my-3">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Emergency Contact Person 2</label>
                                                <input type="text" disabled name="emergency_contact_person_2" class="form-control" id="emergency_person_2">
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="form-group">
                                                <label class="label-control">Emergency Contact Number 2</label>
                                                <input type="text" disabled name="emergency_contact_number_2" class="form-control" id="emergency_number_2">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row my-3">
                                        <div class="col-3">
                                            <div class="form-group">
                                                <label class="label-control">House Number</label>
                                                <input type="text" disabled id="permanent_house_number" name="permanent_house_number" class="form-control text-right">
                                                <small class="bmd-helper">Permanent Address</small>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Street</label>
                                                <input type="text" disabled name="permanent_street" id="permanent_street" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Barangay</label>
                                                <input type="text" disabled name="permanent_barangay" id="permanent_barangay" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Municipality</label>
                                                <input type="text" disabled name="permanent_municipality" id="permanent_municipality" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Province</label>
                                                <input type="text" disabled name="permanent_province" id="permanent_province" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="extra">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col-4">
                                            <h5>Benefits</h5>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="benefits" disabled value="4Ps">
                                                    4Ps
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="benefits" disabled value="Indigent">
                                                    Indigent
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                            <div class="form-check">
                                                <label class="form-check-label">
                                                    <input class="form-check-input" type="checkbox" name="benefits" disabled value="Senior Citizen">
                                                    Senior Citizen
                                                    <span class="form-check-sign">
                                                        <span class="check"></span>
                                                    </span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="form-group">
                                                <label for="" class="label-control">Remarks</label>
                                                <textarea disabled name="remarks" class="form-control" cols="30" rows="10"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer justify-content-end">
                        <div id="edit" class="btn btn-info">Edit</div>
                        <div id="delete" class="btn btn-danger"></div>
                        <div id="cancel" class="btn btn-dark d-none">Cancel</div>
                        <input type="submit" value="Update" class="btn btn-primary d-none">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>