<style>
    .table .form-check .form-check-sign{
        top: 0 !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-8 mr-auto ml-auto">
            <form id="resident_update">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title h3">Resident Info</div>
                        <table class="table table-striped table-bordered" width="100%">
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">First Name</label>
                                            <input type="text" class="form-control" name="first_name">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Middle Name</label>
                                            <input type="text" class="form-control" name="middle_name">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Last Name</label>
                                            <input type="text" class="form-control" name="last_name">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Birth Date</td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" class="form-control datepicker" required name="birth_date">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Contact Number</td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" class="form-control" name="personal_contact_number">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Gender</td>
                                    <td>
                                        <select name="gender">
                                            <option value="Male">Male</option>
                                            <option value="Female">Female</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Civil Status</td>
                                    <td>
                                        <select name="civil_status">
                                            <option value="Single">Single</option>
                                            <option value="Married">Married</option>
                                            <option value="Widowed">Widowed</option>
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Current Address</td>
                                    <td>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">House Number</label>
                                            <input type="text" class="form-control" name="current_house_number">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Street</label>
                                            <input type="text" class="form-control" name="current_street">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Barangay</label>
                                            <input type="text" disabled value="Hehi 2" class="form-control" name="current_barangay">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Municipality</label>
                                            <input type="text" disabled value="Ila" class="form-control" name="current_municipality">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Region</label>
                                            <input type="text" disabled value="Region 18" class="form-control" name="current_province">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Benefits</td>
                                    <td>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="benefits" value="4Ps">
                                                4Ps
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="benefits" value="Indigent">
                                                Indigent
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                        <div class="form-check">
                                            <label class="form-check-label">
                                                <input class="form-check-input" type="checkbox" name="benefits" value="Senior Citizen">
                                                Senior Citizen
                                                <span class="form-check-sign">
                                                    <span class="check"></span>
                                                </span>
                                            </label>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer justify-content-end">
                        <input type="submit" value="Submit" class="btn btn-primary"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>