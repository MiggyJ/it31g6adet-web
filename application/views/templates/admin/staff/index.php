<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">verified</i>
                </div>
                <p class="card-category">Certificates Approved</p>
                <h2 class="card-title" id="certificates">0</h2>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">visibility</i>
                    <a href="<?=base_url()?>staff/certificates">View All Certificates</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">local_police</i>
                </div>
                <p class="card-category">Blotters Recorded</p>
                <h2 class="card-title" id="blotters">0</h2>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">visibility</i>
                    <a href="<?=base_url()?>staff/blotters">View All Blotters</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">flag</i>
                </div>
                <p class="card-category">Complaints Filed</p>
                <h2 class="card-title" id="complaints">0</h2>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">visibility</i>
                    <a href="<?=base_url()?>staff/complaints">View All Complaints</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">gavel</i>
                </div>
                <p class="card-category">Summons Called</p>
                <h2 class="card-title" id="summons">0</h2>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">visibility</i>
                    <a href="<?=base_url()?>staff/summons">View All Summons</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">group</i>
                </div>
                <p class="card-category">Residents</p>
                <h2 class="card-title" id="residents">0</h2>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">visibility</i>
                    <a href="<?=base_url()?>/staff/residents">View All Residents</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">accessibility</i>
                </div>
                <p class="card-category">Relief Efforts</p>
                <h2 class="card-title" id="relief_operations">0</h2>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">visibility</i>
                    <a href="<?=base_url()?>/staff/relief_operations">View All Relief Operations</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">store</i>
                </div>
                <p class="card-category">Businesses</p>
                <h2 class="card-title" id="businesses">0</h2>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">visibility</i>
                    <a href="<?=base_url()?>/staff/businesses">View All Businesses</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">article</i>
                </div>
                <p class="card-category">Ordinances</p>
                <h2 class="card-title" id="ordinances">0</h2>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">visibility</i>
                    <a href="<?=base_url()?>/staff/ordinances">View All Ordinances</a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-5">
        <div class="card">
            <div class="card-body">
                <div class="card-title h3">Complaints</div>
                <canvas id="complaints_chart" height="350"></canvas>
            </div>
        </div>
    </div>
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="card-title h3">Population</div>
                <div class="container-fluid mt-5 mb-2">
                    <div class="row">
                        <div class="col-6">
                            <div class="card card-stats">
                                <div class="card-header card-header-icon card-header-rose">
                                    <div class="card-icon">
                                        <i class="material-icons">male</i>
                                    </div>
                                    <p class="card-category">Male</p>
                                    <h2 class="card-title" id="male">0</h2>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card card-stats">
                                <div class="card-header card-header-icon card-header-rose">
                                    <div class="card-icon">
                                        <i class="material-icons">female</i>
                                    </div>
                                    <p class="card-category">Female</p>
                                    <h2 class="card-title" id="female">0</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <div class="col-6 mx-auto">
                            <div class="card card-stats">
                                <div class="card-header card-header-icon card-header-rose">
                                    <div class="card-icon">
                                        <i class="material-icons">transgender</i>
                                    </div>
                                    <p class="card-category">Non-binary</p>
                                    <h2 class="card-title" id="nonbinary">0</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>