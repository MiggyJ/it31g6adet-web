<style>
    td.dt-body-center{
        text-align: center;
    }
</style>
<div class="card">
    <div class="card-body">
        <ul class="nav nav-pills nav-pills-rose justify-content-end" role="tablist">
            <li class="nav-item">
                <a class="nav-link bg-primary text-white" href="<?=base_url()?>staff/ordinance_new" >
                New Ordinance
                </a>
            </li>
        </ul>
        <hr>
        <div class="tab-content">
            <div class="tab-pane active" id="active">
                <table class="table table-striped table-bordered display" width=100%>
                    <thead>
                        <tr>
                            <th>ORD #</th>
                            <th>Title</th>
                            <th>Author</th>
                            <th>Date Effective</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>