<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-md-12 mr-auto ml-auto">
            <form id="ordinance_update">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title h3">Ordinance Info</div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="label-control">Ordinance Number</label>
                                        <input type="text" required disabled name="ordinance_number" class="form-control">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-static">Author</label>
                                        <select name="author" required disabled></select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="label-control">Date Effective</label>
                                        <input type="text" required disabled name="date_effective" class="form-control">
                                    </div>
                                </div>
                                <div class="col">
                                    <a href="" id="fileview" class="btn btn-primary" target="_blank">View File</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <textarea rows="2" required disabled name="title" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Details</label><br>
                                        <textarea disabled name="details" id="details" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2 d-none" id="change_file">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-outline-secondary btn-file">
                                        <span class="fileinput-new">New file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="file" accept="application/pdf">
                                    </span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer justify-content-end">
                        <div id="edit" class="btn btn-info">Edit</div>
                        <div id="cancel" class="btn btn-dark d-none">Cancel</div>
                        <input type="submit" value="Update" class="btn btn-primary d-none">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>