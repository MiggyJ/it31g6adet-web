<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-md-12 mr-auto ml-auto">
            <form id="ordinance_new" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title h3">Ordinance Info</div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="label-control">Ordinance Number</label>
                                        <input type="text" required name="ordinance_number" class="form-control">
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-static">Author</label>
                                        <select name="author" required title="Select Author"></select>
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="label-control">Date Effective</label>
                                        <input type="text" required  name="date_effective" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label>Title</label>
                                        <textarea rows="2" required  name="title" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Details</label><br>
                                        <textarea  name="details" id="details" cols="30" rows="10" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2" id="change_file">
                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                    <span class="btn btn-outline-secondary btn-file">
                                        <span class="fileinput-new">Ordinance file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" name="file" accept="application/pdf">
                                    </span>
                                    <span class="fileinput-filename"></span>
                                    <a href="#" class="close fileinput-exists" data-dismiss="fileinput" style="float: none">&times;</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer justify-content-end">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>