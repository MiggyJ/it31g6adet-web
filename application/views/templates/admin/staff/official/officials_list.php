<style>
    td.dt-body-center{
        text-align: center;
    }
</style>
<div class="card">
    <div class="card-body">
        <ul class="nav nav-pills nav-pills-rose justify-content-end" role="tablist">
            <li class="nav-item">
                <a class="nav-link bg-primary text-white" href="<?=base_url()?>staff/official_new" >
                New Official
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#all" role="tablist">
                All
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#current" role="tablist">
                Current
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#trash" role="tablist">
                Trash
                </a>
            </li>
        </ul>
        <hr>
        <div class="tab-content">
            <div class="tab-pane active" id="all">
                <table class="table table-striped table-bordered display" id="allTable" width=100%>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Term Start</th>
                            <th>Term End</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane" id="current">
                <table class="table table-striped table-bordered display" id="currentTable" width=100%>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Term Start</th>
                            <th>Term End</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane" id="trash">
                <table class="table table-striped table-bordered display" id="inactiveTable" width=100%>
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Name</th>
                            <th>Position</th>
                            <th>Term Start</th>
                            <th>Term End</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>