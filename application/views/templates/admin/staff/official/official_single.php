<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-sm-6 mr-auto ml-auto">
            <div class="card">
                <form id="official_update">
                    <div class="card-body">
                        <div class="card-title h3">Barangay Official Info</div>
                        <div class="container-fluid">
                            <div class="row px-2">
                                <a href="" id="resident_profile">View Resident Profile</a>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-static">Resident Name</label>
                                        <select name="resident_id" disabled required></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-static">Position</label>
                                        <select name="position" disabled required>
                                            <option value="Barangay Chairman">Barangay Chairman</option>
                                            <option value="Barangay Councilor">Barangay Councilor</option>
                                            <option value="SK Chairman">SK Chairman</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Start of Term</label>
                                        <input type="text" class="form-control" disabled required name="term_start">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">End of Term</label>
                                        <input type="text" class="form-control" disabled required name="term_end">
                                    </div>
                                </div>
                            </div>
                            <!--footer-->
                        </div>
                    </div>
                    <div class="card-footer justify-content-end">
                        <div id="edit" class="btn btn-info">Edit</div>
                        <div id="delete" class="btn btn-danger"></div>
                        <div id="cancel" class="btn btn-dark d-none">Cancel</div>
                        <input type="submit" value="Update" class="btn btn-primary d-none">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>