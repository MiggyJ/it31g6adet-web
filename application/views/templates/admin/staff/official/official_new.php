<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-4 col-sm-6 mr-auto ml-auto">
            <div class="card">
                <form id="official_new">
                    <div class="card-body">
                        <div class="card-title h3">Barangay Official Info</div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-static">Resident Name</label>
                                        <select name="resident_id" title="Select Resident" required></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-static">Position</label>
                                        <select name="position" title="Select Position" required>
                                            <option value="Barangay Chairman">Barangay Chairman</option>
                                            <option value="Barangay Councilor">Barangay Councilor</option>
                                            <option value="SK Chairman">SK Chairman</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Start of Term</label>
                                        <input type="text" class="form-control" required name="term_start">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">End of Term</label>
                                        <input type="text" class="form-control" required name="term_end">
                                    </div>
                                </div>
                            </div>
                            <!--footer-->
                        </div>
                    </div>
                    <div class="card-footer justify-content-end">
                        <input type="submit" value="Submit" class="btn btn-primary">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>