<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-sm-12 mr-auto ml-auto">
            <form id="announcement_update">
            <div class="card">
            <div class="card-header card-header-tabs card-header-rose">
                    <div class="card-title h4">Announcement Info</div>
                    </div><br>
                    <div class="container-fluid">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="label-control">Announcement Title</label>
                                            <input type="text" disabled id="title" name="title" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="label-control">Announcement Date</label>
                                            <input type="text" disabled id="created_at" name="created_at" class="form-control">
                                        </div>
                                    </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Body</label>
                                                <textarea class="form-control" disabled name="body" id="body" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div>
                                                <!-- <label for="img">Upload Image</label> -->
                                                <input type="file" id="img" name="img" accept="image/*">                
                                            </div>
                                        </div>
                                    </div>
                               
                            <!--footer-->
                            <div class="card-footer justify-content-end">
                            </div>
                        </div>
                        <!--footer-->
                        <div class="card-footer justify-content-end">
                            <div id="edit" class="btn btn-info">Edit</div>
                            <div id="delete" class="btn btn-danger"></div>
                            <div id="cancel" class="btn btn-dark d-none">Cancel</div>
                            <input type="submit" value="Update" class="btn btn-primary d-none">
                        </div>
                </div>
            </form>
        </div>
    </div>
</div>