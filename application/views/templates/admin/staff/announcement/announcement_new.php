<style>
    .table .form-check .form-check-sign{
        top: 0 !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-sm-12 mr-auto ml-auto">
            <form id="announcement_new" enctype="multipart/form-data">
                <div class="card">
                    <div class="card-header card-header-tabs card-header-rose">
                        <div class="nav-tabs-navigation">
                            <div class="nav-tabs-wrapper">
                            <span class="nav-tabs-title h4 my-0">Announcement Info</span>
                            <ul class="nav nav-tabs justify-content-end" data-tabs="tabs">
                                <li class="nav-item">
                                <a class="nav-link active" href="#basic" data-toggle="tab">
                                    <i class="material-icons">fingerprint</i> Basic Info
                                    <div class="ripple-container"></div>
                                </a>
                                </li>
                                <li class="nav-item">
                                <a class="nav-link" href="#image" data-toggle="tab">
                                    <i class="material-icons">image</i> Image
                                    <div class="ripple-container"></div>
                                </a>
                                </li>
                            </ul>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="basic">
                                <div class="container-fluid">
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Title</label>
                                                <input type="text" id="title" name="title" class="form-control">
                                            </div>
                                        </div>                      
                                    </div>
                                    <div class="row">
                                        <div class="col">
                                            <div class="form-group">
                                                <label class="label-control">Body</label>
                                                <textarea class="form-control" name="body" id="body" rows="5"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                            </div>
                            <div class="tab-pane" id="image">
                                <center>
                                    <div class="fileinput fileinput-new" data-provides="fileinput">
                                        <div class="fileinput-new img-thumbnail" style="width:500px; height: 320px;">
                                            <img src="https://i.stack.imgur.com/y9DpT.jpg"  alt="..." style=" max-width: 480px; max-height: 300px;">
                                        </div>
                                        <div class="fileinput-preview fileinput-exists img-thumbnail" style="max-width: 500px; max-height: 320px;"></div>
                                        <div>
                                            <span class="btn btn-outline-secondary btn-file"><span class="fileinput-new">Select image</span><span class="fileinput-exists">Change</span><input type="file" accept="image/*" name="file"></span>
                                            <a href="#" class="btn btn-outline-secondary fileinput-exists" data-dismiss="fileinput">Remove</a>
                                        </div>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer justify-content-end">
                        <button type="submit" id="" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>