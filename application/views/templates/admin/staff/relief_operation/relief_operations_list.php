<style>
    td.dt-body-center{
        text-align: center;
    }
</style>
<div class="row">
    <div class="col-3">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills nav-pills-icons nav-pills-rose flex-column" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link bg-primary text-white" href="<?=base_url()?>staff/relief_operation_new" >
                        New Relief Operation
                        </a>
                    </li>
                    <hr>
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#active" role="tablist">
                        Active
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#trash" role="tablist">
                        Trash
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-9">
        <div class="card">
            <div class="card-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="active">
                        <table class="table table-striped table-bordered" id="activeTable" width="100%">
                            <thead>
                                <tr>
                                    <th>Sponsor</th>
                                    <th>Operation Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <div class="tab-pane" id="trash">
                    <table class="table table-striped table-bordered" id="inactiveTable" width="100%">
                            <thead>
                                <tr>
                                    <th>Sponsor</th>
                                    <th>Operation Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
