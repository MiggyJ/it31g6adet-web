<style>
  .select-with-transition {
    margin-top: .15rem
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-lg-10 col-sm-12 mr-auto ml-auto">
      <form id="relief_operation_update">
        <div class="card">
          <div class="card-header card-header-tabs card-header-rose">
            <div class="nav-tabs-navigation">
              <div class="nav-tabs-wrapper">
                <span class="nav-tabs-title h4 my-0">Relief Operation Info</span>
                <ul class="nav nav-tabs justify-content-end" data-tabs="tabs">
                  <li class="nav-item">
                    <a class="nav-link active" href="#basic" data-toggle="tab">
                      <i class="material-icons">fingerprint</i> Basic Info <div class="ripple-container"></div>
                    </a>
                  </li>
                  <li class="nav-item">
                    <a class="nav-link" href="#relief_recipient" data-toggle="tab">
                      <i class="material-icons">badge</i> Relief Recipients <div class="ripple-container"></div>
                    </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="card-body">
            <div class="tab-content">
              <div class="tab-pane active" id="basic">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label class="label-control">Sponsor</label>
                        <input type="text" disabled id="sponsor" name="sponsor" class="form-control">
                      </div>
                    </div>
                    <div class="col">
                      <div class="form-group">
                        <label class="label-control">Operation Date</label>
                        <input type="text" disabled id="operation_date" name="operation_date" class="form-control">
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col">
                      <div class="form-group">
                        <label class="label-control">Details</label>
                        <br>
                        <textarea class="form-control" disabled name="details" id="details" rows="5"></textarea>
                      </div>
                    </div>
                  </div>
                </div>
               </div>
                <div class="tab-pane" id="relief_recipient">
                  <div class="container-fluid">
                    <table class="table table-striped table-bordered" width="100%" id="recipient_table">
                      <thead>
                        <tr>
                          <th>First Name</th>
                          <th>Last Name</th>
                          <th>Name</th>
                          <th>Contact Number</th>
                          <th>Items Received</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="card-footer justify-content-end" id="form_footer">
              <div id="edit" class="btn btn-info">Edit</div>
              <div id="delete" class="btn btn-danger"></div>
              <div id="cancel" class="btn btn-dark d-none">Cancel</div>
              <input type="submit" value="Update" class="btn btn-primary d-none">
            </div>
            <div class="card-footer justify-content-end d-none" id="recipient_footer">
              <div data-target="#recipientModal" data-toggle="modal" class="btn btn-primary">Add Relief Recipient</div>
            </div>
          </div>
      </form>
    </div>
  </div>
</div>

<div class="modal fade" id="recipientModal" tabindex="-1" role="dialog" aria-labelledby="recipientModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="recipientModalLabel">New Relief Recipient</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form id="recipient_new">
        <div class="modal-body">
          <div class="form-group">
            <label class="label-control">Resident</label>
            <select name="recipient_id" title="Select Resident"></select>     
          </div>
          <div> <label class="label-control">Items Received</label>
            <textarea class="form-control" id="item_received" rows="5"></textarea>
         </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary">Confirm</button>
        </div>
      </form>
    </div>
  </div>
</div>