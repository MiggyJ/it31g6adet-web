<style>
  .select-with-transition {
    margin-top: .15rem
  }
</style>
<div class="container">
  <div class="row">
    <div class="col-10 col-sm-12 mr-auto ml-auto">
      <div class="card">
        <form id="relief_operation_new">
          <div class="card-body">
            <div class="card-title h3">New Relief Operation</div>
            <div class="container-fluid">
              <div class="row">
                <div class="col">
                  <div class="form-group">
                    <label class="label-control">Sponsor</label>
                    <input type="text" id="sponsor" name="sponsor" class="form-control">
                  </div>
                </div>
                <div class="col">
                  <div class="form-group">
                    <label class="label-control">Operation Date</label>
                    <input type="text" id="operation_date" name="operation_date" class="form-control">
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col">
                  <div class="form-group">
                    <label class="label-control">Details</label>
                    <br>
                    <textarea class="form-control" name="details" id="details" rows="5"></textarea>
                  </div>
                </div>
              </div>
              <div class="form-group text-right">
                <input type="submit" value="Submit" class="btn btn-primary">
              </div>
            </div>
        </form>
      </div>
    </div>
  </div>
</div>