<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
    .table .form-check .form-check-sign{
        top: 0 !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-10 col-sm-12 mr-auto ml-auto">
            <div class="card">
                <form id="complaint_new">
                    <div class="card-body">
                        <div class="card-title h3">
                            New Complaint
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="label-control">Complainant</label>
                                        <select id="complainant_id" name="complainant_id" title="Select Resident">
                                            <option selected disabled>Select Resident</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-3 offset-1">
                                    <div class="form-group">
                                        <label class="label-control">Defendant</label>
                                        <select id="defendant_id" name="defendant_id" title="Select Resident">
                                            <option selected disabled>Select Resident</option>
                                        </select>
                                    </div>
                                </div>                                
                                <div class="col-3 offset-1">
                                    <div class="form-group">
                                        <label class="label-control">Complaint Date</label>
                                        <input type="text" name="complaint_date" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Details</label><br>
                                        <textarea class="form-control" placeholder="Complaint Details..." rows="5" name="details" id="details"></textarea>
                                    </div>
                                </div>
                            </div>                            
                            <div class="card-footer justify-content-end">
                                <button type="submit" id="" class="btn btn-primary">Submit</button>
                            </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>