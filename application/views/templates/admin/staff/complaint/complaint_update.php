<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
    .table .form-check .form-check-sign{
        top: 0 !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-8 mr-auto ml-auto">
            <div class="card">
                <form>
                    <div class="card-body">
                        <div class="card-title h3">Edit Complaint</div>
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <select name="role_id">
                                                <option selected disabled>Select Role</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <select name="resident_id">
                                                <option selected disabled>Select Resident</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group col-6">
                                            <label>Complaint Date</label><br>
                                            <input type="date" name="complaint_date" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label>Details</label><br>
                                            <textarea class="form-control" placeholder="Complaint Details..." rows="5" name="details" id="details"></textarea>
                                        </div>
                                    </div>
                                </div>
                            
                                <div class="card-footer justify-content-end">
                                    <button type="submit" id="" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>