<style>
    /* Chrome, Safari, Edge, Opera */
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    /* Firefox */
    input[type=number] {
    -moz-appearance: textfield;
    }
    .table .form-check .form-check-sign{
        top: 0 !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-10 col-sm-12 mr-auto ml-auto">
            <div class="card">
                <form id="blotter_new">
                    <div class="card-body">
                        <div class="card-title h3">
                            Blotter Info
                        </div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <select id="complainant_id" name="complainant_id" title="Select Resident">
                                        </select>
                                        <label class="bmd-helper">Complainant</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Incidence Place</label>
                                        <input type="text" id="incidence_place" name="incidence_place" class="form-control">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="label-control">Incidence Date</label>
                                        <input type="text" id="incidence_date" name="incidence_date" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <select id="defendant_id" name="defendant_id" title="Select Resident">
                                        </select>
                                        <label class="bmd-helper">Defendant</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Reason</label>
                                        <input type="text" id="reason" name="reason" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Details</label>
                                        <textarea class="form-control" name="details" id="details" placeholder="Complaint Details..." rows="5"></textarea>
                                    </div>
                                </div>
                            </div> 
                            <!--footer-->
                        <div class="card-footer justify-content-end">
                            <button type="submit" id="" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>