<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-sm-12 mr-auto ml-auto">
            <form id="blotter_update">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title h3">Blotter Info</div>
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="label-control">Blotter Number</label>
                                        <input type="text" disabled id="blotterNumber" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <select id="complainant_id" disabled name="complainant_id">
                                        </select>
                                        <label class="bmd-helper">Complainant</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Incidence Place</label>
                                        <input type="text" disabled id="incidence_place" name="incidence_place" class="form-control">
                                    </div>
                                </div>
                                <div class="col-3">
                                    <div class="form-group">
                                        <label class="label-control">Incidence Date</label>
                                        <input type="text" disabled id="incidence_date" name="incidence_date" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-4">
                                    <div class="form-group">
                                        <select id="defendant_id" disabled name="defendant_id">
                                        </select>
                                        <label class="bmd-helper">Defendant</label>
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Reason</label>
                                        <input type="text" disabled id="reason" name="reason" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Details</label>
                                        <textarea class="form-control" disabled name="details" id="details" placeholder="Complaint Details..." rows="5"></textarea>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <!--footer-->
                        <div class="card-footer justify-content-end">
                            <div id="edit" class="btn btn-info">Edit</div>
                            <div id="delete" class="btn btn-danger"></div>
                            <div id="cancel" class="btn btn-dark d-none">Cancel</div>
                            <input type="submit" value="Update" class="btn btn-primary d-none">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>