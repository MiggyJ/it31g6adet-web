<style>
    .table .form-check .form-check-sign{
        top: 0 !important;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-8 mr-auto ml-auto">
            <form id="resident_update">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title h3">Blotter Info</div>
                        <table class="table table-striped table-bordered" width="100%">
                            <tbody>
                                <tr>
                                    <td>Complainant Name</td>
                                    <td>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">First Name</label>
                                            <input type="text" class="form-control" name="first_name">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Middle Name</label>
                                            <input type="text" class="form-control" name="middle_name">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Last Name</label>
                                            <input type="text" class="form-control" name="last_name">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Defendant Name</td>
                                    <td>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">First Name</label>
                                            <input type="text" class="form-control" name="first_name">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Middle Name</label>
                                            <input type="text" class="form-control" name="middle_name">
                                        </div>
                                        <div class="form-group">
                                            <label class="bmd-label-floating">Last Name</label>
                                            <input type="text" class="form-control" name="last_name">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Complaint Date</td>
                                    <td>
                                        <div class="form-group">
                                            <input type="text" class="form-control datepicker" required name="complaint_date">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Reasons</td>
                                    <td>
                                        <textarea class="form-control" placeholder="Blotter Reason..." rows="5"></textarea>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Details</td>
                                    <td>
                                        <textarea class="form-control" placeholder="Blotter Details..." rows="5" name="details" id="details"></textarea>
                                    </td>
                                </tr>
                                <td>Incidence Place</td>
                                    <td>
                                        <input type="text" class="form-control" placeholder="Incidence Place" required name="incidence_place">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Incidence Date</td>
                                    <td>
                                        <input type="date" class="form-control" required name="incidence_date">
                                    </td>
                                </tr>
                                
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer justify-content-end">
                        <input type="submit" value="Submit" class="btn btn-primary"/>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>