<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-8 mr-auto ml-auto">
            <form id="user_update">
                <div class="card">
                    <div class="card-body">
                        <div class="card-title h3">User Account Info</div>
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="" class="label-control">Resident</label>
                                        <input type="text" disabled id="resident" class="form-control">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-statiC">Role</label>
                                        <select disabled name="role_id"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Email</label>
                                        <input type="email" disabled name="email" class="form-control">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label class="label-control">Date Created</label>
                                        <input type="text" disabled id="created" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group bmd-form-group">
                                        <label class="bmd-label-static">Status</label>
                                        <select disabled name="status">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer justify-content-end">
                        <div id="edit" class="btn btn-info">Edit</div>
                        <div id="delete" class="btn btn-danger"></div>
                        <div id="cancel" class="btn btn-black d-none">Cancel</div>
                        <input type="submit" value="Update" class="btn btn-primary d-none">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>