<div class="container">
    <div class="card">
        <div class="card-body">
            <ul class="nav nav-pills nav-pills-rose justify-content-end" role="tablist">
                <li class="nav-item">
                    <a class="nav-link bg-primary text-white" href="<?=base_url()?>admin/user_new" >
                    New User
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#active" role="tablist">
                    Active
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-toggle="tab" href="#trash" role="tablist">
                    Trash
                    </a>
                </li>
            </ul>
            <hr>
            <div class="tab-content">
                <div class="tab-pane active" id="active">
                    <table class="table table-striped table-bordered" id="activeTable" width="100%">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <div class="tab-pane" id="trash">
                    <table class="table table-striped table-bordered" id="inactiveTable" width="100%">
                        <thead>
                            <tr>
                                <th>Email</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>