<div class="container">
    <div class="row">
        <div class="col-8 mr-auto ml-auto">
            <div class="card">
                <form>
                    <div class="card-body">
                        <div class="card-title h3">User Info</div>
                        <table class="table table-striped table-bordered" width="100%">
                            <tbody>
                                <tr>
                                    <td>Name</td>
                                    <td id="name"></td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>
                                        <input type="text" class="form-control" name="email">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Role</td>
                                    <td>
                                        <div class="dropdown bootstrap-select">
                                            <select name="role_id" ></select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        <select name="status">
                                            <option value="Active">Active</option>
                                            <option value="Inactive">Inactive</option>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer justify-content-end">
                        <input type="submit" id="edit" class="btn btn-info" value="Update"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>