<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-sm-12 mr-auto ml-auto">
            <div class="card">
                <div class="card-header card-header-tabs card-header-rose">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                        <span class="nav-tabs-title h4 my-0">Summon Info</span>
                        <ul class="nav nav-tabs justify-content-end" data-tabs="tabs">
                            <li class="nav-item">
                            <a class="nav-link active" href="#basic" data-toggle="tab">
                                <i class="material-icons">fingerprint</i> Basic Info
                                <div class="ripple-container"></div>
                            </a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="#witness" data-toggle="tab">
                                <i class="material-icons">badge</i> Witnesses
                                <div class="ripple-container"></div>
                            </a>
                            </li>
                        </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="basic">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group mt-3" id="complaint">
                                            <a href="#"></a>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="label-control">Summon Date</label>
                                            <input disabled type="text" name="summon_date" id="summon_date" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="label-control">Summon Time</label>
                                            <input disabled type="text" name="summon_time" id="summon_time" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="label-control">Status</label>
                                            <input type="text" class="form-control" id="status" disabled>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="label-control">Complainant</label>
                                            <input type="text" disabled id="summon_complainant" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-4 offset-2">
                                        <div class="form-group">
                                            <label class="label-control">Defendant</label>
                                            <input type="text" disabled id="summon_defendant" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="label-control">Mediator</label>
                                            <input type="text" disabled id="mediator" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-3 offset-2">
                                        <div class="form-group">
                                            <label class="label-control">Duration</label>
                                            <input type="text" disabled id="duration" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group" >
                                            <label class="label-control">Meeting Details</label>
                                            <div id="details"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="witness">
                            <div class="container-fluid">
                                <table class="table table-striped table-bordered" width="100%" id="witness_table">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Contact Number</th>
                                            <th>Address</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
