<div class="row">
    <div class="col-3">
        <div class="card">
            <div class="card-body">
                <ul class="nav nav-pills nav-pills-icons nav-pills-rose flex-column" id="status_filter" style="cursor:pointer;">
                    <li class="nav-item">
                        <a class="nav-link bg-primary text-white" href="#request_form" data-target="#request_form" data-toggle="modal">
                        Request Certificate
                        </a>
                    </li>
                    <hr>
                    <li class="nav-item">
                        <div data-target="#" class="nav-link active" id="all_status">All Status</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" class="nav-link" id="approved">Approved</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" class="nav-link" id="request">Request</div>
                    </li>
                </ul>
                <hr>
                <ul class="nav nav-pills nav-pills-icons nav-pills-black flex-column" id="type_filter" style="cursor:pointer;">
                    <li class="nav-item ">
                        <div data-target="#" id="all_type" class="nav-link active">All Types</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" id="barangay" class="nav-link">Barangay</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" id="business" class="nav-link">Business</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" id="good_moral" class="nav-link">Good Moral</div>
                    </li>
                    <li class="nav-item">
                        <div data-target="#" id="indigency" class="nav-link">Indigency</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-9">
        <div class="card">
            <div class="card-body">
                <table class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Certificate Number</th>
                            <th>Type</th>
                            <th>Date Requested</th>
                            <th>Status</th>
                            <th>Valid Until</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered mt-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Processing Certificate</h5>
      </div>
      <div class="modal-body">
            <div class="progress" style="height: 15px">
                <div class="progress-bar progress-bar-striped progress-bar-animated" role="progressbar" style="width: 0%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="request_form">
  <div class="modal-dialog modal-dialog-centered modal-sm mt-0" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Request Certificate</h5>
      </div>
      <form id="request_new">  
          <div class="modal-body">
            <div class="container">
                <div class="row mb-2">
                    <div class="col">
                        <div class="form-group bmd-form-group">
                            <label class="bmd-label-static">Certificate Type</label>
                            <select required name="type" title="Select Type">
                                <option value="Barangay">Barangay</option>
                                <option value="Good Moral">Good Moral</option>
                                <option value="Indigency">Indigency</option>
                                <option value="Business">Business</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row d-none">
                    <div class="col">
                        <div class="form-group">
                            <label class="label-control">Business Name</label>
                            <input type="text" name="business_name" class="form-control" disabled>
                        </div>
                    </div>
                </div>
                <div class="row d-none">
                    <div class="col">
                        <div class="form-group">
                            <label class="label-control">Business Address</label>
                            <input type="text" name="business_address" class="form-control" disabled>
                        </div>
                    </div>
                </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
  </div>
</div>