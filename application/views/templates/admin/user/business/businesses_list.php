<style>
    td.dt-body-center{
        text-align: center;
    }
</style>
<div class="card">
    <div class="card-body">
        <ul class="nav nav-pills nav-pills-rose justify-content-end" role="tablist">
            <li class="nav-item">
                <a class="nav-link bg-primary text-white" href="<?=base_url()?>user/business_new" >
                New Business
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#active" role="tablist">
                Active
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#trash" role="tablist">
                Trash
                </a>
            </li>
        </ul>
        <hr>
        <div class="tab-content">
            <div class="tab-pane active" id="active">
                <table id="activeTable" class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Business Name</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Owner</th>
                            <th>House Number</th>
                            <th>Street</th>
                            <th>Barangay</th>
                            <th>Municipality</th>
                            <th>Province</th>
                            <th>Address</th>
                            <th>Contacts</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
            <div class="tab-pane" id="trash">
                <table id="inactiveTable" class="table table-striped table-bordered" width="100%">
                    <thead>
                        <tr>
                            <th>Business Name</th>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Owner</th>
                            <th>House Number</th>
                            <th>Street</th>
                            <th>Barangay</th>
                            <th>Municipality</th>
                            <th>Province</th>
                            <th>Address</th>
                            <th>Contacts</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>