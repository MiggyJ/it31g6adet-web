<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="row">
    <div class="col-lg-10 col-sm-12 mr-auto ml-auto">
        <form id="business_update">
            <div class="card">
                <div class="card-body">
                    <div class="card-title h3">Business Info</div>
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label class="label-control">Business Name</label>
                                    <input type="text" name="name" disabled required class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label class="">Business Owner</label>
                                    <input type="text" name="owner_id" class="form-control" disabled required/>
                                </div>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-2">
                                <div class="form-group">
                                    <label class="label-control">House Number</label>
                                    <input type="text" required disabled id="house_number" name="house_number" class="form-control text-right">
                                    <small class="bmd-helper">Business Address</small>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label class="label-control">Street</label>
                                    <input type="text" required disabled name="street" id="street" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label class="label-control">Barangay</label>
                                    <input type="text" required disabled name="barangay" id="barangay" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label class="label-control">Municipality</label>
                                    <input type="text" required disabled name="municipality" id="municipality" class="form-control">
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label class="label-control">Province</label>
                                    <input type="text" required disabled name="province" id="province" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-3">
                                <div class="form-group">
                                    <label class="label-control">Contact Number (&plus;63)</label>
                                    <input type="text" name="contact_number" class="form-control" disabled minlength="10">
                                </div>
                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label class="label-control">E-mail Address</label>
                                    <input type="email" name="email" class="form-control" disabled>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--footer-->
                    <div class="card-footer justify-content-end">
                        <div id="edit" class="btn btn-info">Edit</div>
                        <div id="delete" class="btn btn-danger"></div>
                        <div id="cancel" class="btn btn-dark d-none">Cancel</div>
                        <input type="submit" value="Update" class="btn btn-primary d-none">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>