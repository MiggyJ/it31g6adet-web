<style>
    .select-with-transition{
        margin-top: .15rem
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-lg-10 col-sm-12 mr-auto ml-auto">
            <div class="card">
                <div class="card-header card-header-tabs card-header-rose">
                    <div class="nav-tabs-navigation">
                        <div class="nav-tabs-wrapper">
                        <span class="nav-tabs-title h4 my-0">Complaint Info</span>
                        <ul class="nav nav-tabs justify-content-end" data-tabs="tabs">
                            <li class="nav-item">
                            <a class="nav-link active" href="#basic" data-toggle="tab">
                                <i class="material-icons">fingerprint</i> Basic Info
                                <div class="ripple-container"></div>
                            </a>
                            </li>
                            <li class="nav-item">
                            <a class="nav-link" href="#meeting" data-toggle="tab">
                                <i class="material-icons">event</i> Meetings
                                <div class="ripple-container"></div>
                            </a>
                            </li>
                        </ul>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="tab-content">
                        <div class="tab-pane active" id="basic">
                            <div class="container-fluid">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="label-control">Complaint Number</label>
                                            <input type="text" disabled id="complaint_number" name="complaint_number" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-3 offset-5">
                                        <div class="form-group">
                                            <label for="" class="label-control">Status</label>
                                            <input type="text" id="status" disabled class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label >Complainant</label>
                                            <select disabled name="complainant_id" id="complainant_id">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="form-group">
                                            <label for="">Defendant</label>
                                            <select disabled name="defendant_id" id="defendant_id">
                                            </select>
                                        </div>
                                    </div>    
                                    <div class="col-3">
                                        <div class="form-group">
                                            <label class="label-control">Complaint Date</label>
                                            <input type="text" disabled id="complaint_date" name="complaint_date" class="form-control">
                                        </div>
                                    </div>
                                </div>
        
                                <div class="row">
                                    <div class="col">
                                        <div class="form-group">
                                            <label class="label-control">Details</label>
                                            <div id="details"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="meeting">
                                <table class="table table-striped" id="meeting_table" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Summon Date</th>
                                            <th>Summon Time</th>
                                            <th>Mediator</th>
                                            <th>Status</th>
                                            <th>Action</th>         
                                        </tr>
                                    </thead>
                                    <tbody>

                                    </tbody>
                                </table>
                                <center id="empty">No Meetings Found</center>
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>