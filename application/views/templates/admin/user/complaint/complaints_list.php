<div class="row">
    <div class="card">
        <div class="card-body">
            <div class="card-title h3">Your Complaints</div>
            <table id="complainant" class="table table-striped table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>Complaint Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Defendant</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="card">
        <div class="card-body">
            <div class="card-title h3">Complaints Against You</div>
            <table id="defendant" class="table table-striped table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>Complaint Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Complainant</th>
                        <th>Status</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>