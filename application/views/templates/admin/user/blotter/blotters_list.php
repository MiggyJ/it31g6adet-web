<div class="row">
    <div class="card">
        <div class="card-body">
            <div class="card-title h3">From Your Complaints</div>
            <table id="complainant" class="table table-striped table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>Blotter Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Blotter Defendant</th>
                        <th>Reasons</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="card">
        <div class="card-body">
            <div class="card-title h3">Blotters Against You</div>
            <table id="defendant" class="table table-striped table-bordered" width="100%">
                <thead>
                    <tr>
                        <th>Blotter Number</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Blotter Complainant</th>
                        <th>Reasons</th>
                        <th>Action</th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>