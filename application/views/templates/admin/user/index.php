<!-- Statistics -->
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <div class="card-title h3">Your statistics:</div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header card-header-success card-header-icon">
                                    <div class="card-icon">
                                        <i class="material-icons">verified</i>
                                    </div>
                                    <p class="card-category">Certificates</p>
                                    <h2 class="card-title" id="certificates">0</h2>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">visibility</i>
                                        <a href="<?=base_url()?>user/certificates">View All Certificates</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header card-header-warning card-header-icon">
                                    <div class="card-icon">
                                        <i class="material-icons">local_police</i>
                                    </div>
                                    <p class="card-category">Blotters</p>
                                    <h2 class="card-title" id="blotters">0</h2>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">visibility</i>
                                        <a href="<?=base_url()?>user/blotters">View All Blotters</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header card-header-danger card-header-icon">
                                    <div class="card-icon">
                                        <i class="material-icons">flag</i>
                                    </div>
                                    <p class="card-category">Complaints</p>
                                    <h2 class="card-title" id="complaints">0</h2>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">visibility</i>
                                        <a href="<?=base_url()?>user/complaints">View All Complaints</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6">
                            <div class="card card-stats">
                                <div class="card-header card-header-info card-header-icon">
                                    <div class="card-icon">
                                        <i class="material-icons">gavel</i>
                                    </div>
                                    <p class="card-category">Summons</p>
                                    <h2 class="card-title" id="summons">0</h2>
                                </div>
                                <div class="card-footer">
                                    <div class="stats">
                                        <i class="material-icons">visibility</i>
                                        <a href="<?=base_url()?>user/summons">View All Summons</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </div>
</div>

<div class="row">
    <div class="col-lg-7 col-sm-12">
        <div class="card mt-0">
            <div class="card-body">
                <div class="card-title h3">Upcoming Summons</div>
                <table class="table table-striped" id="upcoming_summon" width="100%">
                    <thead>
                        <tr>
                            <th class="text-center">Summon Date</th>
                            <th class="text-center">Summon Time</th>
                            <th class="text-center">Mediator</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <center id="empty_summon">No summons found</center>
            </div>
        </div>
    </div>
    <div class="col-lg-5">
        <div class="card mt-0">
            <div class="card-body">
                <div class="card-title h3">Your Certificates</div>
                <table class="table table-striped" id="certificate_list" width="100%">
                    <thead>
                        <tr>
                            <th>Type</th>
                            <th>Link</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                <center id="empty_cert">No validated certificates yet</center>
            </div>
        </div>
    </div>
</div>