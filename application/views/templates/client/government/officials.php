<!-- https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png -->
<div class="container text-center">
    <div class="row">
        <div class="col-lg-6 col-sm-12 mr-auto ml-auto">
            <div class="card">
                <div class="card-body">
                    <img src="https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png" width="180" alt="" class="img-thumbnail rounded-circle">
                    <h2 id="captain" class="title"></h2>
                    <div class="category">Barangay Captain</div>
                </div>
            </div>
        </div>
    </div>
    <div class="row pb-5">
        <div class="col-lg-4 col-md-6 py-3">
            <div class="card">
                <div class="card-body">
                    <img src="https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png" width="100" alt="" class="img-thumbnail rounded-circle">
                    <h3 class="council"></h3>
                    <div class="category">Barangay Councilor</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 py-3">
            <div class="card">
                <div class="card-body">
                    <img src="https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png" width="100" alt="" class="img-thumbnail rounded-circle">
                    <h3 class="council"></h3>
                    <div class="category">Barangay Councilor</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 py-3">
            <div class="card">
                <div class="card-body">
                    <img src="https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png" width="100" alt="" class="img-thumbnail rounded-circle">
                    <h3 class="council"></h3>
                    <div class="category">Barangay Councilor</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 py-3">
            <div class="card">
                <div class="card-body">
                    <img src="https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png" width="100" alt="" class="img-thumbnail rounded-circle">
                    <h3 class="council"></h3>
                    <div class="category">Barangay Councilor</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 py-3">
            <div class="card">
                <div class="card-body">
                    <img src="https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png" width="100" alt="" class="img-thumbnail rounded-circle">
                    <h3 class="council"></h3>
                    <div class="category">Barangay Councilor</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 py-3">
            <div class="card">
                <div class="card-body">
                    <img src="https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png" width="100" alt="" class="img-thumbnail rounded-circle">
                    <h3 class="council"></h3>
                    <div class="category">Barangay Councilor</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 py-3">
            <div class="card">
                <div class="card-body">
                    <img src="https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png" width="100" alt="" class="img-thumbnail rounded-circle">
                    <h3 class="council"></h3>
                    <div class="category">Barangay Councilor</div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 py-3">
            <div class="card">
                <div class="card-body">
                    <img src="https://epicattorneymarketing.com/wp-content/uploads/2016/07/Headshot-Placeholder-1.png" width="100" alt="" class="img-thumbnail rounded-circle">
                    <h3 class="sk"></h3>
                    <div class="category">SK Chairman</div>
                </div>
            </div>
        </div>
    </div>
</div>