<div class="container py-4">

<!--blotter card-->
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-info">
                    <h3 class="card-title">
                    <span class="card-icon">
                        <i class="material-icons">local_police</i>
                    </span>
                Blotter</h3>
                    <p class="category">Residents can view the Blotters tab</p>
            </div>
            <div class="card-body">
                <p>Users that are involved in a blotter case can view the record event in their Blotters tab.
                    <br><br>Only the Defendant or the Complainant of the Blotter record can view the following data:
                    <br>
                    <ul>
                        <li>Blotter Number</li>
                        <li>Defendant’s name</li>
                        <li>Complainant’s name</li>
                        <li>Reason</li>
                        <li>Details(other information for the blotter incident)</li>
                    </ul>
                </p>
            </div>
        </div>
    </div>


<!--complaint card-->
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-info">
                    <h3 class="card-title">
                    <span class="card-icon">
                        <i class="material-icons">flag</i>
                    </span>
                Complaint</h3>
                    <p class="category">Residents can view the Complaints tab</p>
            </div>
            <div class="card-body">
                <p>Users that are involved in a blotter case can view the record event in their Blotters tab.
                    <br><br>Only the Defendant or the Complainant of the Complaint record can view the following data:
                    <br>
                    <ul>
                        <li>Defendant’s name</li>
                        <li>Complainant’s name</li>
                        <li>Date of Complaint</li>
                        <li>Status</li>
                    </ul>
                </p>            
            </div>
        </div>
    </div>
</div>



<!--summon card-->
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h3 class="card-title">
                <span class="card-icon">
                        <i class="material-icons">gavel</i>
                </span>
                Summons</h3>
                <p class="category">Residents can view the Summons tab</p>
            </div>

            <div class="card-body">
                <p>Users that are involved in a Summon case can view the record event in their Summon tab.
                    <br><br>Only the Defendant, the Complainant, and the Witness/es of the Summons record can view the following data:
                    <br>
                    <ul>
                        <li>Defendant’s name</li>
                        <li>Complainant’s name</li>
                        <li>Summon Date</li>
                        <li>Summon Time</li>
                    </ul>
                </p>    
            </div>
        </div>
    </div>


<!--certificate card-->
    <div class="col-md-6">
        <div class="card">
            <div class="card-header card-header-info">
                <h3 class="card-title">
                    <span class="card-icon">
                        <i class="material-icons">verified</i>
                    </span>
                Certificates</h3>
                <p class="category">Residents can request certificates</p>
            </div>

            <div class="card-body">
                <p>Residents may request barangay certificates and business certificates.
                <br>User must fill out the certificate request form consist of the following:
                    <br>
                    <ul>
                        <li>Full Name</li>
                        <li>Address</li>
                        <li>Contact Number</li>
                        <li>Email</li>
                        <li>Certificate Type</li>
                    </ul>
                </p>  
            </div>
        </div>
    </div>


</div>