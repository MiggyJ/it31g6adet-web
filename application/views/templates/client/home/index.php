
<div class="container">
    <div class="section text-center">
    <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
            <h2 class="title">Latest Annoucements</h2>
        </div>
    </div>
    <div>
        <div class="row">
        <div class="col-md-4">
            <div class="card card-blog">
                <div class="card-header card-header-image">
                    <img src="" style="max-height: 300px" alt="" class="img img-raised">
                </div>
                <div class="card-body text-left" style="overflow: auto">
                    <div class="card-title "></div>
                    <span></span>
                    <p style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;overflow: hidden;"></p>
                    
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header card-header-image">
                    <img src="" style="max-height: 300px" alt="" class="img img-raised">
                </div>
                <div class="card-body text-left" style="overflow: auto">
                    <div class="card-title "></div>
                    <span></span>
                    <p style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;overflow: hidden;"></p>
                    
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header card-header-image">
                    <img src="" style="max-height: 300px" alt="" class="img img-raised">
                </div>
                <div class="card-body text-left" style="overflow: auto">
                    <div class="card-title "></div>
                    <span></span>
                    <p style="display: -webkit-box; -webkit-line-clamp: 2; -webkit-box-orient: vertical;overflow: hidden;"></p>
                    
                </div>
            </div>
        </div>
    </div>
</div>