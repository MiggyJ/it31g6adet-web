<div class="container">
    <div class="section section-text">
        <div class="row">
            <div class="col-8 mr-auto ml-auto">
                <h3 class="title mb-0" id="title"></h3>
                <small id="created"></small>
                <div>
                    <a href="" id="view" target="_blank" >
                        <img src="" alt="" id="image" width="300" class="mr-2 mb-2" align="left">
                    </a>
                    <div id="body" style="font-size: 1.188rem; line-height: 1.5em"></div>
                </div>
            </div>
        </div>
    </div>
</div>