<div class="container">
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-body">
                    <table class="table table-striped table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Business Name</th>
                                <th>First Name</th>
                                <th>Last Name</th>
                                <th>Business Owner</th>
                                <th>House Number</th>
                                <th>Street</th>
                                <th>Barangay</th>
                                <th>Municipality</th>
                                <th>Province</th>
                                <th>Address</th>
                                <th>Contact Number</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>