
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="../../assets/img/apple-icon.png">
  <link rel="icon" type="image/png" href="../../assets/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    Material Dashboard PRO by Creative Tim
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?=base_url()?>assets/client/css/material-kit.css?v=2.0.7" rel="stylesheet" />
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.24/css/dataTables.bootstrap4.min.css">
  <!-- End Google Tag Manager -->
</head>

<body class="off-canvas-sidebar">
  <!-- End Google Tag Manager (noscript) -->
  <!-- Navbar -->
  <nav class="navbar navbar-expand-lg navbar-transparent navbar-absolute fixed-top text-white">
    <div class="container">
      <div class="navbar-wrapper">
        <a class="navbar-brand" href="<?=base_url()?>">
          Hehi 2  
        </a>
      </div>
      <button class="navbar-toggler" type="button" data-toggle="collapse" aria-controls="navigation-index" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
        <span class="navbar-toggler-icon icon-bar"></span>
      </button>
      <div class="collapse navbar-collapse justify-content-end">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a href="<?=base_url()?>announcements" class="nav-link">
              <i class="material-icons">article</i> Announcements
            </a>
          </li>
          <li class="dropdown nav-item ">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
              <i class="material-icons">groups</i> Government
            </a>
            <div class="dropdown-menu">
              <a href="<?=base_url()?>officials" class="dropdown-item">Officials</a>
              <a href="<?=base_url()?>ordinances" class="dropdown-item">Ordinances</a>
            </div>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>businesses" class="nav-link">
              <i class="material-icons">store</i> Businesses
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>services" class="nav-link">
              <i class="material-icons">miscellaneous_services</i> Online Services
            </a>
          </li>
          <?php if(empty(get_cookie('token'))):?>
          <li class="dropdown nav-item">
            <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
              <i class="material-icons">account_circle</i> Account
            </a>
            <div class="dropdown-menu dropdown-with-icons">
              <a href="<?=base_url()?>login" class="dropdown-item">
                <i class="material-icons">login</i> Login
              </a>
              <a href="<?=base_url()?>register" class="dropdown-item">
                <i class="material-icons">person_add</i> Register
              </a>
            </div>
          </li>
          <?php else:?>
            <li class="nav-item">
            <a href="<?=base_url()?>dashboard" class="nav-link">
              <i class="material-icons">account_circle</i> Account
            </a>
          </li>
          <?php endif?>
        </ul>
      </div>
    </div>
  </nav>
  <!-- End Navbar -->
  <div class="wrapper wrapper-full-page">
    <div class="page-header error-page header-filter" style="background-image: url('<?=base_url()?>assets/pro/img/clint-mckoy.jpg')">
      <!--   you can change the color of the filter page using: data-color="blue | green | orange | red | purple" -->
      <div class="content-center">
        <div class="row">
          <div class="col-md-12">
            <h1 class="title">404</h1>
            <h2>Page not found :(</h2>
            <h4>Ooooups! Looks like you got lost.</h4>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?=base_url()?>assets/client/js/core/jquery.min.js" type="text/javascript"></script>
  <script src="<?=base_url()?>assets/client/js/core/popper.min.js" type="text/javascript"></script>
  <script src="<?=base_url()?>assets/client/js/core/bootstrap-material-design.min.js" type="text/javascript"></script>
  <script src="<?=base_url()?>assets/client/js/plugins/moment.min.js"></script>
  <!--	Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="<?=base_url()?>assets/client/js/plugins/bootstrap-datetimepicker.js" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?=base_url()?>assets/client/js/plugins/nouislider.min.js" type="text/javascript"></script>
  <!-- Datatables -->
  <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?=base_url()?>assets/client/js/material-kit.js" type="text/javascript"></script>
</body>

</html>