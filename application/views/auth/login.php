<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/client/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?=base_url()?>assets/client/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    BMS - Log In
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?=base_url()?>assets/pro/css/material-dashboard.min.css" rel="stylesheet" />
</head>

<body class="off-canvas-sidebar">
  <nav class="navbar navbar-transparent navbar-color-on-scroll text-white fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand text-white" href="<?=base_url()?>">
          Hehi 2  
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a href="<?=base_url()?>announcements" class="nav-link">
              <i class="material-icons">article</i> Announcements
            </a>
          </li>
          <li class="dropdown nav-item">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
              <i class="material-icons">groups</i> Government
            </a>
            <div class="dropdown-menu">
              <a href="<?=base_url()?>officials" class="dropdown-item">Officials</a>
              <a href="<?=base_url()?>ordinances" class="dropdown-item">Ordinances</a>
            </div>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>services" class="nav-link">
              <i class="material-icons">miscellaneous_services</i> Online Services
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>register" class="nav-link">
              <i class="material-icons">person_add</i> Register
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" style="background-image: url('<?=base_url()?>assets/client/img/bg7.jpg'); background-size: cover; background-position: top center;">
      <div class="container">
        <div class="row">
          <div class="col-lg-4 col-md-6 ml-auto mr-auto">
            <div class="card card-login pb-4">
              <form class="form">
                <div class="card-header card-header-primary text-center">
                  <h3 class="card-title">Login</h3>
                </div>
                <div class="card-body mt-5">
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">email</i>
                        </span>
                      </div>
                      <input type="email" class="form-control" name="email" required placeholder="Email...">
                    </div>
                  </span>
                  <span class="bmd-form-group">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <span class="input-group-text">
                          <i class="material-icons">lock_outline</i>
                        </span>
                      </div>
                      <input type="password" class="form-control" name="password" required placeholder="Password...">
                    </div>
                  </span>
                </div>
                <div class="card-footer">
                  <div class="container-fluid">
                    <div class="row d-flex justify-content-center">
                      <div class="input-group">
                        <input type="submit" class="btn btn-block btn-primary mt-4" value="Log In">
                      </div>
                      <div class="text-center mt-4">Doesn't have an account?</div>
                      <a href="<?=base_url()?>register" class="btn btn-block btn-black">Register</a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <footer class="footer footer-default">
        <div class="container">
          <nav class="float-left">
            <ul>
              <li>
                <a href="##">
                  Privacy Policy
                </a>
              </li>
              <li>
                <a href="##">
                  Contact Us
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-facebook"></i> Facebook
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-twitter"></i> Twitter
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="#" target="_blank">BSIT 3-1 Group 6</a>.
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?=base_url()?>assets/pro/js/core/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/pro/js/core/popper.min.js"></script>
  <script src="<?=base_url()?>assets/pro/js/core/bootstrap-material-design.min.js"></script>
  <script src="<?=base_url()?>assets/pro/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Chartist JS -->
  <script src="<?=base_url()?>assets/pro/js/plugins/chartist.min.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?=base_url()?>assets/pro/js/plugins/bootstrap-notify.js"></script>
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?=base_url()?>assets/pro/js/material-dashboard.min.js" type="text/javascript"></script>

  <!-- Custom Scripts -->
  <script>
    $("form").submit(e => {
      e.preventDefault()
      const proxy = 'http://localhost:3000'
      $('input[type=submit]').addClass('disabled')
      $.ajax({
        type: "POST",
        url: proxy + "/api/auth/login",
        data: $("form").serialize(),
        dataType: "json",
        xhrFields: {
         withCredentials: true
        },
        success: function (response) {
          if(response.error == false){
            $.notify({
              message: response.message
            },{
              type: 'success'
            })
            setTimeout(() => {
              window.location.href = "<?=base_url()?>dashboard"
            }, 1000);
          }else{
            $('input[type=submit]').removeClass('disabled')
            $.notify({
              message: response.message
            },{
              type: 'danger'
            })
          }
        }
      });
    })
  </script>
</body>

</html>