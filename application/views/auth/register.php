<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <link rel="apple-touch-icon" sizes="76x76" href="<?=base_url()?>assets/client/img/apple-icon.png">
  <link rel="icon" type="image/png" href="<?=base_url()?>assets/client/img/favicon.png">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <title>
    BMS - Register
  </title>
  <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- CSS Files -->
  <link href="<?=base_url()?>assets/pro/css/material-dashboard.min.css" rel="stylesheet" />
</head>

<body class="off-canvas-sidebar">
  <nav class="navbar navbar-transparent navbar-color-on-scroll fixed-top navbar-expand-lg" color-on-scroll="100" id="sectionsNav">
    <div class="container">
      <div class="navbar-translate">
        <a class="navbar-brand text-white" href="<?=base_url()?>">
          Hehi 2  
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" aria-expanded="false" aria-label="Toggle navigation">
          <span class="sr-only">Toggle navigation</span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
          <span class="navbar-toggler-icon"></span>
        </button>
      </div>
      <div class="collapse navbar-collapse">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a href="<?=base_url()?>announcements" class="nav-link">
              <i class="material-icons">article</i> Announcements
            </a>
          </li>
          <li class="dropdown nav-item">
            <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">
              <i class="material-icons">groups</i> Government
            </a>
            <div class="dropdown-menu">
              <a href="<?=base_url()?>officials" class="dropdown-item">Officials</a>
              <a href="<?=base_url()?>ordinances" class="dropdown-item">Ordinances</a>
            </div>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>services" class="nav-link">
              <i class="material-icons">miscellaneous_services</i> Online Services
            </a>
          </li>
          <li class="nav-item">
            <a href="<?=base_url()?>login" class="nav-link">
              <i class="material-icons">login</i> Login
            </a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  <div class="wrapper wrapper-full-page">
    <div class="page-header login-page header-filter" style="background-image: url('<?=base_url()?>assets/client/img/bg7.jpg'); background-size: cover; background-position: top center;">
      <div class="container">
        <div class="row">
          <div class="col-sm-8 col-md-6 ml-auto mr-auto">
            <form>
              <div class="card">
              <div class="card-header card-header-rose text-center">
                  <h3 class="card-title">Register</h3>
                </div>
                <div class="card-body">
                  <div class="container-fluid">
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label class="label-control">First Name</label>
                          <input type="text" name="first_name" class="form-control">
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group">
                          <label class="label-control">Last Name</label>
                          <input type="text" name="last_name" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col-4">
                        <div class="form-group">
                          <label class="label-control">Birth Date</label>
                          <input type="text" name="birth_date" class="form-control">
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group">
                          <label class="label-control">Current Street Address</label>
                          <input type="text" name="current_street" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label class="label-control">Email</label>
                          <input type="email" name="email" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <div class="form-group">
                          <label class="label-control">Password</label>
                          <input type="password" name="password" minlength="8" class="form-control">
                        </div>
                      </div>
                      <div class="col">
                        <div class="form-group">
                          <label class="label-control">Confirm Password</label>
                          <input type="password" name="confirm_password" class="form-control">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col">
                        <input type="submit" value="Submit" class="btn btn-block btn-rose">
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
      <footer class="footer footer-default">
        <div class="container">
          <nav class="float-left">
            <ul>
              <li>
                <a href="##">
                  Privacy Policy
                </a>
              </li>
              <li>
                <a href="##">
                  Contact Us
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-facebook"></i> Facebook
                </a>
              </li>
              <li>
                <a href="#">
                  <i class="fa fa-twitter"></i> Twitter
                </a>
              </li>
            </ul>
          </nav>
          <div class="copyright float-right">
            &copy;
            <script>
              document.write(new Date().getFullYear())
            </script>, made with <i class="material-icons">favorite</i> by
            <a href="#" target="_blank">BSIT 3-1 Group 6</a>.
          </div>
        </div>
      </footer>
    </div>
  </div>
  <!--   Core JS Files   -->
  <script src="<?=base_url()?>assets/pro/js/core/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/pro/js/core/popper.min.js"></script>
  <script src="<?=base_url()?>assets/pro/js/core/bootstrap-material-design.min.js"></script>
  <script src="<?=base_url()?>assets/pro/js/plugins/perfect-scrollbar.jquery.min.js"></script>
  <!-- Place this tag in your head or just before your close body tag. -->
  <script async defer src="https://buttons.github.io/buttons.js"></script>
  <!-- Chartist JS -->
  <script src="<?=base_url()?>assets/pro/js/plugins/chartist.min.js"></script>
  <!--  Plugin for Sweet Alert -->
  <script src="<?=base_url()?>assets/pro/js/plugins/sweetalert2.js"></script>
  <!--  Notifications Plugin    -->
  <script src="<?=base_url()?>assets/pro/js/plugins/bootstrap-notify.js"></script>
  <!-- Forms Validations Plugin -->
  <script src="<?=base_url()?>assets/pro/js/plugins/jquery.validate.min.js"></script>
  <!-- Plugin for the momentJs  -->
  <script src="<?=base_url()?>assets/pro/js/plugins/moment.min.js"></script>
  <!--  Plugin for the DateTimePicker, full documentation here: https://eonasdan.github.io/bootstrap-datetimepicker/ -->
    <script src="<?=base_url()?>assets/pro/js/plugins/bootstrap-datetimepicker.min.js"></script>
  <!-- Control Center for Material Kit: parallax effects, scripts for the example pages etc -->
  <script src="<?=base_url()?>assets/pro/js/material-dashboard.min.js" type="text/javascript"></script>

  <!-- Custom Scripts -->
  <script>

    setTimeout(() => {
      $('[name=birth_date]').datetimepicker({
            format: 'MM/DD/YYYY',
            icons: {
                date: "fa fa-calendar",
                up: "fa fa-chevron-up",
                down: "fa fa-chevron-down",
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-screenshot',
                clear: 'fa fa-trash',
                close: 'fa fa-remove'
            }
        })
    }, 10);


    let form = $("form")
    .validate({
      rules: {
        first_name: {
          required: true
        },
        last_name: {
          required: true
        },
        current_street:{
          required: true
        },
        birth_date:{
          required: true
        },
        email: {
          required: true,
          email: true,
        },
        password: {
          required: true,
          minlength: 8,
        },
        confirm_password: {
          required: true,
          equalTo: '[name=password]'
        }
      },
      messages: {
        confirm_password:{
          equalTo: 'Passwords must match.'
        }
      },
      errorElement: 'span',
      highlight: function(element) {
        $(element).closest('.form-group').removeClass('has-success').addClass('has-danger')
      },
      unhighlight: function(element, errorClass, successClass){
          $(element).closest('.form-group').removeClass('has-danger')
      },
      success: function(element) {
        $(element).closest('.form-group').removeClass('has-danger').addClass('has-success')
      },
      errorPlacement : function(error, element) {
        element.closest('.form-group').append(error)
      },
      submitHandler: function(){
        const proxy = 'http://localhost:3000'
        $.ajax({
          type: "POST",
          url: proxy + "/api/auth/register",
          data: $("form").serialize(),
          dataType: "json",
          xhrFields: {
          withCredentials: true
          },
          success: function (response) {
            if(response.error == false){
              let redirect = setTimeout(() => {
                window.location.href='<?=base_url()?>login'
              }, 5000);

              Swal.fire({
                title: response.message,
                text: 'Redirecting to Login Page',
                type: 'success',
                showCancelButton: true,
                showConfirmButton: false,
                timer: 5000,
              }).then((result)=>{
                if(result.dismiss === 'cancel'){
                  clearTimeout(redirect)
                  $('form').get(0).reset()

                  form.formReset()
                  $('.form-group').removeClass('has-danger').removeClass('has-success')
                }
              })              
            }else{
              $.notify({
                message: response.message
              },{
                type: 'danger'
              })
            }
          }
        })
      }
    })        
  </script>
</body>

</html>